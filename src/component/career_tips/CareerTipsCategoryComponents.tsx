import React from "react";
import { Link, withRouter } from "react-router-dom";
import { useHistory } from 'react-router-dom';


interface ICareerTipsCategoryComponentProps { }

interface ICareerTipsCategoryComponentState { }

const initialState = {};

const CareerTipsCategoryComponent: React.FC<ICareerTipsCategoryComponentProps> = (props) => {
  const [startUpState, setCareerTipsCategoryComponentState] = React.useState<ICareerTipsCategoryComponentState>(
    initialState
  );
  const history = useHistory();
  return (
    <>
     {/* Header Start */}

     <div className="header header1"> 
        <header> 
          <div className=" container">
            <div className="row"> 
              <div className="col-sm-2">
                <div className=" logo">
                <a
                            onClick={() => {
                              history.push("/");
                            }}>
                    <img 
                src={require("../../images/logo.png")}
                // src={require("../../images/logo.png" 
                className="img-responsive" alt="" /> </a></div> 
              </div> 
              <div className="col-sm-10"> 
                <div className="bs-example">
                  <nav role="navigation" className="navbar navbar-default navbar-static" id="navbar-example">
                    <div className="navbar-header">
                      <button data-target=".bs-example-js-navbar-collapse" data-toggle="collapse" type="button" className="navbar-toggle"> <span className="sr-only">Toggle navigation</span> <span className="icon-bar" /> <span className="icon-bar" /> <span className="icon-bar" /> </button>
                      <div className="collapse navbar-collapse bs-example-js-navbar-collapse">
                        <ul className="nav navbar-nav">  
                        <li><Link to='/'> Home</Link> </li>  
                          <li> <a href="#">Career Developer</a></li>
                          <li> <a href="#">Client </a></li>
                          <li> <a href="#">Vendor</a> </li>  
                          <li><Link to='/help'>Help</Link></li>
                          <li><Link to='/about_us'>About Us</Link></li>
                          <li><Link to='/contact_us'>Contact Us</Link></li> 
                        </ul> 
                      </div> 
                    </div> 
                  </nav> 
                </div> 
              </div>
            </div>
          </div>
        </header>
      </div>
{/* Header ends */}

{/*  */}
<div className="category_sec_sub">
        <div className="post_career_tip1"><a href="#" data-target="#post_career" data-toggle="modal">Post a Career Tip</a></div>
        <div className="container">
          <div className="row"> 
            <div className="col-sm-12"> 
              <h3>Career Tips </h3>  
              <h1>General</h1>  
              <div className="action_btn1">  
                <div className="categoriesbtn"><a href="career_tips.html" className="careertips_btn"> Categories </a> 
                  <Link to='/career_tips'> Back </Link> 
                </div>
                <div className="caretips_action" tabIndex={-1} style={{display: 'none'}}>
                  <div className="action_sec">
                    <ul>
                      <li><Link to='/career_tips_category'>General </Link></li>
                      <li><a href="#">Training </a></li>
                      <li><a href="#">Job hunt </a></li>
                      <li><a href="#">Profile </a></li> 
                      <li><a href="#">Resume and  Cover letter </a></li>
                      <li><a href="#">Communication </a></li>
                      <li><a href="#">Interview </a></li> 
                      <li><a href="#">Telephonic Skills </a></li>
                      <li><a href="#">Personality Skills </a></li>
                      <li><a href="#">Work life  management </a></li> 
                      <li><a href="#">Job shifting</a></li>
                      <li><a href="#">Role switching </a></li>
                      <li><a href="#">Taking a brake </a></li> 
                      <li><a href="#">Excelling in career </a></li> 
                      <li><a href="#">Positive attitude </a></li> 
                    </ul>  
                  </div> 
                </div>
              </div>
            </div>
            <div className="clearfix" />     
            <div className="col-sm-6 col-lg-4"> 
              <div className="categories_sec">
                <div className="achieve_img"><img 
                  src={require("../../images/technology.jpg")}
                // src="images/technology.jpg" 
                className="img-responsive" /></div>
                <h2>Technology </h2>
                <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p> 
                <div className="category_read"><a href="#"><img 
                  src={require("../../images/achive_more.png")}
                // src="images/achive_more.png" 
                /> Read More </a></div>
              </div> 
            </div>
            <div className="col-sm-6 col-lg-4"> 
              <div className="categories_sec">
                <div className="achieve_img"><img 
                  src={require("../../images/ecnomy.jpg")}
                // src="images/ecnomy.jpg"
                 className="img-responsive" /></div>
                <h2>Economy </h2>
                <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p> 
                <div className="category_read"><a href="#"><img 
                  src={require("../../images/achive_more.png")}
                // src="images/achive_more.png" 
                /> Read More </a></div>
              </div>  
            </div>
            <div className="col-sm-6 col-lg-4"> 
              <div className="categories_sec">
                <div className="achieve_img"><img
                  src={require("../../images/business.jpg")}
                //  src="images/business.jpg"
                  className="img-responsive" /></div>
                <h2>Technology </h2>
                <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p> 
                <div className="category_read"><a href="#"><img 
                src={require("../../images/achive_more.png")}
                //  src="images/achive_more.png" 
                 /> Read More </a></div>
              </div>  
            </div>
            <div className="col-sm-6 col-lg-4"> 
              <div className="categories_sec">
                <div className="achieve_img"><img 
                  src={require("../../images/technology.jpg")}
                // src="images/technology.jpg" 
                className="img-responsive" /></div>
                <h2>Technology </h2>
                <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p> 
                <div className="category_read"><a href="#"><img 
                  src={require("../../images/achive_more.png")}
                // src="images/achive_more.png"
                 /> Read More </a></div>
              </div> 
            </div>
            <div className="col-sm-6 col-lg-4"> 
              <div className="categories_sec">
                <div className="achieve_img"><img 
                  src={require("../../images/ecnomy.jpg")}
                // src="images/ecnomy.jpg" 
                className="img-responsive" /></div>
                <h2>Economy </h2>
                <p>Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry Lorem Ipsum is simply dummy text of the  printing and typesetting industry</p> 
                <div className="category_read"><a href="#"><img 
                  src={require("../../images/achive_more.png")}
                // src="images/achive_more.png"
                 /> Read More </a></div>
              </div> 
            </div>
          </div>          
        </div>
      </div>
{/*  */}
    </>
  );
};
export default withRouter(CareerTipsCategoryComponent);
