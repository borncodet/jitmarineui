import React from "react";
import { Switch, Route, useRouteMatch, withRouter } from "react-router-dom";
<<<<<<< HEAD
import { getAllVendorCount, useJobTypeContext, useJobTypeDispatcher } from "../../../action/general/JobTypeAction";
import { getLoggedUserId, useMyProfileContext, useMyProfileDispatcher } from "../../../action/MyProfileAction";
import { getVendorJobAppliedList, setVendorJobAppliedSave, useVendorJobAppliedContext, useVendorJobAppliedDispatcher } from "../../../action/vendor/VendorJobAppliedAction";
import { getVendorJobBookMarkList, useVendorJobBookMarkContext, useVendorJobBookMarkDispatcher } from "../../../action/vendor/VendorJobBookMarkAction";
import { getVendorSuggestedJobList, getVendorSuggestedJobTitleList, useVendorSuggestedJobContext, useVendorSuggestedJobDispatcher } from "../../../action/vendor/VendorSuggestedJobAction";
import { VendorCountAllRequestModel } from "../../../models/general/JobType";
import { VendorGetAllJobRequestModel } from "../../../models/vendor/VendorJobApplied";
import { VendorSuggestedJobTitleRequestModel, VendorSuggestedRequestModel } from "../../../models/vendor/VendorSuggestedJob";
=======
import {
  getAllVendorCount,
  useJobTypeContext,
  useJobTypeDispatcher,
} from "../../../action/general/JobTypeAction";
import {
  getLoggedUserId,
  useMyProfileContext,
  useMyProfileDispatcher,
} from "../../../action/MyProfileAction";
import {
  getVendorJobAppliedList,
  setVendorJobAppliedSave,
  useVendorJobAppliedContext,
  useVendorJobAppliedDispatcher,
} from "../../../action/vendor/VendorJobAppliedAction";
import {
  getVendorJobBookMarkList,
  useVendorJobBookMarkContext,
  useVendorJobBookMarkDispatcher,
} from "../../../action/vendor/VendorJobBookMarkAction";
import {
  getVendorSuggestedJobList,
  getVendorSuggestedJobTitleList,
  useVendorSuggestedJobContext,
  useVendorSuggestedJobDispatcher,
} from "../../../action/vendor/VendorSuggestedJobAction";
import { VendorCountAllRequestModel } from "../../../models/general/JobType";
import { VendorGetAllJobRequestModel } from "../../../models/vendor/VendorJobApplied";
import {
  VendorSuggestedJobTitleRequestModel,
  VendorSuggestedRequestModel,
} from "../../../models/vendor/VendorSuggestedJob";
>>>>>>> 39419b1 (merged-on-10032021)
import AuthService from "../../../services/AuthService";
import VendorDashboardContainer from "../dashboard/VendorDashBoardContainer";
import VendorJobAppliedComponent from "../jobs_applied/VendorJobAppliedComponent";
import VendorSavedJobContainer from "../saved_jobs/VendorSavedJobContainer";
import VendorLayoutSidebar from "./VendorLayoutSidebar";
import VendorMessageContainer from "../message/VendorMessageContainer";
import VendorMessageComponent from "../message/VendorMessageComponent";
import VendorProfileContainer from "./../my_profile/VendorProfileContainer";
import { MyProfileVendorContextProvider } from "../../../context/MyProfileVendorContext";
<<<<<<< HEAD
import { SocialAccountContextProvider, VendorContextProvider, VendorProfileImageContextProvider } from "../../../context/vendor/VendorMyProfile";
import {  getVendorProfileImage, useVendorMyProfileContext, useVendorMyProfileDispatcher } from "../../../action/MyProfileVendorAction";
import { VendorProfileImageRequestModel } from "../../../models/vendor/MyProfileSelectBoxData";


=======
import {
  SocialAccountContextProvider,
  VendorContextProvider,
  VendorProfileImageContextProvider,
} from "../../../context/vendor/VendorMyProfile";
import {
  getVendorProfileImage,
  useVendorMyProfileContext,
  useVendorMyProfileDispatcher,
} from "../../../action/MyProfileVendorAction";
import { VendorProfileImageRequestModel } from "../../../models/vendor/MyProfileSelectBoxData";

>>>>>>> 39419b1 (merged-on-10032021)
interface IVendorLayoutComponentProps {}

interface IVendorLayoutComponentState {}

<<<<<<< HEAD

const initialState = {};


=======
const initialState = {};

>>>>>>> 39419b1 (merged-on-10032021)
// const authorizationToken='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8';
// let loggedVendorId=1;

const VendorLayoutComponent: React.FC<IVendorLayoutComponentProps> = (
  props
) => {
  const [
    vendorLayoutComponentState,
    setVendorLayoutComponentState,
  ] = React.useState<IVendorLayoutComponentState>(initialState);

<<<<<<< HEAD
   const authorizationToken = AuthService.accessToken;

  const { path, url } = useRouteMatch();
  
  const jobAppliedVendorDispatcher = useVendorJobAppliedDispatcher();
  const jobAppliedVendorContext = useVendorJobAppliedContext();
  const { vendorJobAppliedSaveRespond,vendorGetAllJobList } = jobAppliedVendorContext;
=======
  const authorizationToken = AuthService.accessToken;

  const { path, url } = useRouteMatch();

  const jobAppliedVendorDispatcher = useVendorJobAppliedDispatcher();
  const jobAppliedVendorContext = useVendorJobAppliedContext();
  const {
    vendorJobAppliedSaveRespond,
    vendorGetAllJobList,
  } = jobAppliedVendorContext;
>>>>>>> 39419b1 (merged-on-10032021)

  const jobTypeDispatcher = useJobTypeDispatcher();
  const jobTypeContext = useJobTypeContext();
  const { jobType } = jobTypeContext;

  const vendorSuggestedJobDispatcher = useVendorSuggestedJobDispatcher();
  const vendorSuggestedJobContext = useVendorSuggestedJobContext();
  const { vendorSuggestedJobs } = vendorSuggestedJobContext;

  const vendorJobBookMarkDispatcher = useVendorJobBookMarkDispatcher();
  const vendorJobBookMarkContext = useVendorJobBookMarkContext();
  const { bookmarkSaveResult } = vendorJobBookMarkContext;

  const myProfileDispatcher = useMyProfileDispatcher();
  const myVendorProfileDispatcher = useVendorMyProfileDispatcher();
<<<<<<< HEAD
  
  const myVendorProfileContext = useVendorMyProfileContext();
  const {
    basicInfo,
     loggedVendorId,
=======

  const myVendorProfileContext = useVendorMyProfileContext();
  const {
    basicInfo,
    loggedVendorId,
>>>>>>> 39419b1 (merged-on-10032021)
    myProfile,
    myProfileProgressBar,
    profileImage,
  } = myVendorProfileContext;

  let user = AuthService.currentUser;

<<<<<<< HEAD


=======
>>>>>>> 39419b1 (merged-on-10032021)
  React.useEffect(() => {
    if (authorizationToken != null && user?.id != null)
      (async () => {
        await getLoggedUserId(
          myProfileDispatcher,
          parseInt(user.id),
          authorizationToken
        );
      })();
  }, [authorizationToken]);

  React.useEffect(() => {
<<<<<<< HEAD
     if (authorizationToken!=null) {
=======
    if (authorizationToken != null) {
>>>>>>> 39419b1 (merged-on-10032021)
      (async () => {
        await getVendorJobAppliedList(
          jobAppliedVendorDispatcher,
          {
<<<<<<< HEAD
            vendorId:loggedVendorId,
           page:1,
           pageSize:10,
           searchTerm:"",
            showInactive:false,
            sortOrder:"",
=======
            vendorId: loggedVendorId,
            page: 1,
            pageSize: 100,
            searchTerm: "",
            showInactive: false,
            sortOrder: "",
>>>>>>> 39419b1 (merged-on-10032021)
          } as VendorGetAllJobRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
    }
  }, [loggedVendorId]);

  React.useEffect(() => {
<<<<<<< HEAD
     if (authorizationToken!=null) {
      (async () => {
        await   getAllVendorCount(
          jobTypeDispatcher,
          {
            page:1,
            pageSize:10,searchTerm:"",
            showInactive:false,
            sortOrder:"",
            vendorId:loggedVendorId
=======
    if (authorizationToken != null) {
      (async () => {
        await getAllVendorCount(
          jobTypeDispatcher,
          {
            page: 1,
            pageSize: 100,
            searchTerm: "",
            showInactive: false,
            sortOrder: "",
            vendorId: loggedVendorId,
>>>>>>> 39419b1 (merged-on-10032021)
          } as VendorCountAllRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
<<<<<<< HEAD
     }
  }, [loggedVendorId]);

  React.useEffect(() => {
     if (authorizationToken!=null) {
      (async () => {
        await   getVendorSuggestedJobList(
          vendorSuggestedJobDispatcher,
          {
            page:1,
            pageSize:10,searchTerm:"",
            showInactive:false,
            sortOrder:"",
            vendorId:loggedVendorId
=======
    }
  }, [loggedVendorId]);

  React.useEffect(() => {
    if (authorizationToken != null) {
      (async () => {
        await getVendorSuggestedJobList(
          vendorSuggestedJobDispatcher,
          {
            page: 1,
            pageSize: 100,
            searchTerm: "",
            showInactive: false,
            sortOrder: "",
            vendorId: loggedVendorId,
>>>>>>> 39419b1 (merged-on-10032021)
          } as VendorSuggestedRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
<<<<<<< HEAD
     }
  }, [loggedVendorId]);

  React.useEffect(() => {
     if (authorizationToken!=null) {
      (async () => {
        await   getVendorJobBookMarkList(
          vendorJobBookMarkDispatcher,
          {
            page:1,
            pageSize:10,searchTerm:"",
            showInactive:false,
            sortOrder:"",
            vendorId:loggedVendorId
=======
    }
  }, [loggedVendorId]);

  React.useEffect(() => {
    if (authorizationToken != null) {
      (async () => {
        await getVendorJobBookMarkList(
          vendorJobBookMarkDispatcher,
          {
            page: 1,
            pageSize: 100,
            searchTerm: "",
            showInactive: false,
            sortOrder: "",
            vendorId: loggedVendorId,
>>>>>>> 39419b1 (merged-on-10032021)
          } as VendorGetAllJobRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
<<<<<<< HEAD
     }
  }, [loggedVendorId]);

  React.useEffect(() => {
     if (authorizationToken !=null) {
      (async () => {
        await   getVendorSuggestedJobTitleList(
          vendorSuggestedJobDispatcher,
          {
            page:1,
            pageSize:10,
            searchTerm:"",
            showInactive:false,
            sortOrder:"",
            vendorId:loggedVendorId,
=======
    }
  }, [loggedVendorId]);

  React.useEffect(() => {
    if (authorizationToken != null) {
      (async () => {
        await getVendorSuggestedJobTitleList(
          vendorSuggestedJobDispatcher,
          {
            page: 1,
            pageSize: 100,
            searchTerm: "",
            showInactive: false,
            sortOrder: "",
            vendorId: loggedVendorId,
>>>>>>> 39419b1 (merged-on-10032021)
          } as VendorSuggestedJobTitleRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
<<<<<<< HEAD
     }
  }, [loggedVendorId]);


  React.useEffect(() => {
    if (authorizationToken !=null) {
=======
    }
  }, [loggedVendorId]);

  React.useEffect(() => {
    if (authorizationToken != null) {
>>>>>>> 39419b1 (merged-on-10032021)
      (async () => {
        await getVendorProfileImage(
          myVendorProfileDispatcher,
          {
<<<<<<< HEAD
           page:10,pageSize:10,searchTerm:"",showInactive:true,
           sortOrder:"",vendorId:loggedVendorId
=======
            page: 10,
            pageSize: 10,
            searchTerm: "",
            showInactive: true,
            sortOrder: "",
            vendorId: loggedVendorId,
>>>>>>> 39419b1 (merged-on-10032021)
          } as VendorProfileImageRequestModel,
          authorizationToken
        );
      })();
    }
<<<<<<< HEAD
 }, [loggedVendorId]);



  return (
    <div id="wrapper">
       {/* <VendorProfileImageContextProvider> */}
      <VendorLayoutSidebar />
      {/* </VendorProfileImageContextProvider> */}
      <Route exact path={`${path}`}>
      <VendorDashboardContainer />
      </Route>
      <Route exact path={`${path}/jobs-applied`}>
          <VendorJobAppliedComponent />
        </Route>
        <Route exact path={`${path}/saved-jobs`}>
          <VendorSavedJobContainer />
        </Route>
        <Route exact path={`${path}/messages`}>
          <VendorMessageContainer />
        </Route>
        <Route exact path={`${path}/my-profile`}>
          <VendorContextProvider>
          <MyProfileVendorContextProvider>
            <SocialAccountContextProvider>
              <VendorProfileContainer />
            </SocialAccountContextProvider>
          </MyProfileVendorContextProvider>
          </VendorContextProvider>
        </Route>
=======
  }, [loggedVendorId]);

  return (
    <div id="wrapper">
      {/* <VendorProfileImageContextProvider> */}
      <VendorLayoutSidebar />
      {/* </VendorProfileImageContextProvider> */}
      <Route exact path={`${path}`}>
        <VendorDashboardContainer />
      </Route>
      <Route exact path={`${path}/jobs-applied`}>
        <VendorJobAppliedComponent />
      </Route>
      <Route exact path={`${path}/saved-jobs`}>
        <VendorSavedJobContainer />
      </Route>
      <Route exact path={`${path}/messages`}>
        <VendorMessageContainer />
      </Route>
      <Route exact path={`${path}/my-profile`}>
        {/* <VendorContextProvider>
          <MyProfileVendorContextProvider> */}
        <SocialAccountContextProvider>
          <VendorProfileContainer />
        </SocialAccountContextProvider>
        {/* </MyProfileVendorContextProvider>
          </VendorContextProvider> */}
      </Route>
>>>>>>> 39419b1 (merged-on-10032021)
    </div>
  );
};
export default withRouter(VendorLayoutComponent);
