import React, { useState } from "react";
import { useForm } from "react-hook-form";
import {
  Link,
  withRouter,
  useRouteMatch,
  useHistory,
  useLocation,
  Redirect,
} from "react-router-dom";
import { useMyProfileContext } from "../../../action/MyProfileAction";
import { useVendorMyProfileContext } from "../../../action/MyProfileVendorAction";
import { useCandidateProfileImageContext } from "../../../context/candidate/CandidateMyProfile";
import { useVendorContext, useVendorProfileImageContext } from "../../../context/vendor/VendorMyProfile";
import AuthService from "../../../services/AuthService";

function CandidateLayoutSidebar(props: any) {
  const { path, url } = useRouteMatch();

  const location = useLocation();
  const [active, setActive] = useState(
    location.pathname != null ? location.pathname : "/candidate"
  );

  const [menuHide, setMenuHide] = useState(false);

  const back = () => {
    console.log(location);
    props.history.replace(location.pathname);
    return <Redirect to="/" />;
  };

  const candidateProfileImageContext = useCandidateProfileImageContext();

  const {
    candidateProfileImage,
    getCandidateProfileImage,
  } = candidateProfileImageContext;


  const myVendorProfileContext = useVendorMyProfileContext();
  const {
    basicInfo,
     loggedVendorId,
    myProfile,
    myProfileProgressBar,
    profileImage,
  } = myVendorProfileContext;

  const VendorProfileImageContext = useVendorProfileImageContext();
  const { vendorProfileImage, getVendorProfileImage } = VendorProfileImageContext;
  const authorizationToken = AuthService.accessToken;
  
  
  const VendorContext = useVendorContext();
  const {getVendors,  vendorState } = VendorContext;
  const user = AuthService.currentUser;

  const [currentWidth, setCurrentCurrentWidth] = useState<number>(
    window.innerWidth
  );

  const candidateId = loggedVendorId;

  React.useEffect(() => {
    if (loggedVendorId) {  
      console.log(loggedVendorId,'@@@@@/////////// ',vendorState);
      getVendors({
        VendorId: Number(loggedVendorId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: "",
        ShowInactive: false,
      });
    }
    },[loggedVendorId]);

  React.useEffect(() => {
    if (loggedVendorId ) {
      getVendorProfileImage({
        vendorId: Number(loggedVendorId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "string",
        SortOrder: "string",
        ShowInactive: false,
      });
    }
  }, [loggedVendorId]);

  const onLogout = () => {
    AuthService.logout();
    props.history.push("/");
  };

  const navigation = (url: any) => {
    props.history.push(url);
    if (currentWidth < 650) {
      setMenuHide(!menuHide);
    }
  };

  const cl = props.location.pathname;

  console.log(111111111111111,    profileImage);
  console.log(222222222222222, window.innerWidth);

  React.useEffect(() => {
    setCurrentCurrentWidth(window.innerWidth);
  }, [window.innerWidth]);

  return (
    <>
      <div className="icon-mobile-nav">
        <a
          onClick={() => {
            setMenuHide(!menuHide);
          }}
        >
          <i className="fa fa-2x fa-reorder" />
        </a>
      </div>
      <div className="menu_main">
        <div className={`left-menu vend_menu  ${menuHide ? "hide-menu" : ""}`}>
          <div className="dash_logo">
            <img
              src={require("./../../../images/logo_dash.png")}
              className="img-responsive center-block"
            />
          </div>
          <div className="dash_profile">
            {/* <img
            src={
              profileImage != null && profileImage.total > 0
                ? `https://jitapi.clubactive.in/Upload/ProfileImage/${profileImage.data[0].imageUrl}`
                : require("../../../images/profileDefault1.jpg")
            }
            className="img-responsive"
          /> */}

            {vendorProfileImage.data.length > 0 ? (
              <img
                className="img-responsive"
                  src={`https://jitapi.clubactive.in/Upload/VendorProfileImage/${vendorProfileImage.data[0]["imageUrl"]}`}
                alt=""
              />
            ) : (
              <img
                className="img-responsive"
                src={require("./../../../images/profileDefault1.jpg")}
              ></img>
            )}

{/* {vendorProfileImage.data.length > 0 ? (
                    <img
                      width="154"
                      height="187"
                      src={`https://jitapi.clubactive.in/Upload/VendorProfileImage/${vendorProfileImage.data[0]["imageUrl"]}`}
                      alt=""
                      className="image1"
                    />
                  ) : (
                    //<img width="154" height="187" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMEAAACmCAYAAABjs+t+AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAyMDoxMToxMSAxMjoyNzoxMK7z7acAABEiSURBVHhe7Z33i1RXFMfvbjQWlFiwYW+xgAUs2GLDqLCxGwiRBH/zX/KH5BdFUTRRREGISESjYsEWYtegYsUSDMbEhI2f67vr7Djz5r2ZNzvv3Hs+sMnM7Lr7yvneU+659zX99ddfrSZwOnXqZJqamsyrV6/M9evXzd27d829e/fMRx99ZL9857///rNfffv2NSNGjDDDhg0zAwYMMK2trebff/+NfspfghVBseHfuHHDPHnyxH4vFOMvhRNEt27dzMSJE60g+vfvb7/nqyCCEwHGD4z0Fy5cMA8ePLA3PWTDL4cTRL9+/cyYMWOsKDp37uydh/BaBG60f/nypbly5Yq5dOmS+eeff9TgY8Do4c2bN2bdunXeewHwSgRulMfgf/31V/P06VM1+Bpx3oBrOHXqVDNp0iTTpUsX9QR5A+PnphDXnzp1yrw9J/Pxxx9H31WywnmJsWPHmmnTppkePXp4IQbRIigc+dX4Ow7nHcaPH29mzpxpk2jJYhAtAh35G4sTA16BUEkqzdH/xbF3715z5MgRexNUAI2BPIFrf/78eeuVnWeWhjgRUKJ79uyZrfio8ecDxLBlyxZbdub+SENMOORGmePHj9tSpwogf1B+Hj16tJk1a5aopDnXIsDw//77b3P06FFz8+ZNNXxBIAjaMJYvX266du0afZpPci2CO3futMX9WuuXh0ucN23alGuvkNucAC/w008/2dcqAJm4xHn37t3m9evXuU2ccykCkqvHjx9r+OMJL168MDt27LDzOXlMnHMXDjFaXLx40Zw4cUJF4BkucV64cKG9z3kJkXIjApcEHzp0yJbaVAB+4uZ1SJj79OkTfdpYchEOIYA///zT7Ny507Y2qwD8hTwBj7Bnz57chEYNFwEXgkUt27Zt0ypQILiE+ZdffsmFEBoaDnEBTp8+bc6ePaujf4DgEViws3btWvu+UTlCQ0Xw888/6yRY4OD9e/XqZXOERs0yd6gIXOx/8OBBWzbzOfRxE0VAqzE3mBlUVmyxWssJ3yWHjIr0RPE5r58/f25/luvEl/td7pr5eO0471WrVpmBAwd2qBjqLgJn+Lt27fIm5ncGPmTIEPPpp5/aRSYs4+zotbdcW6CahkcltwIfrjGTa3PnzjVTpkyxg0E9qasInAB+/PFHq3LpN8cZP0sMJ0+ebHr27JmLRedODG/vpRUCayw4TulhJjazYMECM2HChLoKoW4i8EkAGBSwimrcuHG5XmPLdefYfvvtN1tw4NglX3vnERh06nXNMxeBL8bPsRPDz58/325EVW+XXC+owLH2gjyMjQekegfuR73yhbp4gn379tkkT7IAWDI4Y8YML/bYceHSmTNnRJejuS8bNmzIvIqUqQgYdX744QexAnBx9NKlS82gQYPEjv7l4P4wI79//36xAxTHzbxClkLIbMaYC0zdn60MpV5gypVffvmldbm+CQA4J86N83R5jjTwBoR2WXqCTESAANjSkFZZya525cqV9lykhz9xcG6cJ0LgnKXBAMu8CSE39yoLahYB8Sa7ONMHIlkA7KETEmyxyDlLFQIRR1a9RzWJwFWCDhw4kPt1pOVwAqDHPSQIjThnqUJgwGXDhWvXrrUl/tVSkwhwrZRCpeYAhQLwMQeoBOdMDZ4mNok5AkI4fPiwHYhrEUJNIiARxpCkioC2h1AFUAjVFgxKqhAYiGvJ46oWAcqjX0WqAKClpSV4AThcO7NEGIgZkKvND6oSgcsFpCbCILVEWA8YRam7L168WGR+wEDMgHz58uWqwqKqPQG1Wqlwo2nM8rkUmhauBYvgaQ6UKAQGZPaoYnBOS2oR4HLYCpE+FIngAbjZtEAr7SE0nDNnju2ZkggegcE5rTdIJQJ++aNHj0TvBcqF+uyzzzQXiGHevHliwyIGZ3qk0pDaE6A0qQLgxs6ePdu2QiulISyibwpvKRFskybBNN4glQgIg1i4IRXcPPMCmgvEg5fEW0olbcUysQhQluQwCC+Am1eSgbeUWkFDBOximJRUniCtwvICN5OJMdy8eoFkSL9OLDFNSiIR4AVQlmQRsEiGBTJKcnh4t+T5lKSTZ4lEwKjABrlSIRfo6G08fICBQ3JIRHdzkgQ5VgT8AlaJff/992JzAW7i6tWrVQBVQF5Aa4nEcinQ3cwal0pCiBUBe+mwTaLkMIg9gbJafBEaDBzkUmweJhEG7mPHjtndzuMoKwLUw4ZZrEmVKgKOm1ZhnRirDbZIlOoNsIHt27fHhnUlRYAA2NWM2TfJXmDo0KGJYkKlPHiDwm0jJYItnDx5smxEUFIEhEH8I6kCAE58+PDh0TulVhhQ4kbTPIMdkxuw/1IpPhABIydtqZK9gEPnBbKD3ECqCABbpsxfKjL4QAR4AR7TL10AbidopXZcgiwZ7JmtKUslye1EgEroEpW8dxBoKJQ9DCiS8wIHrT/F3qCdCKSXRAvp3bt39ErJAmyDBFl6SIQ3KKadCEgcJJdEHdwoZoiV7KDlhI2JpUMXNNvXF3qDNhHwYZrOu7zTvXt3TYoz5pNPPhHtCYCQjt0S8WyONhFgMLgKH0IhzkGT4uxBBD5A5ZPc13kDKwLe3L59W7zKHTwIrlDpSu0wSOJdfRkk2ZnCYUWAwZA1+3CCCBkRaNt09vjSg4Wdkxe4cqkVgQ9l0UIYsZTsoauUAcYHGCyJfoiCmvnPjRs3vBEAaMNcffApxMTeyYE5J+sJfBOBzhEolcDeiX6YFmh++PCh6B0kisHNSe1/zzvkWVJ3sC4FQqCxrvnWrVteeQFFSQp2//vvv5tm30IhRUkDcwbNPoVCipIWHECzegEldFQEStBYTxC9VpRg8VIEvpTw8gYTS9U8BCPveCcC3BttIEp9oJDiWwjtpSfQzbaUNHgpAu0dqg9c11evXkXv/MFLETAB4lOzV14g15K6E10cXuYEJG+6niB7/vjjDy+LDl56AkYrXV+cLbTc++gFwEsRMFqxpbySLXhYHydXvRQBcMPcQmolG9ik2Ue8FAGjlXqC7Hnx4oV6Ailwo9hPlUfOqjfIhu+++67srs7S8TYcQgj379+P3inVwiDC6kOfW1G8FQEwX+Bjr0tHc+fOHS/DIIfXIgCSOQ2JasP31Ydei4AbxwNHdPa4evCkvq8+9F4E7LKtvUTVgQdlNwafvQB4Hw6R0BHTakiUHjwouzGoCITDDaRcqiFReliXQXHBd4IQARNnPrYA15tQtuPxXgSO4qeTKJW5evWqisAXuJHXrl2L3ilJYMDwtWu0mGA8AX0vBw4cUG+QEEqjPjytMgnBiABvwMSZziBXhjXaZ8+ejd75TzAiAIRw8uRJXYhfAYoI5FChEJwImEH2tRsyCwgXz507F70Lg6BEAOoN4uE5XqFUhRxBikC9QWnwAjzA0ee26VIEJwJw3kBpD41yJMQheQEIVgR4Ay2XvofwkFwgNAFAkCIAbrb2E72H8JCnOaoIAkK9wXvwAkeOHInehUewIoBjx44Fv0kXgwAtJUwkhugFIGgR0Btz6tSpoMullEQZDEJpkShF0CJg5KMkSDwcYljEOR86dCi4kmgxQYsAEMLBgwejd+GA97t48WLQYZBDRfDWAFg9debMmegT/8EDsGrsxIkTQYdBjuBFABgCk0ShhETkAXg/FcA7VAQRIYUE5AGhLJhJgoogAhFQK/e9UsT5aR7QHhVBAeyx4/MEGud19+5dDYOKUBEUgHEcPnzY22cbuFxAaY+KoAQ+Ggph0NGjR6N3SiEqgiKIlVmUT/LoS36AV7t8+bIN9TQX+BAVQQkwFGLnzZs3m9evX4sOjTh2Ev7QWyPiUBHEgBikh0bkNyFsqlsLKoIYMBz6irZs2SLWG+zYsUM9QAVUBAlg2eHjx49FCYFjZTfu0JvjkqAiSAAeged2SYNj1jCoMiqChOAJJMHS0efPn0fvlDhUBB5Dd6x6gsqoCBLie09RyKgIEkByOXTo0OidHLp37x69UuJQESRk8ODBohblt7a2mn79+ml1KAHNepHioe9+2rRppkuXLtEncsB76f2tjIogBgQwevRoM336dHFbs3C8o0aNUm9QAa6NhkNlcAJYsmSJ2L2JOO6WlhbTq1cvFUIZrAh0pGiPW3a4ePFi8/nnn4sVgIMS6YoVK8ygQYN0SWUJuD5Nly5daqXLUPtL3jFp0iQzdepU23YgXQCFUOKlnZodJpR3MPiPGDHCNA8fPjz6KEy4ELRL9+zZ03zzzTc2/gefBABv3rwx48ePN19//bX1COr939178qamtze7df/+/ebBgwdBzS46Ixg7dqyZMGGCGTBggDWUEOA82YadXagh1FllbGDjxo2m6e0FaeUhbaytDSEk4sT5IhciaXSlT99G/koQ7tEdy70/f/68fY0YQhEENsAAuGDBAtP09uRb+XDr1q32m77ijJ+Kz+TJk4Ma+eNw7eFsw3LhwgUbEYDvYiAkXLVqlRk4cOA7EXAhSI4ZFXw7eQyfcxo3bpw1fmJ/ZlNDG/krgQ3Qeeoe38p27ay19lUM2MH69eutHbR5gmfPnpk9e/Z4ExJh/EClh4oPYY8afjKcd2BgvHXrlh01EYMvgsA2Zs6caQfFdiKghLZr1y7xT3V0Iz+Gr8ZfG04MCIG84cmTJ16IARuhEujOo00Eju3bt7eNohLgWPmi/Ddr1iw1+jriQiYGShbv82wHvISk6IHj3bBhg+natWv0SZEIOEn2rJewZbcz/iFDhtgMv0ePHmr8HQi24h78jZeQMHByjMycf/HFF+2KIu16hzAiEkgJ0A+zZs0ae0KoWgXQsXC9CSeIq7/99ls7wuYdRDBlyhRbGCnkg3DIgVLYaqTRHoGL60Z7Mnota8rAeQqe+9DoR8NiQ/SBMTtcarAs20XarVs3m1g20s1x4Th4N9qrAOTgPMWcOXNsf04jPUXfvn3LCgDKioB/wGKSRomAv0ufCwevxi8X7IiBjNl5RNHR9oT4KJjEEbuegErL7NmzO1zF/L0+ffrY1+XUq8iBQYyQltxh4sSJtmGxI0BwtMewwi7OjsrmBIV0VNmUv8EXwnMTGYpfMB/FQwPZ47XeNsVgWlwOLUXFlWUkOPPmzau7N+D3U/HhoFUA/oJX6N+/v/UK9fYIzB1ROq9ERRFgjDSd4VbqBQIgCaeXQ+v9/sP95WvZsmVt3r8ezJ07N5EtVRQBoF68Qda4kydpcgesAggHih5fffWVLcNnLQQGVqKYJCQSAeDCsjxQF/5wEUiatAIUHgx4eH6qgFmvgSZySTqgJhYBvzCriTNOlnht9erVNlHS0T9c3L1nLgibyEoIaSKXxCIAdmCo9SD598w/LFy4MPpEUd6F3HQFYBtZ2BiLppKSqERaTDUlUyoBixYtsut5NfRR4iA6OH36tG25SBt9YGfr1q1rm2dKQipPACQbS5cuTaVWflYFoCQFG2HXj7ThET87Y8aMVF4AUouAGI4kBreVBA6Mn1UBKGnAzgiPkm4Ox8/QI4R40tpZahGAU2qlg+P7zDGgThWAkhaEQPk8aUi0fPnyqoosVYkAkvwxSqDs5akCUKqF/rVKBRm+hwCSzA6XomoRQNzBkaDMnz8/eqco1cFgSwMc+UGpyAP7o9ugUpNcHDWJgFCnVEmLg3UJSrUHpigOIgnaoYsX5mBn5AysWagl2qiqRFoMB7Nz504rBl7jIdjdSwWgZAmVSVY7YmPYGstr6WSo1c5q8gQO4ra1a9dapZLEjBw5UgWg1AX2kSLUZqFOFgKATDwBoFK28uMB0hKf7KLIAAGw3UuWNpaZCMB17akAlHqCnWVpY5mKQFEkkklOoCiSUREowaMiUIJHRaAEj4pACR4VgRI4xvwPmYiAC4AtHyIAAAAASUVORK5CYII="></img>
                    <img
                      width="154"
                      height="187"
                      src={require("./../../../images/profileDefault1.jpg")}
                    ></img>
                  )} */}
          </div>
          <div className="dash_profile_name">
            {vendorState.data.length>0 ? vendorState.data[0]["vendorName"] : ""}
          </div>
          <aside
            className="bg-black dk nav_side-xs aside hidden-print"
            id="nav"
<<<<<<< HEAD
            style={{ backgroundColor: "#08679d" }}
=======
            style={{ backgroundColor: "#08679d",paddingTop:40 }}
>>>>>>> 39419b1 (merged-on-10032021)
          >
            <section className="vbox">
              <section className="w-f-md scrollable">
                <div>
                  <div
                    data-height="auto"
                    data-disable-fade-out="true"
                    data-distance={0}
                    data-size="10px"
                    data-railopacity="0.2"
                  >
                    {/* nav */}
                    <div
                      id="divexample1"
                      style={{ overflow: "hidden" }}
                      tabIndex={0}
                    >
                      <nav className="nav_side-primary nav_pad">
                        <ul className="nav_side" data-ride="collapse">
                          <li>
                            <a
                              onClick={() => {
                                props.history.push("/");
                              }}
                              className={
                                cl == "/"
                                  ? "active _cursor-pointer"
                                  : "_cursor-pointer"
                              }
                            >
                              {" "}
                              <i className="fa fa-home" aria-hidden="true" />
                              <div className="menus">Home </div>
                            </a>
                          </li>
                          <li>
                            <a
                              onClick={() => {
                                navigation(`${url}`);
                              }}
                              className={
                                cl == "/vendor"
                                  ? "active _cursor-pointer"
                                  : "_cursor-pointer"
                              }
                            >
                              {" "}
                              <i
                                className="fa fa-th-large"
                                aria-hidden="true"
                              />
                              <div className="menus">Dashboard </div>
                            </a>
                          </li>
                          <li>
                            <a
                              onClick={() => {
                                navigation(`${url}/jobs-applied`);
                              }}
                              className={
                                cl.includes("/vendor/jobs-applied")
                                  ? "active _cursor-pointer"
                                  : "_cursor-pointer"
                              }
                            >
                              {" "}
                              <i
                                className="fa fa-briefcase"
                                aria-hidden="true"
                              />
                              <div className="menus">Jobs Applied</div>
                            </a>
                          </li>
                          {/* <li>
                            <a
                              onClick={() => {
                                navigation(`${url}/digilocker/${3}`);
                              }}
                              className={
                                cl.includes("/candidate/digilocker")
                                  ? "active _cursor-pointer"
                                  : "_cursor-pointer"
                              }
                            >
                              {" "}
                              <i className="fa fa-lock" aria-hidden="true" />
                              <div className="menus">
                                {basicInfo.fullName != undefined
                                  ? basicInfo.fullName.split(" ")[0]
                                  : ""}
                                's DigiLocker
                              </div>
                            </a>
                          </li>
                          <li>
                            <a
                              onClick={() => {
                                navigation(`${url}/my-resume`);
                              }}
                              className={
                                cl.includes("/candidate/my-resume")
                                  ? "active _cursor-pointer"
                                  : "_cursor-pointer"
                              }
                            >
                              {" "}
                              <i className="fa fa-files-o" aria-hidden="true" />
                              <div className="menus">My Resumes</div>
                            </a>
                          </li> */}
                          <li>
                            <a
                              onClick={() => {
                                navigation(`${url}/messages`);
                              }}
                              className={
                                cl.includes("/candidate/messages")
                                  ? "active _cursor-pointer"
                                  : "_cursor-pointer"
                              }
                            >
                              {" "}
                              <i
                                className="fa fa-commenting-o"
                                aria-hidden="true"
                              />
                              <div className="menus">Messages</div>
                            </a>
                          </li>
                          <li>
                            <a
                              onClick={() => {
                                navigation(`${url}/saved-jobs`);
                              }}
                              className={
                                cl.includes("/vendor/saved-jobs")
                                  ? "active _cursor-pointer"
                                  : "_cursor-pointer"
                              }
                            >
                              {" "}
                              <i
                                className="fa fa-suitcase"
                                aria-hidden="true"
                              />
                              <div className="menus">Saved Jobs</div>
                            </a>
                          </li>
                          <li>
                            <a
                              href={`#${url}/my-profile`}
                              className={
                                cl.includes(`${url}/my-profile`)
                                  ? "active _cursor-pointer"
                                  : "_cursor-pointer"
                              }
                            >
                              {" "}
                              <i className="fa fa-user-o" aria-hidden="true" />
                              <div className="menus">My Profile</div>
                            </a>
                          </li>
                        </ul>
                        <ul className="nav_side nav_side1" data-ride="collapse">
                          <li>
                            <Link to="/help">
                              {" "}
                              <i
                                className="fa fa-question-circle"
                                aria-hidden="true"
                              />
                              <div className="menus">Help</div>
                            </Link>
                          </li>
                          <li>
                            <a onClick={onLogout} className="_cursor-pointer">
                              {" "}
                              <i
                                className="fa fa-power-off"
                                aria-hidden="true"
                              />
                              <div className="menus">Log Out </div>
                            </a>
                          </li>
                        </ul>
                      </nav>
                      {/* / nav */}
                    </div>
                  </div>
                </div>
              </section>
            </section>
          </aside>
        </div>
      </div>
    </>
  );
}

export default withRouter(CandidateLayoutSidebar);
