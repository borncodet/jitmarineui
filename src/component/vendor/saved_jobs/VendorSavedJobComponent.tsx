<<<<<<< HEAD
import { DatePicker, Form, Input, Radio } from "antd";

import React, { useState } from 'react'
import { Modal } from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import { FacebookShareButton, LinkedinShareButton, TwitterShareButton } from "react-share";
import { getLoggedUserId, useMyProfileContext, useMyProfileDispatcher } from "../../../action/MyProfileAction";
import { getVendorLoggedUserId, useVendorMyProfileContext, useVendorMyProfileDispatcher } from "../../../action/MyProfileVendorAction";
import { setVendorJobAppliedSave, useVendorJobAppliedContext, useVendorJobAppliedDispatcher } from "../../../action/vendor/VendorJobAppliedAction";
import { getVendorJobBookMarkList, setVendorJobBookmark, useVendorJobBookMarkContext, useVendorJobBookMarkDispatcher } from '../../../action/vendor/VendorJobBookMarkAction';
import { VendorCandidateList, VendorGetAllJobRequestModel, VendorJobAppliedRequestModel } from '../../../models/vendor/VendorJobApplied';
import { VendorJobBookMarkRequestModel } from "../../../models/vendor/VendorJobBookMark";
import AuthService from "../../../services/AuthService";

const  VendorSavedJobComponent=()=> {
  // const authorizationToken='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8';
  // let loggedVendorId=1;
  
  const vendorJobBookMarkDispatcher = useVendorJobBookMarkDispatcher();
  const vendorJobBookMarkContext = useVendorJobBookMarkContext();
  const { bookmarkSaveResult,vendorGetAllJobBookMark } = vendorJobBookMarkContext;
  
  const jobAppliedVendorDispatcher = useVendorJobAppliedDispatcher();
  const jobAppliedVendorContext = useVendorJobAppliedContext();
  const { vendorJobAppliedSaveRespond,vendorGetAllJobList } = jobAppliedVendorContext;
=======
import { DatePicker, Form, Input, Popconfirm, Radio } from "antd";

import React, { useEffect, useRef, useState } from "react";
import { Modal } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import moment from "moment";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
} from "react-share";
import {
  getLoggedUserId,
  useMyProfileContext,
  useMyProfileDispatcher,
} from "../../../action/MyProfileAction";
import {
  getVendorLoggedUserId,
  useVendorMyProfileContext,
  useVendorMyProfileDispatcher,
} from "../../../action/MyProfileVendorAction";
import {
  setVendorJobAppliedSave,
  useVendorJobAppliedContext,
  useVendorJobAppliedDispatcher,
} from "../../../action/vendor/VendorJobAppliedAction";
import {
  getVendorJobBookMarkList,
  setVendorJobBookmark,
  useVendorJobBookMarkContext,
  useVendorJobBookMarkDispatcher,
} from "../../../action/vendor/VendorJobBookMarkAction";
import {
  DeleteVendorJobStatusList,
  getVendorJobStatusList,
  useVendorJobStatusContext,
  useVendorJobStatusDispatcher,
} from "../../../action/vendor/VendorJobStatusAction";
import {
  VendorCandidateList,
  VendorGetAllJobRequestModel,
  VendorJobAppliedRequestModel,
} from "../../../models/vendor/VendorJobApplied";
import { VendorJobBookMarkRequestModel } from "../../../models/vendor/VendorJobBookMark";
import { JobStatusGetAllRequestModel } from "../../../models/vendor/VendorJobStatus";
import AuthService from "../../../services/AuthService";

interface ReactSelectOption {
  value: string;
  label: string;
}
interface IVendorSavedJobComponentProps {}
interface IVendorSavedJobComponentState {
  jobCategorys: string;
  jobTypeData: string;
  location: string;
  // tempData: jobAlertTitleDropdownResult[];
  countNext: number;
  carouselValue: number;
  countNextStatus: number;
  carouselValueStatus: number;
  firstName: string;
  middleName: string;
  lastName: string;
  passportNo: string;
  dob: number;
  activeVisa: string;
  visaIssueCountry: string;
  expiryDate: number;
  currentJobId: number;
  filterFlag: boolean;
}
const defaultValues = {
  jobTypeData: "",
  jobCategorys: "",
  tempData: [],
  location: " ",
  countNext: 0,
  carouselValue: 0,
  countNextStatus: 0,
  carouselValueStatus: 0,
  firstName: "",
  middleName: "",
  lastName: "",
  passportNo: "",
  dob: 0,
  activeVisa: "",
  visaIssueCountry: "",
  expiryDate: 0,
  currentJobId: 0,
  filterFlag: true,
};
const VendorSavedJobComponent: React.FC<IVendorSavedJobComponentProps> = (
  props
) => {
  // const authorizationToken='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8';
  // let loggedVendorId=1;

  const [
    vendorSavedJobComponentState,
    setVendorSavedJobComponentState,
  ] = React.useState<IVendorSavedJobComponentState>(defaultValues);

  const {
    carouselValue,
    countNext,
    currentJobId,
    carouselValueStatus,
    countNextStatus,
  } = vendorSavedJobComponentState;

  const vendorJobBookMarkDispatcher = useVendorJobBookMarkDispatcher();
  const vendorJobBookMarkContext = useVendorJobBookMarkContext();
  const {
    bookmarkSaveResult,
    vendorGetAllJobBookMark,
  } = vendorJobBookMarkContext;

  const jobAppliedVendorDispatcher = useVendorJobAppliedDispatcher();
  const jobAppliedVendorContext = useVendorJobAppliedContext();
  const {
    vendorJobAppliedSaveRespond,
    vendorGetAllJobList,
  } = jobAppliedVendorContext;
>>>>>>> 39419b1 (merged-on-10032021)

  const myVendorProfileDispatcher = useVendorMyProfileDispatcher();
  const myVendorProfileContext = useVendorMyProfileContext();
  const {
    basicInfo,
<<<<<<< HEAD
     loggedVendorId,
=======
    loggedVendorId,
>>>>>>> 39419b1 (merged-on-10032021)
    myProfile,
    myProfileProgressBar,
    profileImage,
  } = myVendorProfileContext;

<<<<<<< HEAD
console.log(4435343,loggedVendorId)
let history = useHistory();

 const authorizationToken = AuthService.accessToken;

const secondButtonRef = React.useRef<HTMLButtonElement | null>(null)
const [jobApplayOpen, setJobApplayOpen] = useState<boolean>(false);
const [filterFlag, setFilterFlag] = useState<boolean>(true);
const [radioValue2, setRadioValue2] = useState<number>(1000);
const [radioIndexList, setRadioIndexList] = useState<any[]>([])
const [radioValue, setRadioValue] = useState<number>(1);
const [currentJobIdValue, setCurrentJobIdValue] = useState<number>(0);
const [isShareOpen, setIsShareOpen] = useState<boolean>(false);

let user = AuthService.currentUser;
React.useEffect(() => {
  if (authorizationToken != null && user?.id != null)
    (async () => {
      await getVendorLoggedUserId(
        myVendorProfileDispatcher,
        parseInt(user.id),
        authorizationToken
      );
    })();
}, [authorizationToken]);

React.useEffect(() => {
   if (authorizationToken != null) {
    (async () => {
      await getVendorJobBookMarkList(
        vendorJobBookMarkDispatcher,
        {
          vendorId:loggedVendorId,
         page:1,
         pageSize:10,
         searchTerm:"",
          showInactive:false,
          sortOrder:"",
        } as VendorGetAllJobRequestModel,
         authorizationToken
        // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
      );
    })();
   }
}, [bookmarkSaveResult]);

const handleSavedJobClick = (id: any) => {
    
  // setVendorDashboardComponentState({
  //   ...vendorDashboardComponentState,
  //   currentJobId: id, 
  // });
   if(authorizationToken!=null){
  (async () => {
    await setVendorJobBookmark(
      vendorJobBookMarkDispatcher,
      {
       IsActive:true,
       jobBookmarkedId:0,
       rowId:0,
       vendorID:loggedVendorId,
       jobId:id
      } as VendorJobBookMarkRequestModel,
       authorizationToken
      // "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImQ5ZjU0MjlmLTY3NTgtNGM0Ny04ODVhLTIwZjVkN2M4ODEyZiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTcyODEsImV4cCI6MTY0NDEzMzI4MSwiaWF0IjoxNjEyNTk3MjgxLCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.sKevXWje7UM90S8rAZicVHpesgOz4IepSaY2JQkvcNM"
    );
  })();
}
};

const handleJobAppliedClick=(id:any)=>{
   
  if( secondButtonRef.current!=null){
  secondButtonRef.current.click()
}
  setJobApplayOpen(!jobApplayOpen);
  setCurrentJobIdValue(id);

}

const onFinish = (values:any) => {
  console.log('Received values of form:', values);

  let vendorCandidateListArray=[{rowId:0,dob:new Date(values.dob),
    expDate:values.expiryDate!=undefined?values.expiryDate._d:null,firstName:values.firstName,isActive:true,
    isActiveVisa:values.visaIssueCountry===''?false:true,lastName:values.lastName,middleName:values.middleName,
    passPortNumber:values.passportNo,visaIssueCountry:values.visaIssueCountry
}] as VendorCandidateList[] ;

  if(values.users!=undefined && values.users.length>0){
    

    values.users.forEach((user:any) => {
      vendorCandidateListArray.push({rowId:0,dob:new Date(user.dob),visaIssueCountry:user.visaIssueCountry,
        passPortNumber:user.passportNo,middleName:user.middleName,lastName:user.lastName,
        isActiveVisa:user.visaIssueCountry===''?false:true,
        isActive:true,firstName:user.firstName,expDate:user.expiryDate!=undefined?user.expiryDate._d:null
      })
    });
  }
   if (authorizationToken != null)
 {
  
    (async () => {
      await setVendorJobAppliedSave(
        jobAppliedVendorDispatcher,
        {
         jobId:1,
         rowId:0,
         vendorCandidateList:vendorCandidateListArray,
         vendorId:loggedVendorId
        } as VendorJobAppliedRequestModel,
        // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        authorizationToken
        );
    })();
 }

};

React.useEffect(() => {
  // if (authorizationToken != null) {
    setJobApplayOpen(false)
  // }
}, [vendorJobAppliedSaveRespond]);



const handleRemoveIndex=(indexValue:any)=>{
  const item=  radioIndexList.filter((value,index)=>{ 
    return(
 value!=indexValue)
    })
    setRadioIndexList(item);
 }

 const handleOnRadioButtonChange=(e:any)=>{
    
  if(e.target.value.charAt(0)=='f'){
   
  const item=  radioIndexList.filter((value,index)=>{ 
    return(
 value!=parseInt(e.target.value.substring(1)))
    })
    setRadioIndexList(item);
  }
  else{
  setRadioIndexList((radioIndexList:any) => [...radioIndexList, parseInt(e.target.value.substring(1))])
  }
}

const handleShareButtonClick = (id: any, value: any) => {
 setCurrentJobIdValue(id)
 setFilterFlag(value)
  setIsShareOpen(!isShareOpen);
};


  return (
    <>
       <div className="content-wrapper">
        <div className="container-fluid">    
          <h1 className="heading">Saved jobs</h1>
          <div className="clearfix" />
          <div className="row "> 
            <div className="col-sm-12"> 
              <div className="section_box3">  
                <div className="row">
                  {vendorGetAllJobBookMark.data!=undefined?vendorGetAllJobBookMark.data.map((data,index)=>{
                    return(
 <div className="col-sm-3"> 
 <div className="matched_jobs">
   <div className="matched_jobs_cat_t">india</div> 
   <div className="jobs_start1">5 days ago</div> 
   <div className="matched_star1"><i className="fa fa-star" aria-hidden="true"  onClick={()=>handleSavedJobClick(data.jobId)}/></div> 
   <div className="clearfix" />
   <div onClick={()=>{history.push(`/job_search_vendor/saved/${data.jobId}`)}}>
   <div className="matched_heading">{data.title}</div>
   <div className="matched_jobs_cat"><i className="fa fa-map-marker" aria-hidden="true" />  San Fransisco</div> 
   <div className="matched_jobs_cat text-right"><i className="fa fa-clock-o" aria-hidden="true" /> {data.jobType}</div> 
   <div className="clearfix" />
   <div className="matched_jobs_cat"><i className="fa fa-list-ul" aria-hidden="true" />  {data.numberOfVacancies} Vecancies</div>
  </div>
   <div className="clearfix" />
   <div className="matched_jobs_share"><a onClick={() =>
            handleShareButtonClick(
              data.jobId,
              true
            )
          } data-target="#share" data-toggle="modal"><i className="fa fa-share-alt" aria-hidden="true" /></a></div>
   <div className="jobs_apply"><a onClick={()=>handleJobAppliedClick(data.jobId)}>Apply Now</a></div>
 </div>  
</div>)
                  }):null}
                 
            
                </div> 
              </div> 
            </div> <Modal
=======
  console.log(4435343, loggedVendorId);
  let history = useHistory();

  const authorizationToken = AuthService.accessToken;

  const secondButtonRef = React.useRef<HTMLButtonElement | null>(null);
  const [jobApplayOpen, setJobApplayOpen] = useState<boolean>(false);
  const [filterFlag, setFilterFlag] = useState<boolean>(true);
  const [radioValue2, setRadioValue2] = useState<number>(1000);
  const [radioIndexList, setRadioIndexList] = useState<any[]>([]);
  const [radioValue, setRadioValue] = useState<number>(1);
  const [currentJobIdValue, setCurrentJobIdValue] = useState<number>(0);
  const [isShareOpen, setIsShareOpen] = useState<boolean>(false);
  const [jobStatusOPen, setJobStatusOPen] = useState<boolean>(false);
  const [statusFilter, setStatusFilter] = useState<number>(0);
  const [filterShow, setFilterShow] = useState<boolean>(false);
  const [isChecked, setIsChecked] = useState(false);
  const [checkedListArray, setCheckedListArray] = useState([] as any[]);
  const [isCheckedAll, setIsCheckedAll] = useState(false);
  const [isRemovedAll, setIsRemovedCheckedAll] = useState(false);
  const [isRenderCheckedList, setIsRenderCheckedList] = useState(true);

  const vendorJobStatusDispatcher = useVendorJobStatusDispatcher();
  const vendorJobStatusContext = useVendorJobStatusContext();
  const {
    vendorGetAllJobStatus,
    vendorJobStatusDeleteResult,
  } = vendorJobStatusContext;

  const [vendorJobStatus, setVendorJobStatus] = useState(
    vendorGetAllJobStatus != undefined &&
      vendorGetAllJobStatus.data != undefined
      ? vendorGetAllJobStatus.data
      : []
  );

  let user = AuthService.currentUser;
  React.useEffect(() => {
    if (authorizationToken != null && user?.id != null)
      (async () => {
        await getVendorLoggedUserId(
          myVendorProfileDispatcher,
          parseInt(user.id),
          authorizationToken
        );
      })();
  }, [authorizationToken]);

  React.useEffect(() => {
    // if (authorizationToken != null) {
    setJobApplayOpen(false);
    setJobStatusOPen(false);
    // }
  }, [vendorJobAppliedSaveRespond]);

  React.useEffect(() => {
    if (authorizationToken != null && jobStatusOPen == true) {
      (async () => {
        await getVendorJobStatusList(
          vendorJobStatusDispatcher,
          {
            JobId: currentJobId,
            jobStatusId: statusFilter,
            Page: 1,
            PageSize: 100,
            SearchTerm: "",
            VendorId: loggedVendorId,
            ShowInactive: false,
            SortOrder: "",
          } as JobStatusGetAllRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
        setFilterShow(false);
      })();
    }
  }, [
    loggedVendorId,
    statusFilter,
    jobStatusOPen,
    vendorJobStatusDeleteResult,
  ]);

  React.useEffect(() => {
    if (authorizationToken != null) {
      (async () => {
        await getVendorJobBookMarkList(
          vendorJobBookMarkDispatcher,
          {
            vendorId: loggedVendorId,
            page: 1,
            pageSize: 100,
            searchTerm: "",
            showInactive: false,
            sortOrder: "",
          } as VendorGetAllJobRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
    }
  }, [
    bookmarkSaveResult,
    vendorJobStatusDeleteResult,
    vendorJobAppliedSaveRespond,
  ]);

  const handleSavedJobClick = (id: any) => {
    // setVendorDashboardComponentState({
    //   ...vendorDashboardComponentState,
    //   currentJobId: id,
    // });
    if (authorizationToken != null) {
      (async () => {
        await setVendorJobBookmark(
          vendorJobBookMarkDispatcher,
          {
            IsActive: true,
            jobBookmarkedId: 0,
            rowId: 0,
            vendorID: loggedVendorId,
            jobId: id,
          } as VendorJobBookMarkRequestModel,
          authorizationToken
          // "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImQ5ZjU0MjlmLTY3NTgtNGM0Ny04ODVhLTIwZjVkN2M4ODEyZiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTcyODEsImV4cCI6MTY0NDEzMzI4MSwiaWF0IjoxNjEyNTk3MjgxLCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.sKevXWje7UM90S8rAZicVHpesgOz4IepSaY2JQkvcNM"
        );
      })();
    }
  };

  const handleJobAppliedClick = (id: any) => {
    if (secondButtonRef.current != null) {
      secondButtonRef.current.click();
    }
    setJobApplayOpen(!jobApplayOpen);
    setCurrentJobIdValue(id);
  };

  const onFinish = (values: any) => {
    console.log("Received values of form:", values);

    let vendorCandidateListArray = [
      {
        rowId: 0,
        dob: values.dob != undefined ? values.dob._d : null,
        expDate: values.expiryDate != undefined ? values.expiryDate._d : null,
        firstName: values.firstName,
        isActive: true,
        isActiveVisa:
          values.visaIssueCountry === "" ||
          values.expiryDate == null ||
          values.visaIssueCountry == undefined ||
          values.expiryDate == undefined
            ? false
            : true,
        lastName: values.lastName,
        middleName: values.middleName,
        passPortNumber: values.passportNo,
        visaIssueCountry: values.visaIssueCountry,
      },
    ] as VendorCandidateList[];

    if (values.users != undefined && values.users.length > 0) {
      values.users.forEach((user: any) => {
        vendorCandidateListArray.push({
          rowId: 0,
          dob: user.dob != undefined ? user.dob._d : null,
          visaIssueCountry: user.visaIssueCountry,
          passPortNumber: user.passportNo,
          middleName: user.middleName,
          lastName: user.lastName,
          isActiveVisa:
            user.visaIssueCountry === "" ||
            user.expiryDate == null ||
            user.visaIssueCountry == undefined ||
            user.expiryDate == undefined
              ? false
              : true,
          isActive: true,
          firstName: user.firstName,
          expDate: user.expiryDate != undefined ? user.expiryDate._d : null,
        });
      });
    }
    if (authorizationToken != null) {
      (async () => {
        await setVendorJobAppliedSave(
          jobAppliedVendorDispatcher,
          {
            jobId: currentJobId,
            rowId: 0,
            vendorCandidateList: vendorCandidateListArray,
            vendorId: loggedVendorId,
          } as VendorJobAppliedRequestModel,
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
          authorizationToken
        );
      })();
    }
  };

  React.useEffect(() => {
    // if (authorizationToken != null) {
    setJobApplayOpen(false);
    // }
  }, [vendorJobAppliedSaveRespond]);

  React.useEffect(() => {
    if (
      vendorGetAllJobStatus != undefined &&
      vendorGetAllJobStatus.data != undefined
    ) {
      setVendorJobStatus(
        vendorGetAllJobStatus != undefined &&
          vendorGetAllJobStatus.data != undefined
          ? vendorGetAllJobStatus.data
          : []
      );
    }
  }, [vendorGetAllJobStatus]);

  function cancel(e: any) {}

  const handleRemoveIndex = (indexValue: any) => {
    const item = radioIndexList.filter((value, index) => {
      return value != indexValue;
    });
    setRadioIndexList(item);
  };

  const handleOnRadioButtonChange = (e: any) => {
    if (e.target.value.charAt(0) == "f") {
      const item = radioIndexList.filter((value, index) => {
        return value != parseInt(e.target.value.substring(1));
      });
      setRadioIndexList(item);
    } else {
      setRadioIndexList((radioIndexList: any) => [
        ...radioIndexList,
        parseInt(e.target.value.substring(1)),
      ]);
    }
  };

  const handleShareButtonClick = (id: any, value: any) => {
    setCurrentJobIdValue(id);
    setFilterFlag(value);
    setIsShareOpen(!isShareOpen);
  };

  const handleJobStatusClick = (jobId: any) => {
    setIsCheckedAll(false);
    setIsRemovedCheckedAll(false);
    setCheckedListArray([]);
    setIsChecked(false);
    setVendorJobStatus([]);
    setJobStatusOPen(!jobStatusOPen);
    setVendorSavedJobComponentState((vendorSavedJobComponentState) => {
      return {
        ...vendorSavedJobComponentState,
        currentJobId: jobId,
      };
    });
  };
  const onFinishStatus = (values: any) => {
    console.log("Received values of form:", values);
    console.log("Received vendorJobStatus", vendorJobStatus);
    let vendorCandidateListArray = [] as VendorCandidateList[];

    //     if(vendorJobStatus.length>0){
    // vendorJobStatus.forEach((data:any) => {
    //         vendorCandidateListArray.push({rowId:data.rowId,dob:data.dob,visaIssueCountry:data.visaIssueCountry,
    //           passPortNumber:data.passportNo,middleName:data.middleName,lastName:data.firstName,
    //           isActiveVisa:data.firstName,isActive:data.isActive,firstName:data.firstName,expDate:data.expDate
    //         })
    //     }
    // )}

    if (values.users != undefined && values.users.length > 0) {
      console.log("hi");
      values.users.forEach((user: any) => {
        vendorCandidateListArray.push({
          rowId: 0,
          dob: user.dob != undefined ? user.dob._d : null,
          visaIssueCountry: user.visaIssueCountry,
          passPortNumber: user.passportNo,
          middleName: user.middleName,
          lastName: user.lastName,
          isActiveVisa:
            user.visaIssueCountry === "" ||
            user.expiryDate == null ||
            user.visaIssueCountry == undefined ||
            user.expiryDate == undefined
              ? false
              : true,
          isActive: true,
          firstName: user.firstName,
          expDate: user.expiryDate != undefined ? user.expiryDate._d : null,
        });
      });
    }
    if (authorizationToken != null) {
      (async () => {
        await setVendorJobAppliedSave(
          jobAppliedVendorDispatcher,
          {
            jobId: currentJobId,
            rowId: 0,
            vendorCandidateList: vendorCandidateListArray,
            vendorId: loggedVendorId,
          } as VendorJobAppliedRequestModel,
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
          authorizationToken
        );
      })();
    }
  };

  React.useEffect(() => {
    if (isChecked) {
      if (checkedListArray.length == vendorGetAllJobStatus.data.length) {
        setIsCheckedAll(true);
        console.log("checkedListArray lenth", checkedListArray.length);
        console.log("vendorJobStatus.length", vendorJobStatus.length);
      } else {
        setIsCheckedAll(false);
      }
      setIsChecked(false);
    }
  }, [isChecked]);

  const menuRef = useRef<any>();
  React.useEffect(() => {
    const handler = (event: any) => {
      if (menuRef.current != undefined) {
        if (
          menuRef &&
          menuRef.current &&
          !menuRef.current.contains(event.target)
        ) {
          setFilterShow(false);
          // setIsProfileOpen(false);
        }
      }
    };

    document.addEventListener("mousedown", handler);
    return () => {
      document.removeEventListener("mousedown", handler);
    };
  }, []);

  let IdArray = [] as any[];

  const handleAllChecked = (e: any) => {
    console.log(12, e.target.checked);
    if (e.target.checked) {
      IdArray = [];
      // digiLockerList
      vendorGetAllJobStatus.data
        // .filter((data) => data.digiDocumentTypeId == documentTypeActiveId)
        .map((d) => {
          IdArray.push(d.candidateId);
        });
      setCheckedListArray(IdArray);
      console.log(23, checkedListArray);
      setIsCheckedAll(e.target.checked);
      console.log("checkedListArray", checkedListArray);
      setIsRemovedCheckedAll(false);
    } else {
      IdArray = [];
      setCheckedListArray(IdArray);
      setIsCheckedAll(e.target.checked);
      console.log("checkedListArray", checkedListArray);
      setIsRemovedCheckedAll(true);
    }
  };

  React.useEffect(() => {
    if (
      vendorGetAllJobStatus != undefined &&
      vendorGetAllJobStatus.data != undefined &&
      isRenderCheckedList
    ) {
      setCheckedListArray(IdArray);
      setIsCheckedAll(false);
      setIsRemovedCheckedAll(false);
      setIsRenderCheckedList(false);
      setVendorJobStatus(
        vendorGetAllJobStatus != undefined &&
          vendorGetAllJobStatus.data != undefined
          ? vendorGetAllJobStatus.data
          : []
      );
    }
  }, [isRenderCheckedList, vendorGetAllJobStatus]);

  const handleChecked = (e: any, id: any) => {
    // setIsChecked(true);
    if (e.target.checked) {
      IdArray.push(...checkedListArray, id);
      setCheckedListArray(IdArray);
      console.log("checkedListArray", checkedListArray);
    } else {
      IdArray = checkedListArray.filter((data) => {
        return data !== id;
      });
      setCheckedListArray(IdArray);
      console.log("checkedListArray", checkedListArray);
    }

    setIsChecked(true);
  };

  const handleJobStatusDelete = () => {
    if (authorizationToken != null) {
      (async () => {
        await DeleteVendorJobStatusList(
          vendorJobStatusDispatcher,
          {
            RowId: 0,
            JobId: currentJobId,
            VendorId: loggedVendorId,
            VendorCandidateList: checkedListArray,
          },
          authorizationToken
          // "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImQ5ZjU0MjlmLTY3NTgtNGM0Ny04ODVhLTIwZjVkN2M4ODEyZiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTcyODEsImV4cCI6MTY0NDEzMzI4MSwiaWF0IjoxNjEyNTk3MjgxLCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.sKevXWje7UM90S8rAZicVHpesgOz4IepSaY2JQkvcNM"
        );
      })();
    }
  };

  const handleAllRemoved = (e: any) => {
    if (e.target.checked) {
      IdArray = [];
      setCheckedListArray(IdArray);
      setIsCheckedAll(false);
      setIsRemovedCheckedAll(true);
      console.log("checkedListArray", checkedListArray);
    }
  };

  function onChangeDate(date: any, dateString: any) {
    console.log(date, dateString);
  }

  return (
    <>
      <div className="content-wrapper">
        <div className="container-fluid">
          <h1 className="heading">Saved jobs</h1>
          <div className="clearfix" />
          <div className="row ">
            <div className="col-sm-12">
              <div className="section_box3">
                <div className="row">
                  {vendorGetAllJobBookMark.data != undefined
                    ? vendorGetAllJobBookMark.data.map((data, index) => {
                        return (
                          <div className="col-sm-3">
                            <div className="matched_jobs">
                              <div className="matched_jobs_cat_t">
                                {data.categoryName}
                              </div>
                              <div className="jobs_start1">
                                {data.days_Ago} days ago
                              </div>
                              <div className="matched_star1">
                                <i
                                  className="fa fa-star"
                                  aria-hidden="true"
                                  onClick={() =>
                                    handleSavedJobClick(data.jobId)
                                  }
                                />
                              </div>
                              <div className="clearfix" />
                              <div
                                onClick={() => {
                                  history.push(
                                    `/job_search_vendor/saved/${data.jobId}`
                                  );
                                }}
                              >
                                <div className="matched_heading">
                                  {data.title}
                                </div>
                                <div className="matched_jobs_cat">
                                  <i
                                    className="fa fa-map-marker"
                                    aria-hidden="true"
                                  />{" "}
                                  {data.location}
                                </div>
                                <div className="matched_jobs_cat text-right">
                                  <i
                                    className="fa fa-clock-o"
                                    aria-hidden="true"
                                  />{" "}
                                  {data.jobType}
                                </div>
                                <div className="clearfix" />
                                <div className="matched_jobs_cat">
                                  <i
                                    className="fa fa-list-ul"
                                    aria-hidden="true"
                                  />{" "}
                                  {data.numberOfVacancies} Vacancies
                                </div>
                              </div>
                              <div className="clearfix" />
                              <div className="matched_jobs_share">
                                <a
                                  onClick={() =>
                                    handleShareButtonClick(data.jobId, true)
                                  }
                                  data-target="#share"
                                  data-toggle="modal"
                                >
                                  <i
                                    className="fa fa-share-alt"
                                    aria-hidden="true"
                                  />
                                </a>
                              </div>
                              <div className="jobs_apply">
                                {/* <a onClick={()=>handleJobAppliedClick(data.jobId)}>Apply Now</a> */}
                                {data.isApplied ? (
                                  <a
                                    onClick={() =>
                                      handleJobStatusClick(data.jobId)
                                    }
                                    data-target="#status_pop"
                                    data-toggle="modal"
                                  >
                                    Status
                                  </a>
                                ) : (
                                  <a
                                    onClick={() =>
                                      handleJobAppliedClick(data.jobId)
                                    }
                                    data-target="#apply_now_pop"
                                    data-toggle="modal"
                                  >
                                    Apply Now
                                  </a>
                                )}
                              </div>
                            </div>
                          </div>
                        );
                      })
                    : null}
                </div>
              </div>
            </div>

            <Modal
>>>>>>> 39419b1 (merged-on-10032021)
              show={jobApplayOpen}
              onHide={() => {
                setJobApplayOpen(!jobApplayOpen);
                setRadioIndexList([]);
              }}
<<<<<<< HEAD
              
            >
               <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
              {/* <form onSubmit={handleSubmit3(handleJobSubmit)} noValidate > */}
=======
            >
              <Form
                name="dynamic_form_nest_item"
                onFinish={onFinish}
                autoComplete="off"
              >
                {/* <form onSubmit={handleSubmit3(handleJobSubmit)} noValidate > */}
>>>>>>> 39419b1 (merged-on-10032021)
                <Modal.Header closeButton>
                  <Modal.Title>Apply Now</Modal.Title>
                </Modal.Header>
                <Modal.Body>
<<<<<<< HEAD
                <div className="candidate_no">C001</div>
        <div className="close_icon _cursor-pointer" style={{display:'none'}}>
          <img
            
            src={require("./../../../images/close_icon.png")}
            width={16}
            height={16}
            style={{marginTop:26}}
          />
        </div>
        <div className="col-sm-4">
        <label htmlFor="email">Candidate First  Name</label>
                <Form.Item name='firstName'   rules={[{ required: true, message: 'First  Name Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4">
        <label htmlFor="email">Middle Name</label>
                <Form.Item name='middleName'   rules={[{ required: true, message: 'Middle Name Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4">
        <label htmlFor="email">Last Name</label>
                <Form.Item name='lastName'   rules={[{ required: true, message: 'Last Name Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4">
        <label htmlFor="email">Passport No</label>
                <Form.Item name='passportNo'   rules={[{ required: true, message: 'Passport No Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4">
        <label htmlFor="email">DOB</label>
                <Form.Item name='dob'   rules={[{ required: true, message: 'DOB Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4">
<div className="form-group">
            <label htmlFor="email">Active Visa?</label>
            <div className="row">
           
                <Form.Item name='activeVisa'   rules={[{ required: false, message: 'DOB Missing ' }]}>  
                <Radio.Group defaultValue={radioValue  } onChange={radioValue==1?()=>{setRadioValue(2)}:()=>{setRadioValue(1)}}> 
                <div className="col-xs-6">
                <Radio value={1}>Yes</Radio>   
                </div> 
                <div className="col-xs-6">
                <Radio value={2}>No</Radio>   
                </div>   
                </Radio.Group>       
              
               </Form.Item>
</div>
              </div></div>


        

              <div className="clearfix" />

              <div className="col-sm-4" style={{display:radioValue!=1?'none':''}}>
        <label htmlFor="email">Visa Issuing Country</label>
                <Form.Item name='visaIssueCountry'   rules={[{ required:radioValue!=1?false: true, message: 'Visa Issuing Country Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4" style={{display:radioValue!=1?'none':''}}>
        <label htmlFor="email">Expiry Date</label>
                <Form.Item name='expiryDate'   rules={[{ required:radioValue!=1?false: true, message: 'expiryDate Missing ' }]}>  
                
                <DatePicker />
               </Form.Item>
</div>

 

       
      <Form.List name="users">
        {(fields, { add, remove }) => (
          <>
            {fields.map((field,index) => (
            
                <div style={{paddingTop:120}}>


                <div className="candidate_no">C00{index+2}</div>
        <div className="close_icon _cursor-pointer">
          <img
            onClick={() => {remove(field.name);handleRemoveIndex(index);}}
            src={require("./../../../images/close_icon.png")}
            width={16}
            height={16}
            style={{marginTop:26}}
          />
        </div>
        <div className="col-sm-4">
        <label htmlFor="email">Candidate First  Name</label>
                <Form.Item   {...field}
                  name={[field.name, 'firstName']}
                  fieldKey={[field.fieldKey, 'firstName']}
                  rules={[{ required: true, message: 'Missing first name' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>


      
<div className="col-sm-4">
        <label htmlFor="email">Middle Name</label>
                <Form.Item  {...field}
                  name={[field.name, 'middleName']}
                  fieldKey={[field.fieldKey, 'middleName']}  rules={[{ required: true, message: 'Middle Name Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4">
        <label htmlFor="email">Last Name</label>
                <Form.Item  {...field}
                  name={[field.name, 'lastName']}
                  fieldKey={[field.fieldKey, 'lastName']}   rules={[{ required: true, message: 'Last Name Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4">
        <label htmlFor="email">Passport No</label>
                <Form.Item  {...field}
                  name={[field.name, 'passportNo']}
                  fieldKey={[field.fieldKey, 'passportNo']}   rules={[{ required: true, message: 'Passport No Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4">
        <label htmlFor="email">DOB</label>
                <Form.Item  {...field}
                  name={[field.name, 'dob']}
                  fieldKey={[field.fieldKey, 'dob']}  rules={[{ required: true, message: 'DOB Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>     



<div className="col-sm-4">
<div className="form-group">
            <label htmlFor="email">Active Visa?</label>
            <div className="row">
           
                <Form.Item  {...field}
                  name={[field.name, 'activeVisa']}
                  fieldKey={[field.fieldKey, 'activeVisa']} initialValue={radioIndexList.includes(index)?false:true}  rules={[{ required: false, message: 'DOB Missing ' }]}>  
                <Radio.Group 
                // onChange={radioValue2!=index?()=>{setRadioValue2(index)}:()=>{setRadioValue2(1000)}} 
                onChange={handleOnRadioButtonChange}
                 defaultValue={`f${index}`}
                > 
                <div className="col-xs-6">
                <Radio   defaultChecked   value={`f${index}`}>Yes</Radio>   
                </div> 
                <div className="col-xs-6">
                <Radio   value={`s${index}`}>No</Radio>   
                </div>   
                </Radio.Group>       
              
               </Form.Item>
</div>
              </div></div>

              <div className="clearfix" />

              <div className="col-sm-4" style={{display:radioIndexList.includes(index)?'none':''}}>
        <label htmlFor="email">Visa Issuing Country</label>
                <Form.Item  {...field}
                  name={[field.name, 'visaIssueCountry']}
                  fieldKey={[field.fieldKey, 'visaIssueCountry']}  rules={[{ required:radioIndexList.includes(index)?false:true, message: 'Visa Issuing Country Missing ' }]}>  
                <Input placeholder=""  />              
               </Form.Item>
</div>

<div className="col-sm-4" style={{display:radioIndexList.includes(index)?'none':''}}>
        <label htmlFor="email">Expiry Date</label>
                <Form.Item   {...field}
                  name={[field.name, 'expiryDate']}
                  fieldKey={[field.fieldKey, 'expiryDate']}  rules={[{ required:radioIndexList.includes(index)?false:true, message: 'expiryDate Missing ' }]}>  
              
               <DatePicker />
               </Form.Item>
</div>
        
                
             
            </div>
              

            ))}
             <div className="col-sm-4">
            <Form.Item>
           
          <div className="form-group m_t_30">
            <label htmlFor="email"></label>
            <button ref={secondButtonRef} className="btn btn-success save-event waves-effect waves-light" type="button"
            onClick={() => add()}
            >Add Candidates</button>
          </div> 
          </Form.Item>       
        </div>
            
          </>
        )}
      </Form.List>
     
  

      
=======
                  <div className="candidate_no">C001</div>
                  <div
                    className="close_icon _cursor-pointer"
                    style={{ display: "none" }}
                  >
                    <img
                      src={require("./../../../images/close_icon.png")}
                      width={16}
                      height={16}
                      style={{ marginTop: 26 }}
                    />
                  </div>
                  <div className="col-sm-4">
                    <label htmlFor="email">Candidate First Name</label>
                    <Form.Item
                      name="firstName"
                      rules={[
                        { required: true, message: "First  Name Missing " },
                      ]}
                    >
                      <Input placeholder="" style={{ height: 39 }} />
                    </Form.Item>
                  </div>

                  <div className="col-sm-4">
                    <label htmlFor="email">Middle Name</label>
                    <Form.Item
                      name="middleName"
                      rules={[
                        { required: true, message: "Middle Name Missing " },
                      ]}
                    >
                      <Input placeholder="" style={{ height: 39 }} />
                    </Form.Item>
                  </div>

                  <div className="col-sm-4">
                    <label htmlFor="email">Last Name</label>
                    <Form.Item
                      name="lastName"
                      rules={[
                        { required: true, message: "Last Name Missing " },
                      ]}
                    >
                      <Input placeholder="" style={{ height: 39 }} />
                    </Form.Item>
                  </div>

                  <div className="col-sm-4">
                    <label htmlFor="email">Passport No</label>
                    <Form.Item
                      name="passportNo"
                      rules={[
                        { required: true, message: "Passport No Missing " },
                      ]}
                    >
                      <Input placeholder="" style={{ height: 39 }} />
                    </Form.Item>
                  </div>

                  <div className="col-sm-4">
                    <label htmlFor="email">DOB</label>
                    <Form.Item
                      name="dob"
                      rules={[{ required: true, message: "DOB Missing " }]}
                    >
                      {/* <Input placeholder=""  />               */}
                      <DatePicker style={{ height: 39, width: 196 }} />
                    </Form.Item>
                  </div>

                  <div className="col-sm-4">
                    <div className="form-group">
                      <label htmlFor="email">Active Visa?</label>
                      <div className="row">
                        <Form.Item
                          name="activeVisa"
                          rules={[{ required: false, message: "DOB Missing " }]}
                        >
                          <Radio.Group
                            defaultValue={radioValue}
                            onChange={
                              radioValue == 1
                                ? () => {
                                    setRadioValue(2);
                                  }
                                : () => {
                                    setRadioValue(1);
                                  }
                            }
                          >
                            <div className="col-xs-6">
                              <Radio value={1}>Yes</Radio>
                            </div>
                            <div className="col-xs-6">
                              <Radio value={2}>No</Radio>
                            </div>
                          </Radio.Group>
                        </Form.Item>
                      </div>
                    </div>
                  </div>

                  <div className="clearfix" />

                  <div
                    className="col-sm-4"
                    style={{ display: radioValue != 1 ? "none" : "" }}
                  >
                    <label htmlFor="email">Visa Issuing Country</label>
                    <Form.Item
                      name="visaIssueCountry"
                      rules={[
                        {
                          required: radioValue != 1 ? false : true,
                          message: "Visa Issuing Country Missing ",
                        },
                      ]}
                    >
                      <Input placeholder="" style={{ height: 39 }} />
                    </Form.Item>
                  </div>

                  <div
                    className="col-sm-4"
                    style={{ display: radioValue != 1 ? "none" : "" }}
                  >
                    <label htmlFor="email">Expiry Date</label>
                    <Form.Item
                      name="expiryDate"
                      rules={[
                        {
                          required: radioValue != 1 ? false : true,
                          message: "expiryDate Missing ",
                        },
                      ]}
                    >
                      <DatePicker style={{ height: 39, width: 196 }} />
                    </Form.Item>
                  </div>

                  <Form.List name="users">
                    {(fields, { add, remove }) => (
                      <>
                        {fields.map((field, index) => (
                          <div style={{ paddingTop: 120 }}>
                            <div className="candidate_no">C00{index + 2}</div>
                            <div className="close_icon _cursor-pointer">
                              <img
                                onClick={() => {
                                  remove(field.name);
                                  handleRemoveIndex(index);
                                }}
                                src={require("./../../../images/close_icon.png")}
                                width={16}
                                height={16}
                                style={{ marginTop: 26 }}
                              />
                            </div>
                            <div className="col-sm-4">
                              <label htmlFor="email">
                                Candidate First Name
                              </label>
                              <Form.Item
                                {...field}
                                name={[field.name, "firstName"]}
                                fieldKey={[field.fieldKey, "firstName"]}
                                rules={[
                                  {
                                    required: true,
                                    message: "Missing first name",
                                  },
                                ]}
                              >
                                <Input placeholder="" style={{ height: 39 }} />
                              </Form.Item>
                            </div>

                            <div className="col-sm-4">
                              <label htmlFor="email">Middle Name</label>
                              <Form.Item
                                {...field}
                                name={[field.name, "middleName"]}
                                fieldKey={[field.fieldKey, "middleName"]}
                                rules={[
                                  {
                                    required: true,
                                    message: "Middle Name Missing ",
                                  },
                                ]}
                              >
                                <Input placeholder="" style={{ height: 39 }} />
                              </Form.Item>
                            </div>

                            <div className="col-sm-4">
                              <label htmlFor="email">Last Name</label>
                              <Form.Item
                                {...field}
                                name={[field.name, "lastName"]}
                                fieldKey={[field.fieldKey, "lastName"]}
                                rules={[
                                  {
                                    required: true,
                                    message: "Last Name Missing ",
                                  },
                                ]}
                              >
                                <Input placeholder="" style={{ height: 39 }} />
                              </Form.Item>
                            </div>

                            <div className="col-sm-4">
                              <label htmlFor="email">Passport No</label>
                              <Form.Item
                                {...field}
                                name={[field.name, "passportNo"]}
                                fieldKey={[field.fieldKey, "passportNo"]}
                                rules={[
                                  {
                                    required: true,
                                    message: "Passport No Missing ",
                                  },
                                ]}
                              >
                                <Input placeholder="" style={{ height: 39 }} />
                              </Form.Item>
                            </div>

                            <div className="col-sm-4">
                              <label htmlFor="email">DOB</label>
                              <Form.Item
                                {...field}
                                name={[field.name, "dob"]}
                                fieldKey={[field.fieldKey, "dob"]}
                                rules={[
                                  { required: true, message: "DOB Missing " },
                                ]}
                              >
                                {/* <Input placeholder=""  />               */}
                                <DatePicker
                                  style={{ height: 39, width: 196 }}
                                />
                              </Form.Item>
                            </div>

                            <div className="col-sm-4">
                              <div className="form-group">
                                <label htmlFor="email">Active Visa?</label>
                                <div className="row">
                                  <Form.Item
                                    {...field}
                                    name={[field.name, "activeVisa"]}
                                    fieldKey={[field.fieldKey, "activeVisa"]}
                                    rules={[
                                      {
                                        required: false,
                                        message: "activeVisa Missing ",
                                      },
                                    ]}
                                  >
                                    <Radio.Group
                                      // onChange={radioValue2!=index?()=>{setRadioValue2(index)}:()=>{setRadioValue2(1000)}}
                                      onChange={handleOnRadioButtonChange}
                                      defaultValue={`f${index}`}
                                    >
                                      <div className="col-xs-6">
                                        <Radio
                                          defaultChecked
                                          value={`f${index}`}
                                        >
                                          Yes
                                        </Radio>
                                      </div>
                                      <div className="col-xs-6">
                                        <Radio value={`s${index}`}>No</Radio>
                                      </div>
                                    </Radio.Group>
                                  </Form.Item>
                                </div>
                              </div>
                            </div>

                            <div className="clearfix" />

                            <div
                              className="col-sm-4"
                              style={{
                                display: radioIndexList.includes(index)
                                  ? "none"
                                  : "",
                              }}
                            >
                              <label htmlFor="email">
                                Visa Issuing Country
                              </label>
                              <Form.Item
                                {...field}
                                name={[field.name, "visaIssueCountry"]}
                                fieldKey={[field.fieldKey, "visaIssueCountry"]}
                                rules={[
                                  {
                                    required: radioIndexList.includes(index)
                                      ? false
                                      : true,
                                    message: "Visa Issuing Country Missing ",
                                  },
                                ]}
                              >
                                <Input placeholder="" style={{ height: 39 }} />
                              </Form.Item>
                            </div>

                            <div
                              className="col-sm-4"
                              style={{
                                display: radioIndexList.includes(index)
                                  ? "none"
                                  : "",
                              }}
                            >
                              <label htmlFor="email">Expiry Date</label>
                              <Form.Item
                                {...field}
                                name={[field.name, "expiryDate"]}
                                fieldKey={[field.fieldKey, "expiryDate"]}
                                rules={[
                                  {
                                    required: radioIndexList.includes(index)
                                      ? false
                                      : true,
                                    message: "expiryDate Missing ",
                                  },
                                ]}
                              >
                                <DatePicker
                                  style={{ height: 39, width: 196 }}
                                />
                              </Form.Item>
                            </div>
                          </div>
                        ))}
                        <div className="col-sm-4">
                          <Form.Item>
                            <div className="form-group m_t_30">
                              <label htmlFor="email"></label>
                              <button
                                ref={secondButtonRef}
                                className="btn btn-success save-event waves-effect waves-light"
                                type="button"
                                onClick={() => add()}
                              >
                                Add Candidates
                              </button>
                            </div>
                          </Form.Item>
                        </div>
                      </>
                    )}
                  </Form.List>
>>>>>>> 39419b1 (merged-on-10032021)
                </Modal.Body>
                <div className="modal-footer  m-t-30">
                  <button
                    className="btn btn-success save-event waves-effect waves-light"
                    type="submit"
                  >
                    Save
                  </button>
                  <button
                    onClick={() => {
<<<<<<< HEAD
                      setJobApplayOpen  (false);
=======
                      setJobApplayOpen(false);
>>>>>>> 39419b1 (merged-on-10032021)
                    }}
                    data-dismiss="modal"
                    className="btn btn-default waves-effect"
                    type="submit"
                  >
                    Cancel
                  </button>
                </div>
                <div className="clearfix" />
<<<<<<< HEAD
              {/* </form> */}
              </Form>
            </Modal>


          </div>
          <div className="clearfix" />                      
        </div>  
      
 {/* scoial media share modal statrt */}
 <Modal
              show={isShareOpen}
              onHide={() => {
                setIsShareOpen(!isShareOpen);
              }}
            >
              <Modal.Header closeButton>
                <Modal.Title>Share</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div className="col-sm-12 m_t_30 text-center">
                  <div className="social1">
                  <FacebookShareButton
                    url={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                    quote={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                    hashtag={'efg'}
                    title={"hij"}

                    // quote={`${
                    //   filterFlag
                    //     ? matchedJobVendor != undefined &&
                    //       matchedJobVendor.data != undefined
                    //       ? matchedJobVendor.data.filter(
                    //           (data) => data.rowId === currentJobId
                    //         )[0] != undefined
                    //         ? matchedJobVendor.data.filter(
                    //             (data) => data.rowId === currentJobId
                    //           )[0].description
                    //         : ""
                    //       : ""
                    //     : matchedJobVendor.data != undefined &&
                    //       matchedJobVendor.data != undefined
                    //     ? matchedJobVendor.data.filter(
                    //         (data) => data.rowId === currentJobId
                    //       )[0] != undefined
                    //       ? matchedJobVendor.data.filter(
                    //           (data) => data.rowId === currentJobId
                    //         )[0].description
                    //       : ""
                    //     : ""
                    // }`}
                    // hashtag={`${
                    //   filterFlag
                    //     ? matchedJobVendor != undefined &&
                    //       matchedJobVendor.data != undefined
                    //       ? matchedJobVendor.data.filter(
                    //           (data) => data.rowId === currentJobId
                    //         )[0] != undefined
                    //         ? matchedJobVendor.data.filter(
                    //             (data) => data.rowId === currentJobId
                    //           )[0].description
                    //         : ""
                    //       : ""
                    //     : matchedJobVendor.data != undefined &&
                    //       matchedJobVendor.data != undefined
                    //     ? matchedJobVendor.data.filter(
                    //         (data) => data.rowId === currentJobId
                    //       )[0] != undefined
                    //       ? matchedJobVendor.data.filter(
                    //           (data) => data.rowId === currentJobId
                    //         )[0].description
                    //       : ""
                    //     : ""
                    // }`}
                    className={""}
                  >
                    {/* <FacebookIcon size={36} /> */}
                    <a href="#" className="social_face">
                      <i className="fa fa-facebook" aria-hidden="true" />
                    </a>
                  </FacebookShareButton>

                  <TwitterShareButton
                    url={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                   
                    className={""}
                  >
                    {/* <FacebookIcon size={36} /> */}
                    <a href="#" className="social_twit">
                      <i className="fa fa-twitter" aria-hidden="true" />
                    </a>
                  </TwitterShareButton>

                    <LinkedinShareButton
                    url={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                  
                  
                    source={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                  >
                    <a href="#" className="social_twit">
                      <i className="fa fa-linkedin" aria-hidden="true" />
                    </a>
                  </LinkedinShareButton>
                  </div>
                </div>
                <div className="modal-footer  m-t-30"></div>
              </Modal.Body>
            </Modal>
        {/* social media modal end */}

      </div>
    </>
  )
}

export default VendorSavedJobComponent
=======
                {/* </form> */}
              </Form>
            </Modal>
          </div>
          <div className="clearfix" />
        </div>

        {/* Modal job Status */}

        <Modal
          show={jobStatusOPen}
          onHide={() => {
            setJobStatusOPen(!jobStatusOPen);
          }}
          // style={{width:680}}
        >
          <Form
            name="dynamic_form_nest_item"
            onFinish={onFinishStatus}
            autoComplete="off"
          >
            {/* <form onSubmit={handleSubmit3(handleJobSubmit)} noValidate > */}
            <Modal.Header closeButton>
              <Modal.Title>My Candidate</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {/* filteration */}

              <div className="action_btn1">
                <button
                  type="button"
                  className="actions_bt"
                  onClick={() => {
                    setFilterShow(!filterShow);
                  }}
                >
                  <i
                    className="fa fa-ellipsis-h mdi-toggle-switch-off"
                    aria-hidden="true"
                  />
                </button>
                <div className="col-sm-12">
                  <div
                    className="dash_action"
                    tabIndex={-1}
                    style={{ display: filterShow ? "block" : "none" }}
                  >
                    <div className="action_sec">
                      <ul ref={menuRef}>
                        <li>
                          <a
                            onClick={() => {
                              setStatusFilter(0);
                            }}
                          >
                            All
                          </a>{" "}
                        </li>
                        <li>
                          <a
                            onClick={() => {
                              setStatusFilter(2);
                            }}
                          >
                            {" "}
                            Selected{" "}
                          </a>
                        </li>
                        <li>
                          <a
                            onClick={() => {
                              setStatusFilter(3);
                            }}
                          >
                            Rejected{" "}
                          </a>
                        </li>
                        <li>
                          <a
                            onClick={() => {
                              setStatusFilter(1);
                            }}
                          >
                            In Process
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* filteration end*/}

              <div>
                <div className="candidate_top">
                  <div className="col-sm-4">
                    <input
                      style={{ cursor: "pointer" }}
                      id="checkbox1"
                      type="checkbox"
                      name="checkbox"
                      defaultValue={1}
                      checked={isCheckedAll}
                      // defaultChecked={isCheckedAll ? true : false}
                      onChange={(e: any) => handleAllChecked(e)}
                    />
                    <label htmlFor="checkbox1">
                      <span /> Select All
                    </label>
                  </div>
                  <div className="col-sm-4">
                    <input
                      style={{ cursor: "pointer" }}
                      id="checkbox1"
                      type="checkbox"
                      name="checkbox"
                      defaultValue={1}
                      checked={isRemovedAll}
                      onChange={(e: any) => handleAllRemoved(e)}
                    />
                    <label htmlFor="checkbox1">
                      <span /> De-select All
                    </label>
                  </div>
                  <div className="col-sm-4" style={{ paddingTop: 4 }}>
                    <Popconfirm
                      title={
                        <div>
                          <p>Are you sure delete this Candidate </p>
                        </div>
                      }
                      onConfirm={handleJobStatusDelete}
                      onCancel={cancel}
                      okText="Yes"
                      cancelText="No"
                    >
                      <div
                        style={{ cursor: "pointer" }}
                        //  onClick={handleJobStatusDelete}
                      >
                        <i className="fa fa-trash-o" aria-hidden="true" />{" "}
                        Delete
                      </div>
                    </Popconfirm>
                  </div>
                </div>
                <div className="clearfix" />

                {vendorJobStatus != undefined
                  ? vendorJobStatus.map((data, index) => {
                      return (
                        <>
                          <div style={{ paddingTop: 22 }}>
                            <div className="candidate_no">
                              <div>
                                {" "}
                                <input
                                  id="checkbox1"
                                  type="checkbox"
                                  name="checkbox"
                                  onChange={(e: any) =>
                                    handleChecked(e, data.candidateId)
                                  }
                                  checked={checkedListArray.includes(
                                    data.candidateId
                                  )}
                                />
                                <label htmlFor="checkbox1">
                                  <span />
                                  c00{index + 1}
                                </label>
                              </div>
                            </div>
                            <div className="col-sm-4">
                              <div className="form-group">
                                <label htmlFor="email">
                                  Candidate first Name
                                </label>
                                <input
                                  type="text"
                                  disabled
                                  defaultValue={data.firstName}
                                  className="form-control "
                                  placeholder=""
                                />
                              </div>
                            </div>
                            <div className="col-sm-4">
                              <div className="form-group">
                                <label htmlFor="email">Middle Name</label>
                                <input
                                  type="text"
                                  disabled
                                  defaultValue={data.middleName}
                                  className="form-control "
                                  placeholder=""
                                />
                              </div>
                            </div>
                            <div className="col-sm-4">
                              <div className="form-group">
                                <label htmlFor="email">Last Name</label>
                                <input
                                  type="text"
                                  disabled
                                  defaultValue={data.lastName}
                                  className="form-control "
                                  placeholder=""
                                />
                              </div>
                            </div>
                            <div className="col-sm-4">
                              <div className="form-group">
                                <label htmlFor="email">Passport No</label>
                                <input
                                  type="text"
                                  disabled
                                  defaultValue={data.passPort}
                                  className="form-control "
                                  placeholder=""
                                />
                              </div>
                            </div>
                            <div className="col-sm-4">
                              <div className="form-group">
                                <label htmlFor="email">DOB</label>
                                <DatePicker
                                  style={{ height: 39, width: 196 }}
                                  disabled
                                  onChange={onChangeDate}
                                  defaultValue={moment(data.dob, "DD/MM/YYYY")}
                                  format={"DD/MM/YYYY"}
                                />
                              </div>
                            </div>
                            <div className="col-sm-4">
                              <div className="form-group">
                                <label htmlFor="email">Active Visa?</label>
                                <div className="row">
                                  <div className="col-xs-6">
                                    {/* <input disabled id="radio1" type="radio" name="radio" checked={data.isActivevisa} /><label htmlFor="radio1"><span><span /></span> Yes</label> */}
                                    <Radio
                                      checked={data.isActivevisa}
                                      value={1}
                                    >
                                      Yes
                                    </Radio>
                                  </div>
                                  <div className="col-xs-6">
                                    <Radio
                                      checked={!data.isActivevisa}
                                      value={2}
                                    >
                                      No
                                    </Radio>
                                    {/* <input disabled id="radio1" type="radio" name="radio" defaultValue={1} checked={data.isActivevisa?false:true}/><label htmlFor="radio1"><span><span /></span> No</label> */}
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="clearfix" />
                            <div
                              className="col-sm-4"
                              style={{
                                display: data.isActivevisa ? "" : "none",
                              }}
                            >
                              <div className="form-group">
                                <label htmlFor="email">
                                  Visa Issuing Country{" "}
                                </label>
                                <input
                                  disabled
                                  defaultValue={data.visaIssueCountry}
                                  type="text"
                                  className="form-control "
                                  placeholder=""
                                />
                              </div>
                            </div>
                            <div
                              className="col-sm-4"
                              style={{
                                display: data.isActivevisa ? "" : "none",
                              }}
                            >
                              <div className="form-group">
                                <label htmlFor="email">Expiry date </label>
                                <DatePicker
                                  style={{ height: 39, width: 196 }}
                                  disabled
                                  onChange={onChangeDate}
                                  defaultValue={moment(
                                    data.expirary_Date,
                                    "DD/MM/YYYY"
                                  )}
                                  format={"DD/MM/YYYY"}
                                />
                              </div>
                            </div>
                          </div>

                          <div
                            style={{ paddingTop: data.isActivevisa ? 72 : 1 }}
                          ></div>
                        </>
                      );
                    })
                  : null}

                <Form.List name="users">
                  {(fields, { add, remove }) => (
                    <>
                      {fields.map((field, index) => (
                        <div style={{ paddingTop: 100 }}>
                          <div className="candidate_no">C00{index + 1}</div>
                          <div className="close_icon _cursor-pointer">
                            <img
                              onClick={() => {
                                remove(field.name);
                                handleRemoveIndex(index);
                              }}
                              src={require("./../../../images/close_icon.png")}
                              width={16}
                              height={16}
                              style={{ marginTop: 26 }}
                            />
                          </div>
                          <div className="col-sm-4">
                            <label htmlFor="email">Candidate First Name</label>
                            <Form.Item
                              {...field}
                              name={[field.name, "firstName"]}
                              fieldKey={[field.fieldKey, "firstName"]}
                              rules={[
                                {
                                  required: true,
                                  message: "Missing first name",
                                },
                              ]}
                            >
                              <Input placeholder="" />
                            </Form.Item>
                          </div>

                          <div className="col-sm-4">
                            <label htmlFor="email">Middle Name</label>
                            <Form.Item
                              {...field}
                              name={[field.name, "middleName"]}
                              fieldKey={[field.fieldKey, "middleName"]}
                              rules={[
                                {
                                  required: true,
                                  message: "Middle Name Missing ",
                                },
                              ]}
                            >
                              <Input placeholder="" />
                            </Form.Item>
                          </div>

                          <div className="col-sm-4">
                            <label htmlFor="email">Last Name</label>
                            <Form.Item
                              {...field}
                              name={[field.name, "lastName"]}
                              fieldKey={[field.fieldKey, "lastName"]}
                              rules={[
                                {
                                  required: true,
                                  message: "Last Name Missing ",
                                },
                              ]}
                            >
                              <Input placeholder="" />
                            </Form.Item>
                          </div>

                          <div className="col-sm-4">
                            <label htmlFor="email">Passport No</label>
                            <Form.Item
                              {...field}
                              name={[field.name, "passportNo"]}
                              fieldKey={[field.fieldKey, "passportNo"]}
                              rules={[
                                {
                                  required: true,
                                  message: "Passport No Missing ",
                                },
                              ]}
                            >
                              <Input placeholder="" />
                            </Form.Item>
                          </div>

                          <div className="col-sm-4">
                            <label htmlFor="email">DOB</label>
                            <Form.Item
                              {...field}
                              name={[field.name, "dob"]}
                              fieldKey={[field.fieldKey, "dob"]}
                              rules={[
                                { required: true, message: "DOB Missing " },
                              ]}
                            >
                              {/* <Input placeholder=""  />               */}
                              <DatePicker style={{ height: 39, width: 196 }} />
                            </Form.Item>
                          </div>

                          <div className="col-sm-4">
                            <div className="form-group">
                              <label htmlFor="email">Active Visa?</label>
                              <div className="row">
                                <Form.Item
                                  {...field}
                                  name={[field.name, "activeVisa"]}
                                  fieldKey={[field.fieldKey, "activeVisa"]}
                                  rules={[
                                    {
                                      required: false,
                                      message: "activeVisa Missing ",
                                    },
                                  ]}
                                >
                                  <Radio.Group
                                    // onChange={radioValue2!=index?()=>{setRadioValue2(index)}:()=>{setRadioValue2(1000)}}
                                    onChange={handleOnRadioButtonChange}
                                    defaultValue={`f${index}`}
                                  >
                                    <div className="col-xs-6">
                                      <Radio defaultChecked value={`f${index}`}>
                                        Yes
                                      </Radio>
                                    </div>
                                    <div className="col-xs-6">
                                      <Radio value={`s${index}`}>No</Radio>
                                    </div>
                                  </Radio.Group>
                                </Form.Item>
                              </div>
                            </div>
                          </div>

                          <div className="clearfix" />

                          <div
                            className="col-sm-4"
                            style={{
                              display: radioIndexList.includes(index)
                                ? "none"
                                : "",
                            }}
                          >
                            <label htmlFor="email">Visa Issuing Country</label>
                            <Form.Item
                              {...field}
                              name={[field.name, "visaIssueCountry"]}
                              fieldKey={[field.fieldKey, "visaIssueCountry"]}
                              rules={[
                                {
                                  required: radioIndexList.includes(index)
                                    ? false
                                    : true,
                                  message: "Visa Issuing Country Missing ",
                                },
                              ]}
                            >
                              <Input placeholder="" />
                            </Form.Item>
                          </div>

                          <div
                            className="col-sm-4"
                            style={{
                              display: radioIndexList.includes(index)
                                ? "none"
                                : "",
                            }}
                          >
                            <label htmlFor="email">Expiry Date</label>
                            <Form.Item
                              {...field}
                              name={[field.name, "expiryDate"]}
                              fieldKey={[field.fieldKey, "expiryDate"]}
                              rules={[
                                {
                                  required: radioIndexList.includes(index)
                                    ? false
                                    : true,
                                  message: "expiryDate Missing ",
                                },
                              ]}
                            >
                              <DatePicker style={{ height: 39, width: 196 }} />
                            </Form.Item>
                          </div>
                        </div>
                      ))}
                      <div className="col-sm-4">
                        <Form.Item>
                          <div className="form-group m_t_30">
                            <label htmlFor="email"></label>
                            <button
                              ref={secondButtonRef}
                              className="btn btn-success save-event waves-effect waves-light"
                              type="button"
                              onClick={() => add()}
                            >
                              Add Candidates
                            </button>
                          </div>
                        </Form.Item>
                      </div>
                    </>
                  )}
                </Form.List>

                <div className="clearfix" />

                {/* <div className="col-sm-12">
          <div className="form-group ">
            <label htmlFor="email"></label>
            <button className="btn btn-success save-event waves-effect waves-light" type="button">Add Candidates</button>
          </div>        
        </div> */}
              </div>
            </Modal.Body>
            <div className="modal-footer  m-t-30">
              <button
                className="btn btn-success save-event waves-effect waves-light"
                type="submit"
              >
                Save
              </button>
              <button
                onClick={() => {
                  setJobStatusOPen(false);
                }}
                data-dismiss="modal"
                className="btn btn-default waves-effect"
                type="button"
              >
                Cancel
              </button>
            </div>
            <div className="clearfix" />
            {/* </form> */}
          </Form>
        </Modal>

        {/* end modal job Status  */}

        {/* scoial media share modal statrt */}
        <Modal
          show={isShareOpen}
          onHide={() => {
            setIsShareOpen(!isShareOpen);
          }}
        >
          <Modal.Header closeButton>
            <Modal.Title>Share</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="col-sm-12 m_t_30 text-center">
              <div className="social1">
                <FacebookShareButton
                  url={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                  quote={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                  hashtag={"efg"}
                  title={"facebook"}
                  // quote={`${
                  //   filterFlag
                  //     ? matchedJobVendor != undefined &&
                  //       matchedJobVendor.data != undefined
                  //       ? matchedJobVendor.data.filter(
                  //           (data) => data.rowId === currentJobId
                  //         )[0] != undefined
                  //         ? matchedJobVendor.data.filter(
                  //             (data) => data.rowId === currentJobId
                  //           )[0].description
                  //         : ""
                  //       : ""
                  //     : matchedJobVendor.data != undefined &&
                  //       matchedJobVendor.data != undefined
                  //     ? matchedJobVendor.data.filter(
                  //         (data) => data.rowId === currentJobId
                  //       )[0] != undefined
                  //       ? matchedJobVendor.data.filter(
                  //           (data) => data.rowId === currentJobId
                  //         )[0].description
                  //       : ""
                  //     : ""
                  // }`}
                  // hashtag={`${
                  //   filterFlag
                  //     ? matchedJobVendor != undefined &&
                  //       matchedJobVendor.data != undefined
                  //       ? matchedJobVendor.data.filter(
                  //           (data) => data.rowId === currentJobId
                  //         )[0] != undefined
                  //         ? matchedJobVendor.data.filter(
                  //             (data) => data.rowId === currentJobId
                  //           )[0].description
                  //         : ""
                  //       : ""
                  //     : matchedJobVendor.data != undefined &&
                  //       matchedJobVendor.data != undefined
                  //     ? matchedJobVendor.data.filter(
                  //         (data) => data.rowId === currentJobId
                  //       )[0] != undefined
                  //       ? matchedJobVendor.data.filter(
                  //           (data) => data.rowId === currentJobId
                  //         )[0].description
                  //       : ""
                  //     : ""
                  // }`}
                  className={""}
                >
                  {/* <FacebookIcon size={36} /> */}
                  <a href="#" className="social_face">
                    <i className="fa fa-facebook" aria-hidden="true" />
                  </a>
                </FacebookShareButton>

                <TwitterShareButton
                  url={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                  className={""}
                >
                  {/* <FacebookIcon size={36} /> */}
                  <a href="#" className="social_twit">
                    <i className="fa fa-twitter" aria-hidden="true" />
                  </a>
                </TwitterShareButton>

                <LinkedinShareButton
                  url={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                  source={`http://careerappui.clubactive.in/#/job_search/${currentJobIdValue}`}
                >
                  <a href="#" className="social_twit">
                    <i className="fa fa-linkedin" aria-hidden="true" />
                  </a>
                </LinkedinShareButton>
              </div>
            </div>
            <div className="modal-footer  m-t-30"></div>
          </Modal.Body>
        </Modal>
        {/* social media modal end */}
      </div>
    </>
  );
};

export default VendorSavedJobComponent;
>>>>>>> 39419b1 (merged-on-10032021)
