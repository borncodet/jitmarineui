import React, { useState, useEffect } from "react";
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import {
  useVendorMyProfileContext,
  useVendorMyProfileDispatcher,
} from "../../../action/MyProfileVendorAction";
import AuthService from "../../../services/AuthService";
<<<<<<< HEAD
import { useUserListContext, useMessageListContext} from "../../../context/vendor/VendorMyProfile";
import { isNullOrUndefined } from "util";
import { useMyProfileDispatcher } from "../../../action/MyProfileAction";

=======
import { useUserListContext, useMessageListContext } from "../../../context/vendor/VendorMyProfile";
import { isNullOrUndefined } from "util";
import { useMyProfileDispatcher } from "../../../action/MyProfileAction";
import 'emoji-mart/css/emoji-mart.css'
import { Picker } from 'emoji-mart'
import { Modal } from "react-bootstrap";
import { sendChatFiles } from "../../../apis/vendor"
import AudioReactRecorder, { RecordState } from 'audio-react-recorder'
import ReactAudioPlayer from 'react-audio-player';
import ReactPlayer from 'react-player'
>>>>>>> 39419b1 (merged-on-10032021)

interface IVendorMessageComponentProps { }

interface IVendorMessageComponentState {
<<<<<<< HEAD
      // sendUserName: string;
      // message: string;
      // messages: string[];
      // hubConnection: null;
 }

 const defaultValues = {
      // sendUserName: "",
      // message: "",
      // messages: [],
      // hubConnection: null,
};

const VendorMessageComponent: React.FC<IVendorMessageComponentProps> = (props) => {
  
  const [ VendorMessageComponentState, 
          setVendorMessageComponentState
        ] = React.useState<IVendorMessageComponentState>(defaultValues);

    const authorizationToken = AuthService.accessToken;
    const [message, setMessage] = useState<string>('');
    const [messages, setMessages] = useState<string[]>([]);
    const [sendUserId, setSendUserId] = useState<string>();
    const [sendUserName, setSendUserName] = useState<string>();
    const [receiveUserId, setReceiveUserId] = useState<string>();
    const [receiveUserName, setReceiveUserName] = useState<string>();
    const [filter, setFilter] = useState<string>("");
    const [hubConnection, setHubConnection] = useState<HubConnection>();
    const [list, setList] = useState();
    const [DBMessageList, setDBMessageList] = useState();
    const [messagesList, setMessagesList] = useState();
    const [showing, setShowing] = useState(false);

    const myProfileDispatcher = useMyProfileDispatcher();

    const myProfileVendorDispatcher = useVendorMyProfileDispatcher();
    const myVendorProfileContext = useVendorMyProfileContext();
    const {
      basicInfo,
       loggedVendorId,
      myProfile,
      myProfileProgressBar,
      profileImage,
    } = myVendorProfileContext;
   
  const userListContext = useUserListContext();
  const messageListContext = useMessageListContext();  
=======
  // sendUserName: string;
  // message: string;
  // messages: string[];
  // hubConnection: null;
}

const defaultValues = {
  // sendUserName: "",
  // message: "",
  // messages: [],
  // hubConnection: null,
};

const VendorMessageComponent: React.FC<IVendorMessageComponentProps> = (props) => {

  const [VendorMessageComponentState,
    setVendorMessageComponentState
  ] = React.useState<IVendorMessageComponentState>(defaultValues);

  const authorizationToken = AuthService.accessToken;
  const [message, setMessage] = useState<string>('');
  const [messages, setMessages] = useState<string[]>([]);
  const [sendUserId, setSendUserId] = useState<string>();
  const [sendUserName, setSendUserName] = useState<string>();
  const [receiveUserId, setReceiveUserId] = useState<string>();
  const [receiveUserName, setReceiveUserName] = useState<string>();
  const [filter, setFilter] = useState<string>("");
  const [hubConnection, setHubConnection] = useState<HubConnection>();
  const [list, setList] = useState();
  const [DBMessageList, setDBMessageList] = useState();
  const [messagesList, setMessagesList] = useState();
  const [showing, setShowing] = useState(false);
  const [showEmoji, setShowEmoji] = useState(false);
  const [isOpenFile, setIsOpenFile] = useState<boolean>(false);
  const [isOpenRecorder, setIsOpenRecorder] = useState<boolean>(false);
  const [selectedFiles, setSelectedFiles] = useState();
  const [recordState, setRecordState] = useState();
  const myProfileDispatcher = useMyProfileDispatcher();
  const myProfileVendorDispatcher = useVendorMyProfileDispatcher();
  const myVendorProfileContext = useVendorMyProfileContext();
  const {
    basicInfo,
    loggedVendorId,
    myProfile,
    myProfileProgressBar,
    profileImage,
  } = myVendorProfileContext;

  const userListContext = useUserListContext();
  const messageListContext = useMessageListContext();
>>>>>>> 39419b1 (merged-on-10032021)

  const {
    userList,
    getUserList
  } = userListContext;

  const {
    messageList,
    getMessageList
  } = messageListContext;

<<<<<<< HEAD
   const user = AuthService.currentUser;
 
  console.log('getLoggedUserId-------------------',loggedVendorId);
  console.log('basicInfo-------------------',basicInfo.fullName != undefined ? basicInfo.fullName : "");
  
               
=======
  const user = AuthService.currentUser;

  console.log('getLoggedUserId-------------------', loggedVendorId);
  console.log('basicInfo-------------------', basicInfo.fullName != undefined ? basicInfo.fullName : "");


>>>>>>> 39419b1 (merged-on-10032021)
  const vendorId = loggedVendorId;

  React.useEffect(() => {
    getUserList({
<<<<<<< HEAD
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: ""
      });
  },[vendorId]);

 React.useEffect(() => {
    getMessageList({
        FromUserId:Number(sendUserId),
        ToUserId:Number(receiveUserId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: ""
      });
  },[sendUserId, receiveUserId]);

  React.useEffect(() => {
    setList(         
      userList.data.map((item, index) => {
      const lowercasedFilter = filter.toLowerCase();
      if(String(item["userName"]).toLowerCase().includes(lowercasedFilter)||filter==""){
              return (
                <li><a onClick={()=>handleReceiveUser(item["userId"],item["userName"])} className="active">
                                        <div className="connect_icon"><div className="icon_clr1">A</div></div>
                                        <div className="connect_con">
                                          <div className="connect_con_name"> {item["userName"]}<span className="dot dot-busy" /></div>
                                          <div className="connect_con_des">Admin - Jitmarine</div>
                                        </div>
                                        <div className="chat_time_sec">
                                          <div className="chat_time">2 Min</div>
                                          <div className="connect_con_noti">2</div>
                                        </div>
                                      </a></li>
              );
      }
    })       
    );
  },[filter,userList]);

  React.useEffect(() => {
    setDBMessageList(         
      messageList.data.map((item, index) => {
      if(item["fromUserId"]==sendUserId){
      return (
               <div className="chat_left">
               <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
               <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> 2 Hours</span></div>
               <div className="clearfix" />
               <div className="chat_box_l" key={index}>{item["message"]}</div>
               </div>
           );
      }
      else{
        return (
                <div className="chat_right">
                <div className="chat_icon2"><div className="icon_clr_rs2">B</div></div>
                <div className="chat_name1"><span>2 Hours</span> {receiveUserName}</div>
                <div className="clearfix" />
                <div className="chat_box_r" key={index}>{item["message"]}</div>
                </div>
           );
      }
    })       
    );
  },[receiveUserId,messageList]);


 React.useEffect(() => {
    setMessagesList(         
      messages.map((msg: React.ReactNode, index: number ) =>{
       return (
                              <div className="chat_left">
                              <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                              <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> 2 Hours</span></div>
                              <div className="clearfix" />
                              <div className="chat_box_l" key={index}> {msg}</div>
                              </div>
              );
      })       
    );
  },[messages]);


   console.log(loggedVendorId,'vendorlist------------>',userList);
   console.log(loggedVendorId,'sendUserId------------>',sendUserId);
   console.log(loggedVendorId,'receiveUserId------------>',receiveUserId);
   console.log(loggedVendorId,'messagelist------------>',messageList);

 
   const handleReceiveUser = async (receiveUserId:any,receiveUserName:any) => {
       setReceiveUserId(receiveUserId);
       setReceiveUserName(receiveUserName);
       setMessages([]);
       setShowing(true);
   }

   
    // Set the Hub Connection on mount.
    useEffect(() => {

            // Set the initial SignalR Hub Connection.
            const createHubConnection = async () => {

            const signalR = require("@aspnet/signalr");
           
            // Build new Hub Connection, url is currently hard coded.
            const hubConnect = new HubConnectionBuilder()
                .configureLogging(signalR.LogLevel.Debug)
                .withUrl("https://jitapi.clubactive.in/chathub", {
                   skipNegotiation: true,
                   transport: signalR.HttpTransportType.WebSockets
                })
               .build();
            
               try {
                await hubConnect.start()            
                console.log('Connection successful!!!');
                console.log('getLoggedUserId',loggedVendorId);
                console.log('user',user);
                console.log('user id',user?.id);
                setSendUserId(user?.id);
                setSendUserName(basicInfo.fullName);
                console.log('Basic Info',basicInfo);
                console.log(basicInfo.fullName)
                console.log("authorizationToken",authorizationToken)
               
                // Bind event handlers to the hubConnection.
                hubConnect.on('ReceiveMessage', (sendUserId:string, sendUserName: string, receiveUserId: string, receiveUserName:string, message: string) => {
                    setMessages(m => [...m, `${message}`]);
                    console.log("sendUserId :",`${sendUserId}`);
                    console.log("sendUserName :",`${sendUserName}`);
                    console.log("receiveUserId :",`${receiveUserId}`);
                    console.log("receiveUserName :",`${receiveUserName}`);
                })
                
                hubConnect.on('newuserconnected', (nick: string) => {
                    setMessages(m => [...m, `${nick} has connected.`]);
                })
                // **TODO**
                // hubConnection.off('userdisconnected', (nick: string) => {
                //  
                //     setMessages(m => [...m, `${nick} has disconnected.`]);
                // })
                   }
               catch (err) {
                        alert(err);
                        console.log('Error while establishing connection: ' + { err })
                    }
            setHubConnection(hubConnect);
        }
        createHubConnection();
    }, [loggedVendorId]);

    const handleMessage = async () => {
    await sendMessage()
    setMessage('');
   }

    const onEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      handleMessage();
    }
   }

   /** Send message to server with client's nickname and message. */
    async function sendMessage(): Promise<void> {
        try {
            if (hubConnection && message !== '') {
                await hubConnection.invoke('SendMessage', sendUserId, sendUserName, receiveUserId, receiveUserName, message)

            }
        }
        catch (err) {
            console.error(err);
        }
    } 

   return (
    <React.Fragment>
    <div className="content-wrapper">
=======
      Page: 1,
      PageSize: 10,
      SearchTerm: "",
      SortOrder: ""
    });
  }, [vendorId]);

  React.useEffect(() => {
    getMessageList({
      FromUserId: Number(sendUserId),
      ToUserId: Number(receiveUserId),
      Page: 1,
      PageSize: 10,
      SearchTerm: "",
      SortOrder: "",
      ShowInactive: false,
    });
  }, [sendUserId, receiveUserId]);

  React.useEffect(() => {
    setList(
      userList.data.map((item, index) => {
        const lowercasedFilter = filter.toLowerCase();
        if (String(item["userName"]).toLowerCase().includes(lowercasedFilter) || filter == "") {
          return (
            <li><a onClick={() => handleReceiveUser(item["userId"], item["userName"])} className="active">
              <div className="connect_icon"><div className="icon_clr1">{String(item["userName"]).charAt(0)}</div></div>
              <div className="connect_con">
                <div className="connect_con_name"> {item["userName"]}<span className="dot dot-busy" /></div>
                <div className="connect_con_des">Admin - Jitmarine</div>
              </div>
              <div className="chat_time_sec">
                <div className="chat_time">2 Min</div>
                <div className="connect_con_noti">2</div>
              </div>
            </a></li>
          );
        }
      })
    );
  }, [filter, userList]);

  React.useEffect(() => {
    setDBMessageList(
      messageList.data.map((item: any, index: any) => {
        if (item["fromUserId"] == sendUserId) {
          console.log(item);
          return (
            <div className="chat_left">
              {/* <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
              <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> 2 Hours</span></div> */}
              <div className="chat_icon1"><div className="icon_clr_rs1">{basicInfo.fullName != undefined ? basicInfo.fullName.charAt(0) : ""}</div></div>
              <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> {item["messageTime"]} {item["messageDate"]}</span></div>
              <div className="clearfix" />
              {/* <div className="chat_box_l" key={index}>{item["message"]}</div> */}
              { item["fileType"] == "Text" &&
                <div className="chat_box_l" key={index}>
                  {item["message"]}
                </div>
              }
              { item["fileType"] != null && item["fileType"] == "File" && (item["contentHeader"].includes("image") || item["contentHeader"] == "image/jpeg")
                &&
                <div className="chat_box_l" key={index}>
                  <img src={"data:" + item["contentHeader"] + ";base64," + item["attachment"]}
                    // src={require("../../images/logo.png"
                    className="img-responsive"
                    alt=""
                    width={200} height={200}
                    onClick={() => downloadFile(`https://jitapi.clubactive.in/Upload/ChatAttachment/${item["chatFile"]}`, item["message"])}
                  />
                </div>
              }
              { item["fileType"] != null && item["fileType"] == "File" && (item["chatFile"].includes(".doc") || item["contentHeader"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                &&
                <div className="chat_box_l" key={index}>
                  <img
                    src={require("../../../images/word_document.png")}
                    //className="img-responsive"
                    alt={item["chatFile"]}
                    width={100} height={100}
                    onClick={() => downloadFile(`https://jitapi.clubactive.in/Upload/ChatAttachment/${item["chatFile"]}`, item["message"])}
                  />
                </div>
              }
              { item["fileType"] != null && item["fileType"] == "File" && (item["chatFile"].includes(".pdf") || item["contentHeader"] == "application/pdf")
                &&
                <div className="chat_box_l" key={index}>
                  <img
                    src={require("../../../images/pdf-icon.png")}
                    //className="img-responsive"
                    alt={item["chatFile"]}
                    width={100} height={100}
                    onClick={() => downloadFile(`https://jitapi.clubactive.in/Upload/ChatAttachment/${item["chatFile"]}`, item["message"])}
                  />
                </div>
              }

              {
                item["fileType"] != null && item["fileType"] == "File" && item["contentHeader"].includes("video")
                &&
                <div className="chat_box_l" key={index}>
                  <ReactPlayer url={`https://jitapi.clubactive.in/Upload/ChatAttachment/${item["chatFile"]}`} width="50%" />

                </div>
              }
              {
                item["fileType"] != null && item["fileType"] == "File" && (item["contentHeader"].includes("audio") || item["contentHeader"] == "audio/wav")
                &&
                <div className="chat_box_l" key={index}>
                  <ReactAudioPlayer
                    src={`https://jitapi.clubactive.in/Upload/ChatAttachment/${item["chatFile"]}`}
                    //autoPlay
                    controls
                  />
                </div>
              }
              { item["fileType"] != null && item["fileType"] == "File" && (item["chatFile"].includes(".xls") || item["contentHeader"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                &&
                <div className="chat_box_l" key={index}>
                  <img
                    src={require("../../../images/excel_document.png")}
                    //className="img-responsive"
                    alt={item["chatFile"]}
                    width={100} height={100}
                    onClick={() => downloadFile(`https://jitapi.clubactive.in/Upload/ChatAttachment/${item["chatFile"]}`, item["message"])}
                  />
                </div>
              }
            </div >
          );
        }
        else {
          return (
            <div className="chat_right">
              {/* <div className="chat_icon2"><div className="icon_clr_rs2">B</div></div>
              <div className="chat_name1"><span>2 Hours</span> {receiveUserName}</div> */}
              <div className="chat_icon2"><div className="icon_clr_rs2">{basicInfo.fullName != undefined ? basicInfo.fullName.charAt(0) : ""}</div></div>
              <div className="chat_name1"><span>{item["messageTime"]}  {item["messageDate"]}</span> {receiveUserName}</div>
              <div className="clearfix" />
              <div className="chat_box_r" key={index}>{item["message"]}</div>

            </div>
          );
        }
      })
    );
  }, [receiveUserId, messageList]);


  React.useEffect(() => {
    setMessagesList(
      messages.map((msg: any, index: number) => {
        console.log(msg);
        return (
          <div className="chat_left">
            {/* <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
            <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> 2 Hours</span></div> */}
            <div className="chat_icon1"><div className="icon_clr_rs1">{basicInfo.fullName != undefined ? basicInfo.fullName.charAt(0) : ""}</div></div>
            <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> {new Date().toLocaleString('en', { hour: 'numeric', hour12: true, minute: 'numeric' })}</span></div>
            <div className="clearfix" />
            {/* <div className="chat_box_l" key={index}> {msg}</div> */}
            { typeof (msg) == "string" &&
              < div className="chat_box_l" key={index}> {msg}</div>
            }
            { typeof (msg) == "object" && msg != null && msg["fileHeaders"].includes("image") &&
              <div className="chat_box_l" key={index}>
                <img src={"data:" + msg["fileHeaders"] + ";base64," + msg["fileBinary"]}
                  // src={require("../../images/logo.png"
                  className="img-responsive"
                  alt=""
                  width={200} height={200}
                  onClick={() => downloadFile(`https://jitapi.clubactive.in/Upload/ChatAttachment/${msg["fileName"]}`, msg["fileName"])}
                />
              </div>
            }
            {/*---------  */}
            { typeof (msg) == "object" && msg != null && msg["fileHeaders"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
              &&
              <div className="chat_box_l" key={index}>
                <img
                  src={require("../../../images/word_document.png")}
                  //className="img-responsive"
                  width={100} height={100}
                  onClick={() => downloadFile(`https://jitapi.clubactive.in/Upload/ChatAttachment/${msg["fileName"]}`, msg["fileName"])}
                />
              </div>
            }
            { typeof (msg) == "object" && msg != null && msg["fileHeaders"] == "application/pdf"
              &&
              <div className="chat_box_l" key={index}>
                <img
                  src={require("../../../images/pdf-icon.png")}
                  className="img-responsive"
                  width={100} height={100}
                  onClick={() => downloadFile(`https://jitapi.clubactive.in/Upload/ChatAttachment/${msg["fileName"]}`, msg["fileName"])}
                />
              </div>
            }

            { typeof (msg) == "object" && msg != null && msg["fileHeaders"].includes("video")
              &&
              <div className="chat_box_l" key={index}>
                <ReactPlayer url={`https://jitapi.clubactive.in/Upload/ChatAttachment/${msg["fileName"]}`} width="50%" />

              </div>
            }
            { typeof (msg) == "object" && msg != null && (msg["fileHeaders"].includes("audio") || msg["fileHeaders"] == "audio/wav")
              &&
              <div className="chat_box_l" key={index}>
                <ReactAudioPlayer
                  src={`https://jitapi.clubactive.in/Upload/ChatAttachment/${msg["fileName"]}`}
                  //autoPlay
                  controls
                />
              </div>
            }
            { typeof (msg) == "object" && msg != null && msg["fileHeaders"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
              &&
              <div className="chat_box_l" key={index}>
                <img
                  src={require("../../../images/excel_document.png")}
                  //className="img-responsive"
                  width={100} height={100}
                  onClick={() => downloadFile(`https://jitapi.clubactive.in/Upload/ChatAttachment/${msg["fileName"]}`, msg["fileName"])}
                />
              </div>
            }
            {/* --------- */}
          </div>
        );
      })
    );
  }, [messages]);


  console.log(loggedVendorId, 'vendorlist------------>', userList);
  console.log(loggedVendorId, 'sendUserId------------>', sendUserId);
  console.log(loggedVendorId, 'receiveUserId------------>', receiveUserId);
  console.log(loggedVendorId, 'messagelist------------>', messageList);


  const handleReceiveUser = async (receiveUserId: any, receiveUserName: any) => {
    setReceiveUserId(receiveUserId);
    setReceiveUserName(receiveUserName);
    setMessages([]);
    setShowing(true);
  }


  // Set the Hub Connection on mount.
  useEffect(() => {

    // Set the initial SignalR Hub Connection.
    const createHubConnection = async () => {

      const signalR = require("@aspnet/signalr");

      // Build new Hub Connection, url is currently hard coded.
      const hubConnect = new HubConnectionBuilder()
        .configureLogging(signalR.LogLevel.Debug)
        .withUrl("https://jitapi.clubactive.in/chathub", {
          skipNegotiation: true,
          transport: signalR.HttpTransportType.WebSockets
        })
        .build();

      try {
        await hubConnect.start()
        console.log('Connection successful!!!');
        console.log('getLoggedUserId', loggedVendorId);
        console.log('user', user);
        console.log('user id', user?.id);
        setSendUserId(user?.id);
        setSendUserName(basicInfo.fullName);
        console.log('Basic Info', basicInfo);
        console.log(basicInfo.fullName)
        console.log("authorizationToken", authorizationToken)

        // Bind event handlers to the hubConnection.
        hubConnect.on('ReceiveMessage', (sendUserId: string, sendUserName: string, receiveUserId: string, receiveUserName: string, message: any) => {
          setMessages(m => [...m, message]);
          console.log("sendUserId :", `${sendUserId}`);
          console.log("sendUserName :", `${sendUserName}`);
          console.log("receiveUserId :", `${receiveUserId}`);
          console.log("receiveUserName :", `${receiveUserName}`);
        })

        hubConnect.on('newuserconnected', (nick: string) => {
          setMessages(m => [...m, `${nick} has connected.`]);
        })
        // **TODO**
        // hubConnection.off('userdisconnected', (nick: string) => {
        //  
        //     setMessages(m => [...m, `${nick} has disconnected.`]);
        // })
      }
      catch (err) {
        alert(err);
        console.log('Error while establishing connection: ' + { err })
      }
      setHubConnection(hubConnect);
    }
    createHubConnection();
  }, [loggedVendorId]);

  const handleMessage = async () => {
    await sendMessage()
    setMessage('');
  }

  const onEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      handleMessage();
    }
  }

  const emojiIconClick = () => {
    setShowEmoji(!showEmoji)
  }
  const addEmoji = (e: any) => {
    let emoji = e.native;
    setMessage(message + emoji)
  };

  /** Send message to server with client's nickname and message. */
  async function sendMessage(): Promise<void> {
    try {
      if (hubConnection && message !== '') {
        await hubConnection.invoke('SendMessage', sendUserId, sendUserName, receiveUserId, receiveUserName, message)

      }
    }
    catch (err) {
      console.error(err);
    }
  }

  const selectFile = (event: any) => {
    setSelectedFiles(event.target.files[0]);
    // console.log(event.target.files[0]);
    // console.log(selectedFiles);

    // const formData = new FormData();


    // formData.append('FormFile', event.target.files[0]);
    // formData.append('FileName', event.target.files[0].name);
    // formData.append('SendUserId', String(sendUserId));
    // formData.append('SendUserName', String(sendUserName));
    // formData.append('ReceiveUserId', String(receiveUserId));
    // formData.append('ReceiveUserName', String(receiveUserName));
    //setSelectedFiles(formData);

    // sendChatFiles(formData).then((res) => {
    //   console.log(res.data);
    // }).catch((err) => {
    //   console.log(err);
    // });
  }
  const sendFiles = () => {
    //console.log(selectedFiles);


    if (selectedFiles != undefined) {
      const formData = new FormData();
      formData.append('FormFile', selectedFiles);
      formData.append('FileName', selectedFiles.name);
      formData.append('SendUserId', String(sendUserId));
      formData.append('SendUserName', String(sendUserName));
      formData.append('ReceiveUserId', String(receiveUserId));
      formData.append('ReceiveUserName', String(receiveUserName));
      sendChatFiles(formData).then((res) => {
        //console.log(res.data);
        setSelectedFiles(undefined);
        setIsOpenFile(false);
      }).catch((err) => {
        console.log(err);
      });



    }
  }
  const sendAudioFiles = (data: any) => {
    const formData = new FormData();

    formData.append('FormFile', data.blob, "blob.wav");
    formData.append('FileName', "blob.wav");
    formData.append('SendUserId', String(sendUserId));
    formData.append('SendUserName', String(sendUserName));
    formData.append('ReceiveUserId', String(receiveUserId));
    formData.append('ReceiveUserName', String(receiveUserName));

    // for (var key of (formData as any).entries()) {
    //   console.log(key[0] + ", " + key[1]);
    // }
    sendChatFiles(formData).then((res) => {
      //console.log(res.data);
      setIsOpenRecorder(false)
    }).catch((err) => {
      console.log(err);
    });

  }

  const start = () => {
    setRecordState(RecordState.START
    )
  }

  const stop = () => {
    setRecordState(RecordState.STOP
    )
  }

  //audioData contains blob and blobUrl
  const onStop = (audioData: any) => {
    console.log('audioData', audioData)
    sendAudioFiles(audioData);
  }

  const downloadFile = (filePath: any, fileName: any) => {
    var FileSaver = require('file-saver');
    FileSaver.saveAs(filePath, fileName);

  }


  return (
    <React.Fragment>
      <div className="content-wrapper">
>>>>>>> 39419b1 (merged-on-10032021)
        <div className="container-fluid">
          <h1 className="heading">Message</h1>
          <div className="clearfix" />
          <div className="row ">
<<<<<<< HEAD
             <div className="col-sm-5 col-lg-4 p-r-0">
=======
            <div className="col-sm-5 col-lg-4 p-r-0">
>>>>>>> 39419b1 (merged-on-10032021)
              <div className="panel panel-default panel_n">
                <div className="panel-body panel-body1">
                  <div className="connect_left">
                    <div>
                      <input value={filter} onChange={e => setFilter(e.target.value)} className="form-control" placeholder="Search" />
                      <div className="search_icon"><i className="fa fa-search" aria-hidden="true" /></div>
                    </div>
                    <div className=" m-t-25">
                      <div className="message_chat_scroll" style={{ overflow: 'hidden', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll">
<<<<<<< HEAD
                         
                          <ul>

                       {list}
        
                           
=======

                          <ul>

                            {list}


>>>>>>> 39419b1 (merged-on-10032021)
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
<<<<<<< HEAD
            </div>  
            <div className="col-sm-7 col-lg-8">
              <div className="panel panel-default panel_n"  style={{ display: ({showing} ? 'block' : 'none') }}>
                <div className="panel-body panel-body1">
                  <div className="connect_right">
                    <div className="connect_right_top">
                        {/* {profileImage != undefined && profileImage.data != undefined && profileImage.data.length > 0 ?(
=======
            </div>
            <div className="col-sm-7 col-lg-8">
              <div className="panel panel-default panel_n" style={{ display: ({ showing } ? 'block' : 'none') }}>
                <div className="panel-body panel-body1">
                  <div className="connect_right">
                    <div className="connect_right_top">
                      {/* {profileImage != undefined && profileImage.data != undefined && profileImage.data.length > 0 ?(
>>>>>>> 39419b1 (merged-on-10032021)
                            <div className="chat_icon1"><img
                className="img-responsive"
                src={`https://jitapi.clubactive.in/Upload/VendorProfileImage/${profileImage.data[0]["imageUrl"]}`}
                alt=""
              /></div>
                        ):( 
                           <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                       )} */}
<<<<<<< HEAD
                       <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                      {/* <div className="connect_con_name_r">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}</div> */}
                       <div className="connect_con_name_r">{receiveUserName}</div>
                      <div className="connect_con_ac">Online{showing}</div>
=======
                      {/* <div className="connect_con_name_r">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}</div> */}
                      {receiveUserName != undefined ?
                        (<div>
                          <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                          <div className="connect_con_name_r">{receiveUserName}</div>
                          <div className="connect_con_ac">Online</div></div>) :
                        (<div className="connect_con_name_r">Please select an admin to chat</div>)
                      }
>>>>>>> 39419b1 (merged-on-10032021)
                    </div>
                    <div className=" m-t-10">
                      <div className="message_chat_des_scroll" style={{ overflow: 'revert', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll_r">
<<<<<<< HEAD
                           {DBMessageList}
                           {messagesList}
                           {/* { messages.map((msg: React.ReactNode, index: number ) => (   
=======
                          {DBMessageList}
                          {messagesList}
                          {/* { messages.map((msg: React.ReactNode, index: number ) => (   
>>>>>>> 39419b1 (merged-on-10032021)
                            //  index%2==0 ? (
                              <div className="chat_left">
                              <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                              <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> 2 Hours</span></div>
                              <div className="clearfix" />
                              <div className="chat_box_l" key={index}> {msg}</div>
                              </div>
        
                          //     ) : (
                          //   <div className="chat_right">
                          //   <div className="chat_icon2"><div className="icon_clr_rs2">B</div></div>
                          //   <div className="chat_name1"><span>2 Hours</span> Name 2</div>
                          //   <div className="clearfix" />
                          //   <div className="chat_box_r" key={index}> {msg}</div>
                          //   <div className="chat_icon3"><div className="icon_clr_nr1">A</div></div>
                          //  </div>
                          //  )                         
                           ))} */}
                        </div>
                      </div>
                    </div>
<<<<<<< HEAD
                    <div className="connect_right_bottom">
                      <div className="connect_right_forms"> 
                      <input 
                        type="text" 
                        value={message} 
                        className="form-control" 
                        placeholder="Type message here" 
                        onChange={e => setMessage(e.target.value)}
                        onKeyDown={onEnter}
                      />                     
                      </div>
                      <div className="smile_icon"><img
                        src={require("../../../images/smile.png")}
                      // src="images/smile.png"
                      /></div>
                      <div className="connect_right_icons">
                        <a href="#"><img
                          src={require("../../../images/attach_icon.png")}
                        // src="images/attach_icon.png" 
                        /></a>
                        <a href="#"><img
                          src={require("../../../images/speaker_icon.png")}
                        // src="images/speaker_icon.png" 
                        /></a>
                      </div>
                    </div>
=======
                    {showEmoji &&
                      <div className="connect_right_bottom">
                        <div className="connect_right_forms">
                          <span>
                            <Picker onSelect={addEmoji} />
                          </span>
                        </div>
                      </div>
                    }
                    <div className="connect_right_bottom">
                      <div className="connect_right_forms">
                        <input
                          type="text"
                          value={message}
                          className="form-control"
                          placeholder="Type message here"
                          onChange={e => setMessage(e.target.value)}
                          onKeyDown={onEnter}
                        />
                      </div>
                      {/* <div className="smile_icon"><img
                        src={require("../../../images/smile.png")}
                      // src="images/smile.png"
                      /></div> */}
                      <div className="smile_icon" ><img onClick={() => emojiIconClick()}
                        src={require("../../../images/smile.png")}
                      // src="images/smile.png"
                      />
                      </div>
                      <div className="connect_right_icons">
                        <img
                          src={require("../../../images/attach_icon.png")}
                          onClick={() => setIsOpenFile(true)}
                        />
                        <img
                          src={require("../../../images/speaker_icon.png")}
                          onClick={() => setIsOpenRecorder(true)}
                        />
                      </div>
                    </div>

                    <Modal show={isOpenFile} onHide={() => { setIsOpenFile(!isOpenFile); }}>
                      <Modal.Header closeButton>
                        <Modal.Title>Send File</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>

                        <div className="form-group">
                          <div className="row">
                            <div className="fileUpload btn btn-primary">
                              <span>Upload</span>
                              <input id="uploadBtn" type="file" onChange={selectFile} className="upload" />
                            </div>
                          </div>
                        </div>
                      </Modal.Body>
                      <div className="modal-footer  m-t-30">
                        <button
                          onClick={() => sendFiles()}
                          className="btn btn-danger" type="submit"
                          disabled={selectedFiles ? false : true}
                        >
                          Send</button>
                        <button
                          onClick={() => setIsOpenFile(false)}
                          className="btn btn-default" type="button">cancel</button>
                      </div>
                      <div className="clearfix" />

                    </Modal>


                    <Modal
                      show={isOpenRecorder}
                      onHide={() => {
                        setIsOpenRecorder(!isOpenRecorder);
                      }}
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>Send File</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <div>
                          <AudioReactRecorder state={recordState} onStop={onStop} />

                          <button onClick={start} className="btn btn-primary" >Start</button>
                          <button onClick={stop} className="btn btn-danger" >Stop</button>

                        </div>

                      </Modal.Body>
                      <div className="modal-footer  m-t-30">
                        <button
                          onClick={() => setIsOpenRecorder(false)}
                          className="btn btn-default" type="button">cancel</button>
                      </div>
                      <div className="clearfix" />

                    </Modal>



>>>>>>> 39419b1 (merged-on-10032021)
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="clearfix" />
        </div>
      </div>
    </React.Fragment>
  );
};
<<<<<<< HEAD
export default VendorMessageComponent;
=======
export default VendorMessageComponent;
>>>>>>> 39419b1 (merged-on-10032021)
