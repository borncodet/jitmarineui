import React, { useState, useEffect } from "react";
import Template from "./Template";
import { Link, useHistory, withRouter } from "react-router-dom";
import {
  getCoverLetterTemplates
} from "../../../../apis/resumebuilder";
import { useMyProfileContext } from "../../../../action/MyProfileAction";

interface ICoverLetterSelectTemplateComponentProps { }

interface ICoverLetterSelectTemplateComponentState {

  rowId: number;
  coverLetterTemplateId: number;
  title: string;
  coverLetterTemplateContent: string;
  coverLetterTemplateImage: string;
  description: string;

}

const defaultValues = {
  rowId: 0,
  coverLetterTemplateId: 0,
  title: '',
  coverLetterTemplateContent: '',
  coverLetterTemplateImage: require("./../../../../images/cover_letter.jpg"),
  description: ''
};


const CoverLetterSelectTemplateComponent: React.FC<ICoverLetterSelectTemplateComponentProps> = (props) => {
  const [coverLetterSelectTemplateComponentState, setCoverLetterSelectTemplateComponentState] = React.useState<ICoverLetterSelectTemplateComponentState>(
    defaultValues
  );
  const [coverLetterTemplates, setCoverLetterTemplates] = useState({});
  const myProfileContext = useMyProfileContext();
  const { loggedUserId } = myProfileContext;
  const candidateId = loggedUserId;
  let history = useHistory()
  const handleSelectTemplate = () => {

  };
  const handleEditCoverLetter = () => {
    history.push({
      pathname: '/candidate/my-resume/cover-letter/Edit_Template',
      state: { templateInfo: coverLetterSelectTemplateComponentState }
    });
  };

  useEffect(() => {
    ///(async function () {
    getCoverLetterTemplates({
      "CandidateId": 0,
      "PageIndex": 1,
      "PageSize": 10,
      "ShowInactive": false
    }).then((res) => {
      console.log(res.data)
      setCoverLetterTemplates(res.data);
    }).catch((err) => {
      console.log(err);
    });


    // })();

  }, [candidateId]);


  return (
    <>
      <div className="content-wrapper">
        <div className="container-fluid">
          <h1 className="heading">Select your Cover Letter</h1>
          <div className="clearfix" />

          <div className="row">
            <Template
              CoverLetters={coverLetterTemplates}
              setCoverLetterSelectTemplateComponentState={setCoverLetterSelectTemplateComponentState}
              coverLetterSelectTemplateComponentState={coverLetterSelectTemplateComponentState}
            ></Template>

          </div>
          <div className="row">
            <div className="col-sm-12">
              <button className="btn continue_but"
                disabled={coverLetterSelectTemplateComponentState.rowId == 0 ? true : false}
                onClick={() => { handleEditCoverLetter(); }}>Next</button>
            </div>
          </div>
        </div>
        <div className="clearfix" />
      </div>
    </>
  );
};
export default CoverLetterSelectTemplateComponent;
