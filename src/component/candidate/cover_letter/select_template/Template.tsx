import React, { useState } from 'react'


function Template(props: any) {

  const { CoverLetters, setCoverLetterSelectTemplateComponentState, coverLetterSelectTemplateComponentState } = props;
  const [carouselValue, setCarouselValue] = useState(0);
  const [countNext, setCountNext] = useState(0);

  const handleCoverLetterTemplateClick = (item: any) => {
    setCoverLetterSelectTemplateComponentState(
      {
        rowId: item.rowId,
        coverLetterTemplateId: item.coverLetterTemplateId,
        title: item.title,
        coverLetterTemplateContent: item.coverLetterTemplateContent,
        coverLetterTemplateImage: "https://jitapi.clubactive.in/Upload/CoverLetterImage/" + item.coverLetterTemplateImage,
        description: item.description
      }
    );

  };


  const handleCarouselNextClick = () => {
    let num = CoverLetters.data != undefined ? CoverLetters.data.length : 0;
    let value = num / 4;
    if (countNext < value && value >= 1) {
      setCountNext(countNext + 1);
      setCarouselValue(carouselValue - 225)  //206
    } else {
      setCountNext(0);
      setCarouselValue(0)
    }
  };

  const handleCarouselPreviousClick = () => {
    if (countNext > 0) {
      setCountNext(countNext + 1);
      setCarouselValue(carouselValue + 225)  //206
    } else {
      setCountNext(0);
      setCarouselValue(0)
    }
  };

  return (
    <React.Fragment>
      {/*=================Content Section Start================*/}
      <div className="col-sm-4">
        <div><img
          src={coverLetterSelectTemplateComponentState.coverLetterTemplateImage}
          className="img-responsive" /></div>
        <div><img src={require("./../../../../images/shadow.jpg")} className="img-responsive" /></div>
      </div>
      <div className="col-sm-8">
        <div className="section_box4">
          <div className="select_templates">
            <div className="row">

              <div className="prodict_list">
                <div id="owl-demo1" className="owl-carousel owl-theme" style={{ opacity: 1, display: 'block' }}>
                  <div className="owl-wrapper-outer">
                    <div className="owl-wrapper"
                      style={{
                        width: '2568px',
                        left: '0px',
                        display: 'block',
                        transition: 'all 0ms ease 0s',
                        transform: `translate3d(${carouselValue}px, 0px, 0px)`,
                      }}

                    >
                      {CoverLetters.data != undefined &&
                        CoverLetters.data.map((e: any, i: any) =>
                          <div className="owl-item" style={{ width: "200px" }}>
                            <div className="item">
                              <div className="matched_jobs_sec">
                                <div className="matched_jobs" onClick={() => handleCoverLetterTemplateClick(e)}>
                                  <img
                                    src={`https://jitapi.clubactive.in/Upload/CoverLetterImage/${e.coverLetterTemplateImage}`}
                                    alt="" className="image2" />
                                </div>
                              </div>
                            </div>
                          </div>
                        )}
                    </div>
                  </div>
                  <div className="owl-controls clickable" style={{ display: 'none' }}>
                    <div className="owl-pagination">
                      <div className="owl-page active">
                        <span className="" />
                      </div>
                    </div>
                    <div className="owl-buttons">
                      <div className="owl-prev" onClick={handleCarouselPreviousClick}>
                        <img
                          src={require("../../../../images/products_ar1.png")}
                        />
                      </div>
                      <div className="owl-next" onClick={handleCarouselNextClick}>
                        <img
                          src={require("../../../../images/products_ar2.png")}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/*=================Content Section End================*/}

    </React.Fragment>
  );

};

export default Template