<<<<<<< HEAD
import React from "react";




interface ICandidateMessageComponentProps { }

interface ICandidateMessageComponentState { }

const initialState = {};

const CandidateMessageComponent: React.FC<ICandidateMessageComponentProps> = (props) => {
  const [CandidateMessageComponentState, setCandidateMessageComponentState] = React.useState<ICandidateMessageComponentState>(
    initialState
  );

  return (
    <React.Fragment>
      <div className="content-wrapper">
=======
import React, { useState, useEffect } from "react";
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import {
  useMyProfileContext,
  useMyProfileDispatcher,
} from "../../../action/MyProfileAction";
import AuthService from "../../../services/AuthService";
import { useUserListContext, useMessageListContext} from "../../../context/vendor/VendorMyProfile";
import { isNullOrUndefined } from "util";
import 'emoji-mart/css/emoji-mart.css'
import { Picker } from 'emoji-mart'

interface ICandidateMessageComponentProps { }

interface ICandidateMessageComponentState {
      // sendUserName: string;
      // message: string;
      // messages: string[];
      // hubConnection: null;
 }

 const defaultValues = {
      // sendUserName: "",
      // message: "",
      // messages: [],
      // hubConnection: null,
};

const CandidateMessageComponent: React.FC<ICandidateMessageComponentProps> = (props) => {
  
  const [ CandidateMessageComponentState, 
          setCandidateMessageComponentState
        ] = React.useState<ICandidateMessageComponentState>(defaultValues);

    const authorizationToken = AuthService.accessToken;
    const [message, setMessage] = useState<string>('');
    const [messages, setMessages] = useState<string[]>([]);
    const [sendUserId, setSendUserId] = useState<string>();
    const [sendUserName, setSendUserName] = useState<string>();
    const [receiveUserId, setReceiveUserId] = useState<string>();
    const [receiveUserName, setReceiveUserName] = useState<string>();
    const [filter, setFilter] = useState<string>("");
    const [hubConnection, setHubConnection] = useState<HubConnection>();
    const [list, setList] = useState();
    const [DBMessageList, setDBMessageList] = useState();
    const [messagesList, setMessagesList] = useState();
    const [showing, setShowing] = useState(false);
    const [showEmoji, setShowEmoji] = useState(false);  
    const myProfileDispatcher = useMyProfileDispatcher();
    const myProfileContext = useMyProfileContext();
    const {
      basicInfo,
      loggedUserId,
      myProfile,
      myProfileProgressBar,
      profileImage,
    } = myProfileContext;
   
  const userListContext = useUserListContext();
  const messageListContext = useMessageListContext();  

  const {
    userList,
    getUserList
  } = userListContext;

  const {
    messageList,
    getMessageList
  } = messageListContext;

   const user = AuthService.currentUser;
 
  console.log('getLoggedUserId-------------------',loggedUserId);
  console.log('basicInfo-------------------',basicInfo.fullName != undefined ? basicInfo.fullName : "");
  
               
  const vendorId = loggedUserId;

  React.useEffect(() => {
    getUserList({
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: ""
      });
  },[vendorId]);

 React.useEffect(() => {
    getMessageList({
        FromUserId:Number(sendUserId),
        ToUserId:Number(receiveUserId),
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: "",
        ShowInactive: false
      });
  },[sendUserId, receiveUserId]);

  React.useEffect(() => {
    setList(         
      userList.data.map((item, index) => {
      const lowercasedFilter = filter.toLowerCase();
      if(String(item["userName"]).toLowerCase().includes(lowercasedFilter)||filter==""){
              return (
                <li><a onClick={()=>handleReceiveUser(item["userId"],item["userName"])} className="active">
                                        <div className="connect_icon"><div className="icon_clr1">{String(item["userName"]).charAt(0)}</div></div>
                                        <div className="connect_con">
                                          <div className="connect_con_name"> {item["userName"]}<span className="dot dot-busy" /></div>
                                          <div className="connect_con_des">Admin - Jitmarine</div>
                                        </div>
                                        <div className="chat_time_sec">
                                          <div className="chat_time">2 Min</div>
                                          <div className="connect_con_noti">2</div>
                                        </div>
                                      </a></li>
              );
      }
    })       
    );
  },[filter,userList]);

 React.useEffect(() => {
    setDBMessageList(         
      messageList.data.map((item, index) => {
          console.log("MessageTime",item["messageTime"]);
          console.log("MessageDate",item["messageDate"]);

     if(item["fromUserId"]==sendUserId){
      return (
               <div className="chat_left">
               <div className="chat_icon1"><div className="icon_clr_rs1">{basicInfo.fullName != undefined ? basicInfo.fullName.charAt(0) : ""}</div></div>
               <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> {item["messageTime"]} {item["messageDate"]}</span></div>
               <div className="clearfix" />
               <div className="chat_box_l" key={index}>{item["message"]}</div>
               </div>
           );
      }
      else{
        return (
                <div className="chat_right">
                <div className="chat_icon2"><div className="icon_clr_rs2">{basicInfo.fullName != undefined ? basicInfo.fullName.charAt(0) : ""}</div></div>
                <div className="chat_name1"><span>{item["messageTime"]}  {item["messageDate"]}</span> {receiveUserName}</div>
                <div className="clearfix" />
                <div className="chat_box_r" key={index}>{item["message"]}</div>
                </div>
           );
      }
    })       
    );
  },[receiveUserId,messageList]);


 React.useEffect(() => {
    setMessagesList(         
      messages.map((msg: React.ReactNode, index: number ) =>{
       return (
                              <div className="chat_left">
                              <div className="chat_icon1"><div className="icon_clr_rs1">{basicInfo.fullName != undefined ? basicInfo.fullName.charAt(0) : ""}</div></div>
                              <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> {new Date().toLocaleString('en', { hour: 'numeric', hour12: true, minute: 'numeric' })}</span></div>
                              <div className="clearfix" />
                              <div className="chat_box_l" key={index}> {msg}</div>
                              </div>
              );
      })       
    );
  },[messages]);


   console.log(loggedUserId,'vendorlist------------>',userList);
   console.log(loggedUserId,'sendUserId------------>',sendUserId);
   console.log(loggedUserId,'receiveUserId------------>',receiveUserId);
   console.log(loggedUserId,'messagelist------------>',messageList);

 
   const handleReceiveUser = async (receiveUserId:any,receiveUserName:any) => {
       setReceiveUserId(receiveUserId);
       setReceiveUserName(receiveUserName);
       setMessages([]);
       setShowing(true);
   }

   
    // Set the Hub Connection on mount.
    useEffect(() => {

            // Set the initial SignalR Hub Connection.
            const createHubConnection = async () => {

            const signalR = require("@aspnet/signalr");
           
            // Build new Hub Connection, url is currently hard coded.
            const hubConnect = new HubConnectionBuilder()
                .configureLogging(signalR.LogLevel.Debug)
                .withUrl("https://jitapi.clubactive.in/chathub", {
                   skipNegotiation: true,
                   transport: signalR.HttpTransportType.WebSockets
                })
               .build();
            
               try {
                await hubConnect.start()            
                console.log('Connection successful!!!');
                console.log('getLoggedUserId',loggedUserId);
                console.log('user',user);
                console.log('user id',user?.id);
                setSendUserId(user?.id);
                setSendUserName(basicInfo.fullName);
                console.log('Basic Info',basicInfo);
                console.log(basicInfo.fullName)
                console.log("authorizationToken",authorizationToken)
               
                // Bind event handlers to the hubConnection.
                hubConnect.on('ReceiveMessage', (sendUserId:string, sendUserName: string, receiveUserId: string, receiveUserName:string, message: string) => {
                    setMessages(m => [...m, `${message}`]);
                    console.log("sendUserId :",`${sendUserId}`);
                    console.log("sendUserName :",`${sendUserName}`);
                    console.log("receiveUserId :",`${receiveUserId}`);
                    console.log("receiveUserName :",`${receiveUserName}`);
                })
                
                hubConnect.on('newuserconnected', (nick: string) => {
                    setMessages(m => [...m, `${nick} has connected.`]);
                })
                // **TODO**
                // hubConnection.off('userdisconnected', (nick: string) => {
                //  
                //     setMessages(m => [...m, `${nick} has disconnected.`]);
                // })
                   }
               catch (err) {
                        alert(err);
                        console.log('Error while establishing connection: ' + { err })
                    }
            setHubConnection(hubConnect);
        }
        createHubConnection();
    }, [loggedUserId]);

    const handleMessage = async () => {
    await sendMessage()
    setMessage('');
   }

    const onEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      handleMessage();
    }
   }

  const emojiIconClick = () => {
    setShowEmoji(!showEmoji)
  }
   const addEmoji = (e:any) => {
    let emoji = e.native;
    setMessage(message + emoji)
  };

   /** Send message to server with client's nickname and message. */
    async function sendMessage(): Promise<void> {
        try {
            if (hubConnection && message !== '') {
                await hubConnection.invoke('SendMessage', sendUserId, sendUserName, receiveUserId, receiveUserName, message)

            }
        }
        catch (err) {
            console.error(err);
        }
    } 

   return (
    <React.Fragment>
    <div className="content-wrapper">
>>>>>>> 39419b1 (merged-on-10032021)
        <div className="container-fluid">
          <h1 className="heading">Message</h1>
          <div className="clearfix" />
          <div className="row ">
<<<<<<< HEAD
            <div className="col-sm-5 col-lg-4 p-r-0">
=======
             <div className="col-sm-5 col-lg-4 p-r-0">
>>>>>>> 39419b1 (merged-on-10032021)
              <div className="panel panel-default panel_n">
                <div className="panel-body panel-body1">
                  <div className="connect_left">
                    <div>
<<<<<<< HEAD
                      <input type="email" className="form-control" placeholder="Search" />
=======
                      <input value={filter} onChange={e => setFilter(e.target.value)} className="form-control" placeholder="Search" />
>>>>>>> 39419b1 (merged-on-10032021)
                      <div className="search_icon"><i className="fa fa-search" aria-hidden="true" /></div>
                    </div>
                    <div className=" m-t-25">
                      <div className="message_chat_scroll" style={{ overflow: 'hidden', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll">
<<<<<<< HEAD
                          <ul>
                            <li><a href="#" className="active">
                              <div className="connect_icon"><div className="icon_clr1">A</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-busy" /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr2">B</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-notactive" /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr3">C</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr4">D</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr5">E</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                              <div className="chat_time_sec">
                                <div className="chat_time">2 Min</div>
                                <div className="connect_con_noti">2</div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr6">F</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"> <div className="icon_clr7">G</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr8">H</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr9">J</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr10">K</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr11">L</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
                            <li><a href="#">
                              <div className="connect_icon"><div className="icon_clr12">M</div></div>
                              <div className="connect_con">
                                <div className="connect_con_name">Name <span className="dot dot-active  " /></div>
                                <div className="connect_con_des">Head of Development </div>
                              </div>
                            </a></li>
=======
                         
                          <ul>

                       {list}
        
                           
>>>>>>> 39419b1 (merged-on-10032021)
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
<<<<<<< HEAD
            </div>
            <div className="col-sm-7 col-lg-8">
              <div className="panel panel-default panel_n">
                <div className="panel-body panel-body1">
                  <div className="connect_right">
                    <div className="connect_right_top">
                      <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                      <div className="connect_con_name_r">Name</div>
                      <div className="connect_con_ac">Online</div>
                    </div>
                    <div className=" m-t-10">
                      <div className="message_chat_des_scroll" style={{ overflow: 'hidden', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll_r">
                          <div className="chat_left">
                            <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                            <div className="chat_name">Name <span> 2 Hours</span></div>
                            <div className="clearfix" />
                            <div className="chat_box_l">zxzxzxz</div>
                          </div>
                          <div className="chat_right">
                            <div className="chat_icon2"><div className="icon_clr_rs2">B</div></div>
                            <div className="chat_name1"><span>2 Hours</span> Name</div>
                            <div className="clearfix" />
                            <div className="chat_box_r">zxzxzxz</div>
                            <div className="chat_icon3"><div className="icon_clr_nr1">A</div></div>
                          </div>
                          <div className="chat_left">
                            <div className="chat_icon1"><img
                              src={require("../../../images/patients.png")}
                            // src="images/patients.png"
                            />
                            </div>
                            <div className="chat_name">Name <span> 2 Hours</span></div>
                            <div className="clearfix" />
                            <div className="chat_box_l">zxzxzxz</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="connect_right_bottom">
                      <div className="connect_right_forms"> <input type="email" className="form-control" placeholder="Search" /></div>
                      <div className="smile_icon"><img
                        src={require("../../../images/smile.png")}
                      // src="images/smile.png"
                      /></div>
=======
            </div>  
            <div className="col-sm-7 col-lg-8">
              <div className="panel panel-default panel_n"  style={{ display: ({showing} ? 'block' : 'none') }}>
                <div className="panel-body panel-body1">
                  <div className="connect_right">
                    <div className="connect_right_top">
                        {/* {profileImage != undefined && profileImage.data != undefined && profileImage.data.length > 0 ?(
                            <div className="chat_icon1"><img
                className="img-responsive"
                src={`https://jitapi.clubactive.in/Upload/CandidateProfileImage/${profileImage.data[0]["imageUrl"]}`}
                alt=""
              /></div>
                        ):( 
                           <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                       )} */}
                      {receiveUserName!=undefined ?
                                (<div> 
                                 <div className="chat_icon1"><div className="icon_clr_rs1">{receiveUserName.charAt(0)}</div></div>
                                 <div className="connect_con_name_r">{receiveUserName}</div>
                                 <div className="connect_con_ac">Online</div></div>):
                                (<div className="connect_con_name_r">Please select an admin to chat</div>)
                       }
                    </div>
                    <div className=" m-t-10">
                      <div className="message_chat_des_scroll" style={{ overflow: 'revert', outline: 'none' }} tabIndex={0}>
                        <div className="connect_scroll_r">
                           {DBMessageList}
                           {messagesList}
                           {/* { messages.map((msg: React.ReactNode, index: number ) => (   
                            //  index%2==0 ? (
                              <div className="chat_left">
                              <div className="chat_icon1"><div className="icon_clr_rs1">A</div></div>
                              <div className="chat_name">{basicInfo.fullName != undefined ? basicInfo.fullName : ""}<span> 2 Hours</span></div>
                              <div className="clearfix" />
                              <div className="chat_box_l" key={index}> {msg}</div>
                              </div>
        
                          //     ) : (
                          //   <div className="chat_right">
                          //   <div className="chat_icon2"><div className="icon_clr_rs2">B</div></div>
                          //   <div className="chat_name1"><span>2 Hours</span> Name 2</div>
                          //   <div className="clearfix" />
                          //   <div className="chat_box_r" key={index}> {msg}</div>
                          //   <div className="chat_icon3"><div className="icon_clr_nr1">A</div></div>
                          //  </div>
                          //  )                         
                           ))} */}
                        </div>
                      </div>
                    </div>
                     { showEmoji && 
                    <div className="connect_right_bottom">
                    <div className="connect_right_forms"> 
                       <span>
                        <Picker onSelect={addEmoji} />
                      </span>                     
                      </div>
                      </div>
                      }
                    <div className="connect_right_bottom">
                      <div className="connect_right_forms"> 
                      <input 
                        type="text" 
                        value={message} 
                        className="form-control" 
                        placeholder="Type message here" 
                        onChange={e => setMessage(e.target.value)}
                        onKeyDown={onEnter}
                      />                     
                      </div>
                      {/* <div className="smile_icon"><img
                        src={require("../../../images/smile.png")}
                      // src="images/smile.png"
                      /></div> */}
                        <div className="smile_icon" ><img onClick={() => emojiIconClick()}
                        src={require("../../../images/smile.png")}
                      // src="images/smile.png"
                      /> 
                      </div>
>>>>>>> 39419b1 (merged-on-10032021)
                      <div className="connect_right_icons">
                        <a href="#"><img
                          src={require("../../../images/attach_icon.png")}
                        // src="images/attach_icon.png" 
                        /></a>
                        <a href="#"><img
                          src={require("../../../images/speaker_icon.png")}
                        // src="images/speaker_icon.png" 
                        /></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="clearfix" />
        </div>
      </div>
    </React.Fragment>
  );
};
export default CandidateMessageComponent;
