<<<<<<< HEAD
import React, { useRef, useState } from 'react'

const SuperAdminDashboardComponent = () => {
  const [isListShow, setIsListShow] = useState<string>('none');

  const menuRef = useRef<any>();

  React.useEffect(() => {
    const handler = (event: any) => {
      if (menuRef.current != undefined) {
        if (
          menuRef &&
          menuRef.current &&
          !menuRef.current.contains(event.target)
        ) {
          setIsListShow('none');
        }
      }
    };

=======
import { ErrorMessage } from "@hookform/error-message";
import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { Modal } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { useNotificationListContext } from "../../../context/superadmin/SuperAdminMyProfile";
import { useMyProfileContext, useMyProfileDispatcher } from "../../../action/MyProfileAction";
import { useSuperAdminMyProfileContext, useSuperAdminMyProfileDispatcher } from "../../../action/MyProfileSuperAdminAction";
import {
  deleteSuperAdminUser,
  getSuperAdminUserById,
  getSuperAdminUserList,
  saveSuperAdminUser,
  useSuperAdminUserContext,
  useSuperAdminUserDispatcher,
} from "../../../action/superAdmin/SuperAdminUserAction";
import { AppUrls } from "../../../environments/environment";
import {
  CreateAdminRequestModel,
  InvitationRequestModel,
  SuperAdminRequestModelById,
  SuperAdminUserDeleteRequestModel,
  SuperAdminUserRequestModel,
} from "../../../models/superadmin/SuperAdminUsers";
import AuthService from "../../../services/AuthService";
import PhoneNumberWithCode from "../../components/PhoneNumberWithCode";

const defaultValues = {
  countryCode: "",
  mobileNumber: "",
  firstName: "",
};

const SuperAdminDashboardComponent = () => {
  // const [isListShow, setIsListShow] = useState<string>('none');

  const authorizationToken = AuthService.accessToken;
  let user = AuthService.currentUser;
  const [list, setList] = useState();
  const [isListShow, setIsListShow] = useState<boolean>(false);
  const [isListShowWithIndex, setIsListShowWithIndex] = useState<number>(0);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [currentAdminId, setCurrentAdmin] = useState<number>(0);
  const [generatedPassword, setGeneratedPassword] = useState<string>("");
  const [permissionArrayList, setPermissionArrayList] = useState<number[]>([]);

  const superAdminUserDispatcher = useSuperAdminUserDispatcher();
  const superAdminUserContext = useSuperAdminUserContext();
  const {
    getAllPermission,
    superAdminUsersList,
    deleteSuperAdminUserResponse,
    superAdminUser,
    saveAdminRespondModel,
  } = superAdminUserContext;

  const myProfileSuperAdminContext = useSuperAdminMyProfileContext();
  const {
    myProfile,
    basicInfo,
    loggedSuperAdminId,
  } = myProfileSuperAdminContext;

  const menuRef = useRef<any>();

  const {
    register: register2,
    errors: errors2,
    handleSubmit: handleSubmit2,
    control: control2,
    watch: watch2,
    setValue: setValue2,
  } = useForm({
    defaultValues,
  });

const notificationListContext = useNotificationListContext();
 const {
    notificationList,
    getNotificationList
  } = notificationListContext;

  React.useEffect(() => {
    if(loggedSuperAdminId >0 && authorizationToken != null  && authorizationToken != undefined )
    {
      console.log(loggedSuperAdminId, ' notificationList0 ',authorizationToken);
      getNotificationList({
        SuperAdminId:loggedSuperAdminId,
        Page: 1,
        PageSize: 10,
        SearchTerm: "",
        SortOrder: ""
      });
        console.log(loggedSuperAdminId, ' notificationList0 ',notificationList,' ', authorizationToken);
 
    }
  },[loggedSuperAdminId]);

  React.useEffect(() => {
   if(notificationList!=null && notificationList!=undefined  && authorizationToken != null   && authorizationToken != undefined && loggedSuperAdminId>0)
  {
    if(notificationList.data!=undefined)
    {
      console.log(loggedSuperAdminId, ' notificationList1 ',notificationList,' ', authorizationToken);
 
      setList(         
          notificationList.data.map((item) => {
            return (
                  <div className="notification_sec">
                    <a href="#">
                      <span className="notification_icon">
                        <img
                          src={require("../../../images/profileDefault1.jpg")}
                          className="img-responsive"
                        />
                      </span>
                      <div className="notification_con">
                        <div className="notification_head">
                         {item}
                        </div>
                        {/* <div className="notification_pra">4 days ago</div> */}
                      </div>
                    </a>
                  </div>              
              )
          })
    )
  }
}
},[loggedSuperAdminId,notificationList]);
  
  React.useEffect(() => {
    const handler = (event: any) => {
      if (1) {
        if (1) {
          // console.log("*");
          // setIsListShowWithIndex(
          //   isListShowWithIndex == 1000 ? 1000 : isListShowWithIndex
          // );
        } else {
          console.log(isListShowWithIndex);
        }
      }
    };
>>>>>>> 39419b1 (merged-on-10032021)
    document.addEventListener("mousedown", handler);
    return () => {
      document.removeEventListener("mousedown", handler);
    };
  }, []);
<<<<<<< HEAD
  return (
    <>
         {/*=================Content Section Start================*/}
         <div className="content-wrapper">
          <div className="container-fluid">  
            <div className="row m-t-25"> 
              <div className="col-sm-8"> 
                <div className="section_box2"> 
                  <h1>Users</h1>  
                  <div className="row">
                    <div className="admin_dash" style={{overflow: 'hidden', outline: 'none'}} tabIndex={0}>
                      <div className="users_admin_m">

                        {/* user Start */}
                        <div className="col-sm-3"> 
                          <div className="users_admin">
                            <div className="action_btn1">  
                              <button type="button" ref={menuRef} onClick={()=>{setIsListShow('block')}} className="actions_user"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                              <div className="user_action" tabIndex={-1} style={{display:isListShow}}>
                                <div className="action_sec">
                                  <ul>
                                    <li><a href="#">Dummy</a> </li>
                                    <li><a href="#">Dummy </a></li>
                                    <li><a href="#">Dummy </a></li>
                                    <li><a href="#">Dummy</a></li> 
                                  </ul>  
                                </div> 
                              </div>
                            </div>
                            <div className="clearfix" />
                            <div className="admin_p"><img src={require("../../../images/admin_users.jpg")}
                            // src="images/admin_users.jpg"
                             /></div>
                            <div className="admins_names">Emilin Jhon</div>
                            <div className="admins_contact"> <i className="fa fa-phone" aria-hidden="true" />
                              086 834 2525</div>
                            <div className="admins_contact"><i className="fa fa-envelope-o" aria-hidden="true" />
                              emilinjhon@gmail.com</div>
                          </div>  
                        </div> 
                        
                        
                        <div className="col-sm-3"> 
                          <div className="users_admin">
                            <div className="action_btn1">  
                              <button type="button" className="actions_user1"><i className="fa fa-ellipsis-h mdi-toggle-switch-off" aria-hidden="true" /></button>
                              <div className="user_action1" tabIndex={-1}>
                                <div className="action_sec">
                                  <ul>
                                    <li><a href="#">Dummy</a> </li>
                                    <li><a href="#">Dummy </a></li>
                                    <li><a href="#">Dummy </a></li>
                                    <li><a href="#">Dummy</a></li> 
                                  </ul>  
                                </div> 
                              </div>
                            </div>
                            <div className="clearfix" />
                            <div className="admin_p"><img src="images/admin_users1.jpg" /></div>
                            <div className="admins_names">Kian</div>
                            <div className="admins_contact"> <i className="fa fa-phone" aria-hidden="true" />
                              086 834 2525</div>
                            <div className="admins_contact"><i className="fa fa-envelope-o" aria-hidden="true" />
                              kian@jitgmail.com</div>
                          </div>  
                        </div> 
                      
                      </div>
                    </div>
                  </div>
                </div>  
              </div>
              <div className="col-sm-4">
                <div className="section_box2">
                  <h1>Messages</h1>
                  <div className="admin_dash" style={{overflow: 'hidden', outline: 'none'}} tabIndex={0}>
                    <div className="message_sec1">
                      <a href="message.html">
                        <span className="message_icon"><img src="images/4.jpg" className="img-responsive" /></span>
                        <div className="message_con_vend">
                          <div className="message_head">Alan Mathew</div> 
                          <div className="message_pra">Hi, Share your Aadhar card please!</div>  
                        </div>
                        <div className="message_noti">
                          <div className="message_noti_con">1 day ago</div>
                          <div className="message_noti_count">2</div>
                        </div>
                      </a>
                    </div>
                    <div className="message_sec1">
                      <a href="message.html">
                        <span className="message_icon"><img src="images/5.jpg" className="img-responsive" /></span>
                        <div className="message_con_vend">
                          <div className="message_head">Helen</div> 
                          <div className="message_pra">Please find below form.</div>  
                        </div>
                        <div className="message_noti">
                          <div className="message_noti_con">3 day ago</div>
                          <div className="message_noti_count">2</div>
                        </div>
                      </a>
                    </div>
                    <div className="message_sec1">
                      <a href="message.html">
                        <span className="message_icon"><img src="images/6.jpg" className="img-responsive" /></span>
                        <div className="message_con_vend">
                          <div className="message_head">Udai</div> 
                          <div className="message_pra">Share your availability for an interview.</div>  
                        </div>
                        <div className="message_noti">
                          <div className="message_noti_con">5 day ago</div>
                          <div className="message_noti_count">5</div>
                        </div>
                      </a>
                    </div>
                    <div className="message_sec1">
                      <a href="message.html">
                        <span className="message_icon"><img src="images/4.jpg" className="img-responsive" /></span>
                        <div className="message_con_vend">
                          <div className="message_head">Alan Mathew</div> 
                          <div className="message_pra">Hi, Share your Aadhar card please!</div>  
                        </div>
                        <div className="message_noti">
                          <div className="message_noti_con">1 day ago</div>
                          <div className="message_noti_count">2</div>
                        </div>
                      </a>
                    </div>
                    <div className="message_sec1">
                      <a href="message.html">
                        <span className="message_icon"><img src="images/5.jpg" className="img-responsive" /></span>
                        <div className="message_con_vend">
                          <div className="message_head">Helen</div> 
                          <div className="message_pra">Please find below form.</div>  
                        </div>
                        <div className="message_noti">
                          <div className="message_noti_con">3 day ago</div>
                          <div className="message_noti_count">2</div>
                        </div>
                      </a>
                    </div>
                    <div className="message_sec1">
                      <a href="message.html">
                        <span className="message_icon"><img src="images/6.jpg" className="img-responsive" /></span>
                        <div className="message_con_vend">
                          <div className="message_head">Udai</div> 
                          <div className="message_pra">Share your availability for an interview.</div>  
                        </div>
                        <div className="message_noti">
                          <div className="message_noti_con">5 day ago</div>
                          <div className="message_noti_count">5</div>
                        </div>
                      </a>
                    </div>
                    <div className="message_sec1">
                      <a href="message.html">
                        <span className="message_icon"><img src="images/6.jpg" className="img-responsive" /></span>
                        <div className="message_con_vend">
                          <div className="message_head">Udai</div> 
                          <div className="message_pra">Share your availability for an interview.</div>  
                        </div>
                        <div className="message_noti">
                          <div className="message_noti_con">5 day ago</div>
                          <div className="message_noti_count">5</div>
                        </div>
                      </a>
                    </div>
                  </div>         
                </div>
              </div>
              <div className="col-sm-6">
                <div className="section_box2">
                  <h1>Database</h1> 
                  <div className="data_base_btn"><a href="#" className="btn submit_btn">Backup Now</a></div>
                  <div className="clearfix" />
                  <div className="col-sm-3"><img src="images/database_icon.png" /></div>
                  <div className="col-sm-9">
                    <div className="admins_data_h">My Database Details</div>
                    <div className="admins_data_c">Database Name :</div>
                    <div className="admins_data_c">Server :</div>
                    <div className="admins_data_c">Host :</div>
                    <div className="admins_data_c">User Name :</div>
                    <div className="admins_data_c">Connection State : Active</div> 
                  </div>
                </div>        
              </div>
              {/* BEGIN MODAL */}
              <div className="attach_docs">
                <div className="modal fade none-border" id="database_setup">
                  <div className="modal-dialog">
                    <div className="modal-content">
                      <div className="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" className="close" type="button">×</button>
                        <h4 className="modal-title">Setup Automatic Backup </h4>
                      </div>
                      <div className="modal-body">
                        <div className="col-sm-6"> 
                          <div className="form-group">
                            <label htmlFor="email">Frequency</label>
                            <span className="select-wrapper-sec"><select name="timepass" className="custom-select-sec">
                                <option value="">Select</option>
                                <option value="">Dummy</option>  
                                <option value="">Dummy</option> 
                              </select><span className="holder">Select</span></span>     
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label htmlFor="email">Time</label>
                            <input type="email" className="form-control" placeholder="time" />
                            <div className="time_icon"><img src="images/time_icon.png" /></div>    
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">        
                            <input id="checkbox1" type="checkbox" name="checkbox" defaultValue={1} /><label htmlFor="checkbox1"><span /> Notify me Via Email</label>
                          </div> 
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">             
                            <input id="checkbox1" type="checkbox" name="checkbox" defaultValue={1} /><label htmlFor="checkbox1"><span /> Notify me via Message </label>
                          </div>
                        </div>
                      </div>
                      <div className="modal-footer  m-t-30"> 
                        <button className="btn btn-success save-event waves-effect waves-light" type="button">Save</button>
                        <button data-dismiss="modal" className="btn btn-default waves-effect" type="button">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* END MODAL */}
              <div className="col-sm-6">
                <div className="section_box2">
                  <h1>Notifications</h1>
                  <div className="dash_noti_scroll" style={{overflow: 'hidden', outline: 'none'}} tabIndex={0}> 
                    <div className="notification_sec">
                      <a href="#">
                        <span className="notification_icon"><img src="images/1.jpg" className="img-responsive" /></span>
                        <div className="notification_con">
                          <div className="notification_head"><strong>Stephan parker</strong> has messaged you</div> 
                          <div className="notification_pra">4 days ago</div> 
                        </div> 
                      </a>
                    </div>
                    <div className="notification_sec">
                      <a href="#">
                        <span className="notification_icon"><img src="images/2.jpg" className="img-responsive" /></span>
                        <div className="notification_con">
                          <div className="notification_head"><strong>24 Matched jobs</strong> for you. Review them.</div> 
                          <div className="notification_pra">4 days ago</div> 
                        </div>
                      </a>
                    </div>
                    <div className="notification_sec">
                      <a href="#">
                        <span className="notification_icon"><img src="images/3.jpg" className="img-responsive" /></span>
                        <div className="notification_con">
                          <div className="notification_head"><strong>2 Documents are getting expired</strong> this month. Re-new them now.</div> 
                          <div className="notification_pra">4 days ago</div>  
                        </div>
                      </a>
                    </div>
                    <div className="notification_sec">
                      <a href="#">
                        <span className="notification_icon"><img src="images/3.jpg" className="img-responsive" /></span>
                        <div className="notification_con">
                          <div className="notification_head"><strong>2 Documents are getting expired</strong> this month. Re-new them now.</div> 
                          <div className="notification_pra">4 days ago</div>  
                        </div>
                      </a>
=======

  useEffect(() => {
    console.log(290, superAdminUser);

    if (superAdminUser.data != undefined) {
      let listArray = [];
      if (superAdminUser.data.permissionTypes.length > 0) {
        listArray = superAdminUser.data.permissionTypes.map(
          (dataValue: any) => {
            return dataValue.toString();
          }
        );
      }
      console.log(291, superAdminUser.data);
      setValue2("firstName", superAdminUser.data.adminName);
      setValue2("email", superAdminUser.data.email);
      setValue2("location", superAdminUser.data.location);
      setValue2("mobileNumber", superAdminUser.data.mobileNumber);
      // setValue2("password", superAdminUser.data.generatePassword);
      setGeneratedPassword(superAdminUser.data.generatePassword);
      setValue2("checkbox", listArray);
      // setPermissionArrayList(superAdminUser.data.permissionTypes);
    }
  }, [superAdminUser.data]);

  // useEffect(() => {
  //   if (saveAdminRespondModel.IsSuccess) {
  //     setIsOpen(false);
  //   }
  // }, [saveAdminRespondModel]);

  const handleInvitationClick = (
    mobileNumber: string,
    email: string,
    countryCode: string,
    password: string
  ) => {
    if (authorizationToken != null) {
      let token = "";
      try {
        if (authorizationToken?.startsWith(`"`)) {
          let token = authorizationToken.substring(1);
          token = authorizationToken.slice(0, -1);
        }

        let header = {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
        };

        let data = {
          Email: email,
          PhoneNumber: countryCode + mobileNumber,
          Password: password,
        } as InvitationRequestModel;

        const url1 = AppUrls.SendInvitationPhone;
        const url2 = AppUrls.SendInvitationEmail;
        axios
          .post(url1, JSON.stringify(data), { headers: header })
          .then((res) => {
            console.log(res);
          });

        axios
          .post(url2, JSON.stringify(data), { headers: header })
          .then((res) => {
            console.log(res);
          });
      } catch (e) {}
      setIsListShowWithIndex(1000);
    }
  };

  const handleAdminClick = (data: any) => {
    let permissionArray = [];
    permissionArray = data.checkbox;
    let permissionArrayInt = permissionArray.map((dataValue: any) => {
      return parseInt(dataValue);
    });
    console.log(295, data);
    if (authorizationToken != null) {
      (async () => {
        await saveSuperAdminUser(
          superAdminUserDispatcher,
          {
            AdminName: data.firstName,
            AdminTitleIdList: permissionArrayInt,
            CountryCode: data.countryCode === "" ? "+91" : data.countryCode,
            Email: data.email,
            GeneratePassword: data.password,
            IsActive: true,
            Location: data.location,
            MobileNumber: data.mobileNumber,
            RowId: currentAdminId,
            SuperAdminId: 1,
          } as CreateAdminRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
      setIsOpen(true);
      setIsListShowWithIndex(1000);
      // setIsListShow("none");
    }
  };

  const handleEditAdminClick = (data: any) => {
    if (authorizationToken != null) {
      (async () => {
        await getSuperAdminUserById(
          superAdminUserDispatcher,
          {
            RowId: data,
          } as SuperAdminRequestModelById,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
      setCurrentAdmin(data);
      setIsListShow(false);
      setIsOpen(true);
      setIsListShowWithIndex(1000);
      // setIsListShow("none");
    }
  };

  // setValue2("firstName", "fdfh");

  const handleGeneratePasword = () => {
    let A1 = "Admin";
    let A2 = "@";
    const min = 1;
    const max = 100;
    const rand1 = Math.floor(Math.random() * 31) + 10;
    const rand2 = Math.floor(Math.random() * 31) + 50;
    // const pwd = (A1 +Math.round(rand1) + A2 + Math.round(rand2)).toString();
    setGeneratedPassword(A1 + Math.round(rand1) + A2 + Math.round(rand2));
    console.log(56888, rand1);
  };

  React.useEffect(() => {
    if (authorizationToken != null) {
      (async () => {
        await getSuperAdminUserList(
          superAdminUserDispatcher,
          {
            page: 1,
            pageSize: 10,
            searchTerm: "",
            showInactive: false,
            sortOrder: "",
            superAdminId: 1,
          } as SuperAdminUserRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
      setIsOpen(false);
    }
  }, [loggedSuperAdminId, deleteSuperAdminUserResponse, saveAdminRespondModel]);

  const handleAdminDelete = (id: any) => {
    console.log(3545776556374, id);
    if (authorizationToken != null) {
      (async () => {
        await deleteSuperAdminUser(
          superAdminUserDispatcher,
          {
            RowId: id,
            SuperAdminId: 1,
          } as SuperAdminUserDeleteRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
      // setIsListShow("none");
    }
  };

  return (
    <>
      {/*=================Content Section Start================*/}
      <div className="content-wrapper">
        <div className="container-fluid">
          <div className="row m-t-25">
            <div className="col-sm-8">
              <Modal
                show={isOpen}
                onHide={() => {
                  setIsOpen(!isOpen);
                }}
              >
                <form onSubmit={handleSubmit2(handleAdminClick)} noValidate>
                  <Modal.Header closeButton>
                    <Modal.Title>
                      {currentAdminId == 0
                        ? " New Admin Profile"
                        : "Update Admin Profile"}
                    </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="col-sm-4">
                      <div className="form-group">
                        <label htmlFor="email">Name</label>
                        <input
                          type="text"
                          className="form-control "
                          placeholder="Name"
                          name="firstName"
                          ref={register2({
                            required: "Name is required",
                          })}
                        />
                        <ErrorMessage
                          errors={errors2}
                          name="firstName"
                          render={({ message }) => (
                            <div className="login_validation">{message}</div>
                          )}
                        />
                      </div>
                    </div>

                    <div className="col-sm-4">
                      <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input
                          type="text"
                          className="form-control "
                          placeholder=""
                          name="email"
                          ref={register2({
                            required: "Email is required",
                          })}
                        />
                        <ErrorMessage
                          errors={errors2}
                          name="email"
                          render={({ message }) => (
                            <div className="register_validation">{message}</div>
                          )}
                        />
                      </div>
                    </div>
                    <div className="col-sm-4">
                      <div className="form-group">
                        <label htmlFor="email">Mobile Nymber</label>
                        <input
                          type="text"
                          name="countryCode"
                          style={{ display: "none" }}
                          disabled={false}
                          ref={register2}
                        />
                        <Controller
                          control={control2}
                          name="countryCode"
                          render={({ onChange, onBlur, value, name }) => (
                            <PhoneNumberWithCode
                              codeHeight={"41px"}
                              noPenIcon={true}
                              onChange={onChange}
                              onBlur={onBlur}
                              value={value}
                              name={name}
                              phoneNo={
                                <input
                                  type="text"
                                  name="mobileNumber"
                                  id="phoneNumber"
                                  className="form-control phoneNumber"
                                  placeholder="Mobile number"
                                  ref={register2({
                                    required: "Mobile number is required",
                                    maxLength: {
                                      value: 15,
                                      message: "Should be a mobile number",
                                    },

                                    pattern: {
                                      value: /^[0-9]*$/,
                                      message:
                                        "Mobile number should be numbers only",
                                    },
                                  })}
                                />
                              }
                            />
                          )}
                        />

                        <ErrorMessage
                          errors={errors2}
                          name="mobileNumber"
                          render={({ message }) => (
                            <div className="register_validation">{message}</div>
                          )}
                        />
                      </div>

                      {/* <div className="form-group"> */}
                      {/* <label htmlFor="email">Mobile </label>
                    <UserMobileInput />
                    <div className="input-phone"> </div>
                  </div> */}
                    </div>
                    <div className="col-sm-4">
                      <div className="form-group ">
                        <label htmlFor="email" className="col-sm-12">
                          Password
                        </label>
                        <button
                          onClick={handleGeneratePasword}
                          className="btn btn-success save-event waves-effect waves-light"
                          type="button"
                        >
                          Generate Password
                        </button>
                      </div>
                    </div>

                    <div className="col-sm-4">
                      <div className="form-group ">
                        <label htmlFor="email" className="col-sm-12"></label>

                        <input
                          type="text"
                          // className="form-control "
                          style={{
                            paddingTop: 25,
                            color: "green",
                            border: "none",
                            fontSize: "large",
                          }}
                          // placeholder="location"
                          name="password"
                          value={generatedPassword}
                          ref={register2({
                            required: "password is required",
                          })}
                        />
                        {generatedPassword !== "" ? null : (
                          <ErrorMessage
                            errors={errors2}
                            name="password"
                            render={({ message }) => (
                              <div
                                style={{ fontSize: "large", marginTop: -28 }}
                                className="login_validation"
                              >
                                {message}
                              </div>
                            )}
                          />
                        )}
                        <div className="col-sm-4"></div>
                      </div>
                    </div>

                    <div className="col-sm-12">
                      <div className="form-group">
                        <label htmlFor="email">Location</label>
                        <input
                          type="text"
                          className="form-control "
                          placeholder="location"
                          name="location"
                          ref={register2({
                            required: false,
                          })}
                        />
                        <ErrorMessage
                          errors={errors2}
                          name="location"
                          render={({ message }) => (
                            <div className="login_validation">{message}</div>
                          )}
                        />
                      </div>
                    </div>

                    <div className="col-sm-12">
                      <h1 className="modal_sub_head">Permissions:</h1>
                    </div>

                    <div className="col-sm-12">
                      <div className="modal_sub_head1">Approval</div>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={1}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Vendors registration
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={2}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Clients job posts
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={3}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Career Tips
                      </label>
                    </div>
                    <div className="col-sm-12">
                      <div className="modal_sub_head1">Site Settings </div>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={4}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> My team
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={5}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Stories of JIT
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={6}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Company contacts and social media details
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={7}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Add job category
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={8}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Add job title
                      </label>
                    </div>
                    <div className="clearfix" />
                    <div className="col-sm-12">
                      <div className="modal_sub_head1">Reports </div>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={9}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Client wise
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={10}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Vendor wise{" "}
                      </label>
                    </div>
                    <div className="clearfix" />
                    <div className="col-sm-12">
                      <div className="modal_sub_head1">Job applications</div>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={11}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Create and Edit Job posts
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={12}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> View and Process job applications
                      </label>
                    </div>
                    <div className="clearfix" />
                    <div className="col-sm-12">
                      <div className="modal_sub_head1">Client Profiling </div>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={13}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Create and Edit client's Profile
                      </label>
                    </div>
                    <div className="clearfix" />
                    <div className="col-sm-12">
                      <div className="modal_sub_head1">Career Developer</div>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={14}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Add new categories
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={15}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Add new video under category
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={16}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Delete category
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={17}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Delete video under category
                      </label>
                    </div>
                    <div className="col-sm-4">
                      <input
                        id="checkbox1"
                        type="checkbox"
                        name="checkbox"
                        defaultValue={18}
                        ref={register2({
                          required: false,
                        })}
                      />
                      <label htmlFor="checkbox1">
                        <span /> Update video under category
                      </label>
                    </div>
                    <div className="clearfix" />
                  </Modal.Body>
                  <div className="modal-footer  m-t-30">
                    <button
                      className="btn btn-success save-event waves-effect waves-light"
                      type="submit"
                    >
                      Save
                    </button>
                    <button
                      data-dismiss="modal"
                      className="btn btn-default waves-effect"
                      type="button"
                    >
                      Cancel
                    </button>
                  </div>
                  <div className="clearfix" />
                </form>
              </Modal>

              <div className="section_box2">
                <h1>Users</h1>
                <div className="row">
                  <div
                    className="admin_dash"
                    style={{ overflow: "hidden", outline: "none" }}
                    tabIndex={0}
                  >
                    <div className="users_admin_m">
                      {/* user Start */}

                      {superAdminUsersList.data != undefined
                        ? superAdminUsersList.data.map((data, i) => {
                            return (
                              <div className="col-sm-3">
                                <div className="users_admin1">
                                  <div className="action_btn1">
                                    <button
                                      type="button"
                                      onClick={() => {
                                        setIsListShow(false);
                                        // setIsListShow(
                                        //   data.adminId == isListShowWithIndex
                                        //     ? true
                                        //     : false
                                        // );
                                        setIsListShowWithIndex(data.adminId);
                                      }}
                                      className="actions_user"
                                    >
                                      <i
                                        className="fa fa-ellipsis-h mdi-toggle-switch-off"
                                        aria-hidden="true"
                                      />
                                    </button>
                                    <div
                                      className="user_action"
                                      style={{
                                        display:
                                          data.adminId == isListShowWithIndex
                                            ? "block"
                                            : "none",
                                      }}
                                      tabIndex={-1}
                                    >
                                      <div className="action_sec">
                                        <ul ref={menuRef}>
                                          <li
                                            onClick={() => {
                                              handleInvitationClick(
                                                data.mobileNumber,
                                                data.email,
                                                data.countryCode,
                                                data.generatePassword
                                              );
                                              setIsListShowWithIndex(
                                                data.adminId
                                              );
                                            }}
                                          >
                                            <a>Invite </a>
                                          </li>
                                          <li
                                            onClick={(id: any) =>
                                              handleEditAdminClick(data.adminId)
                                            }
                                          >
                                            <a>Edit </a>
                                          </li>
                                          <li
                                            onClick={(id: any) =>
                                              handleAdminDelete(data.adminId)
                                            }
                                          >
                                            <a style={{ cursor: "pointer" }}>
                                              Delete
                                            </a>
                                          </li>
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="clearfix" />
                                  <div className="admin_user">
                                    <img
                                      src={require("./../../../images/admin_users.jpg")}
                                    />
                                  </div>
                                  <div className="admins_names">
                                    {data.adminName}
                                  </div>
                                  <div className="admins_contact1">
                                    {" "}
                                    <i
                                      className="fa fa-phone"
                                      aria-hidden="true"
                                    />{" "}
                                    {data.mobileNumber}
                                  </div>
                                  <div className="admins_contact1">
                                    <i
                                      className="fa fa-envelope-o"
                                      aria-hidden="true"
                                    />{" "}
                                    {data.email}
                                  </div>
                                </div>
                              </div>
                            );
                          })
                        : null}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="section_box2">
                <h1>Messages</h1>
                <div
                  className="admin_dash"
                  style={{ overflow: "hidden", outline: "none" }}
                  tabIndex={0}
                >
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img src="images/4.jpg" className="img-responsive" />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Alan Mathew</div>
                        <div className="message_pra">
                          Hi, Share your Aadhar card please!
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">1 day ago</div>
                        <div className="message_noti_count">2</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img src="images/5.jpg" className="img-responsive" />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Helen</div>
                        <div className="message_pra">
                          Please find below form.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">3 day ago</div>
                        <div className="message_noti_count">2</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img src="images/6.jpg" className="img-responsive" />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Udai</div>
                        <div className="message_pra">
                          Share your availability for an interview.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">5 day ago</div>
                        <div className="message_noti_count">5</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img src="images/4.jpg" className="img-responsive" />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Alan Mathew</div>
                        <div className="message_pra">
                          Hi, Share your Aadhar card please!
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">1 day ago</div>
                        <div className="message_noti_count">2</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img src="images/5.jpg" className="img-responsive" />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Helen</div>
                        <div className="message_pra">
                          Please find below form.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">3 day ago</div>
                        <div className="message_noti_count">2</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img src="images/6.jpg" className="img-responsive" />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Udai</div>
                        <div className="message_pra">
                          Share your availability for an interview.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">5 day ago</div>
                        <div className="message_noti_count">5</div>
                      </div>
                    </a>
                  </div>
                  <div className="message_sec1">
                    <a href="message.html">
                      <span className="message_icon">
                        <img src="images/6.jpg" className="img-responsive" />
                      </span>
                      <div className="message_con_vend">
                        <div className="message_head">Udai</div>
                        <div className="message_pra">
                          Share your availability for an interview.
                        </div>
                      </div>
                      <div className="message_noti">
                        <div className="message_noti_con">5 day ago</div>
                        <div className="message_noti_count">5</div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="section_box2">
                <h1>Database</h1>
                <div className="data_base_btn">
                  <a href="#" className="btn submit_btn">
                    Backup Now
                  </a>
                </div>
                <div className="clearfix" />
                <div className="col-sm-3">
                  <img src="images/database_icon.png" />
                </div>
                <div className="col-sm-9">
                  <div className="admins_data_h">My Database Details</div>
                  <div className="admins_data_c">Database Name :</div>
                  <div className="admins_data_c">Server :</div>
                  <div className="admins_data_c">Host :</div>
                  <div className="admins_data_c">User Name :</div>
                  <div className="admins_data_c">Connection State : Active</div>
                </div>
              </div>
            </div>
            {/* BEGIN MODAL */}
            <div className="attach_docs">
              <div className="modal fade none-border" id="database_setup">
                <div className="modal-dialog">
                  <div className="modal-content">
                    <div className="modal-header">
                      <button
                        aria-hidden="true"
                        data-dismiss="modal"
                        className="close"
                        type="button"
                      >
                        ×
                      </button>
                      <h4 className="modal-title">Setup Automatic Backup </h4>
                    </div>
                    <div className="modal-body">
                      <div className="col-sm-6">
                        <div className="form-group">
                          <label htmlFor="email">Frequency</label>
                          <span className="select-wrapper-sec">
                            <select
                              name="timepass"
                              className="custom-select-sec"
                            >
                              <option value="">Select</option>
                              <option value="">Dummy</option>
                              <option value="">Dummy</option>
                            </select>
                            <span className="holder">Select</span>
                          </span>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <label htmlFor="email">Time</label>
                          <input
                            type="email"
                            className="form-control"
                            placeholder="time"
                          />
                          <div className="time_icon">
                            <img src="images/time_icon.png" />
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <input
                            id="checkbox1"
                            type="checkbox"
                            name="checkbox"
                            defaultValue={1}
                          />
                          <label htmlFor="checkbox1">
                            <span /> Notify me Via Email
                          </label>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <input
                            id="checkbox1"
                            type="checkbox"
                            name="checkbox"
                            defaultValue={1}
                          />
                          <label htmlFor="checkbox1">
                            <span /> Notify me via Message{" "}
                          </label>
                        </div>
                      </div>
                    </div>
                    <div className="modal-footer  m-t-30">
                      <button
                        className="btn btn-success save-event waves-effect waves-light"
                        type="button"
                      >
                        Save
                      </button>
                      <button
                        data-dismiss="modal"
                        className="btn btn-default waves-effect"
                        type="button"
                      >
                        Cancel
                      </button>
>>>>>>> 39419b1 (merged-on-10032021)
                    </div>
                  </div>
                </div>
              </div>
            </div>
<<<<<<< HEAD
            <div className="clearfix" />                      
          </div>  
        </div> 
        {/*=================Content Section End================*/}
     
    </>
  )
}

export default SuperAdminDashboardComponent
=======
            {/* END MODAL */}
            <div className="col-sm-6">
              <div className="section_box2">
                <h1>Notifications</h1>
                <div
                  className="dash_noti_scroll"
                  style={{ overflow: "hidden", outline: "none" }}
                  tabIndex={0}
                >
                  {list}
              </div>
              </div>
            </div>
          </div>
          <div className="clearfix" />
        </div>
      </div>
      {/*=================Content Section End================*/}
    </>
  );
};

export default SuperAdminDashboardComponent;
>>>>>>> 39419b1 (merged-on-10032021)
