import React from "react";
import {
  getSuperAdminReportList,
  useSuperAdminReportContext,
  useSuperAdminReportDispatcher,
} from "../../../action/superAdmin/SuperAdminReportAction";
import { SuperAdminReportRequestModel } from "../../../models/superadmin/SuperAdminReport";
import AuthService from "../../../services/AuthService";

const SuperAdminReportComponent = () => {
  const authorizationToken = AuthService.accessToken;
  let user = AuthService.currentUser;

  const superAdminReportDispatcher = useSuperAdminReportDispatcher();
  const superAdminReportContext = useSuperAdminReportContext();
  const { adminReportsList } = superAdminReportContext;

  console.log(545454, adminReportsList);

  React.useEffect(() => {
    if (authorizationToken != null) {
      (async () => {
        await getSuperAdminReportList(
          superAdminReportDispatcher,
          {
            FilterId: 0,
            Page: 1,
            SuperAdminId: 1,
            PageSize: 10,
            SearchTerm: "",
            ShowInactive: false,
            SortOrder: "",
          } as SuperAdminReportRequestModel,
          authorizationToken
          // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNyIsIm5hbWUiOiJhYjEzOWZlOC01MTJkLTQyZGMtOTQ3MS0zODMzZTFlYjRlZjYiLCJ0b2tlbl91c2FnZSI6ImFjY2Vzc190b2tlbiIsImp0aSI6ImVmNTU5ZjYxLTBlZTktNDBhMS1iYTViLTQ5ZTE2MDk5Mzg0MiIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicGhvbmUiLCJwcm9maWxlIiwib2ZmbGluZV9hY2Nlc3MiLCJyb2xlcyJdLCJuYmYiOjE2MTI1OTg2NjQsImV4cCI6MTY0NDEzNDY2NCwiaWF0IjoxNjEyNTk4NjY0LCJpc3MiOiJodHRwczovL2xvY2FsaG9zdDo0NDM2MS8ifQ.WyMMc_5KbhYoYnmAjyx65VQAn5-mAu4dEsHxVHPWDA8'
        );
      })();
    }
  }, []);

  return (
    <>
      <div className="content-wrapper">
        <div className="container-fluid">
          <h1 className="heading">Reports</h1>
          <a href="#" className="cutomise_but">
            Export To Exel
          </a>
          <a href="#" className="cutomise_but">
            Export To PDF
          </a>
          <div className="clearfix" />
          <div className="row ">
            <div className="col-sm-12 col-lg-12">
              <div className="report_top">
                <div className="col-sm-9">
                  <div className="report_sec">
                    <div className="report_co">Approved</div>
                    <div className="report_co1">
                      {adminReportsList.noofAprovedJobs}
                    </div>
                  </div>
                  <div className="report_sec">
                    <div className="report_co">Published</div>
                    <div className="report_co1">20</div>
                  </div>
                  <div className="report_sec">
                    <div className="report_co">Viewed</div>
                    <div className="report_co1">25</div>
                  </div>
                  <div className="report_sec">
                    <div className="report_co">Processed</div>
                    <div className="report_co1">30</div>
                  </div>
                  <div className="report_sec">
                    <div className="report_co">Created</div>
                    <div className="report_co1">35</div>
                  </div>
                  <div className="report_sec">
                    <div className="report_co">Deleted</div>
                    <div className="report_co1">5</div>
                  </div>
                  <div className="report_sec">
                    <div className="report_co">Import</div>
                    <div className="report_co1">54</div>
                  </div>
                  <div className="report_sec">
                    <div className="report_co">Export</div>
                    <div className="report_co1">50</div>
                  </div>
                </div>
                <div className="col-sm-3">
                  <span className="select-wrapper-sec">
                    <select name="timepass" className="custom-select-sec">
                      <option value="">Filter</option>
                      <option value="">Dummy</option>
                      <option value="">Dummy</option>
                    </select>
                    <span className="holder">Filter</span>
                  </span>
                </div>
              </div>
              <div className="section_box4">
                <div className="reports">
                  <div className="table-responsive">
                    <table
                      className="table table-hover table-bordered dataTable no-footer dtr-inline"
                      width="100%"
                    >
                      <thead>
                        <tr>
                          <th colSpan={2} className="bg">
                            Name{" "}
                          </th>
                          <th colSpan={4} className="bg">
                            Jobs
                          </th>
                          <th colSpan={2} className="bg">
                            Users
                          </th>
                          <th colSpan={2} className="bg">
                            Database
                          </th>
                        </tr>
                        <tr>
                          <th>&nbsp;</th>
                          <th>&nbsp;</th>
                          <th>Approved </th>
                          <th>Published </th>
                          <th>Viewed </th>
                          <th>Processed</th>
                          <th>Created </th>
                          <th>Deleted</th>
                          <th>Import </th>
                          <th>Export</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <img
                              src="images/admin_users.jpg"
                              className="report_img"
                            />
                          </td>
                          <td className="text-left">Emilin Jhon</td>
                          <td>8</td>
                          <td>12</td>
                          <td>14</td>
                          <td>16</td>
                          <td>18</td>
                          <td>5</td>
                          <td>50</td>
                          <td>45</td>
                        </tr>
                        <tr>
                          <td>
                            <img
                              src="images/admin_users.jpg"
                              className="report_img"
                            />
                          </td>
                          <td className="text-left">Emilin Jhon</td>
                          <td>8</td>
                          <td>12</td>
                          <td>14</td>
                          <td>16</td>
                          <td>18</td>
                          <td>5</td>
                          <td>50</td>
                          <td>45</td>
                        </tr>
                        <tr>
                          <td>
                            <img
                              src="images/admin_users.jpg"
                              className="report_img"
                            />
                          </td>
                          <td className="text-left">Emilin Jhon</td>
                          <td>8</td>
                          <td>12</td>
                          <td>14</td>
                          <td>16</td>
                          <td>18</td>
                          <td>5</td>
                          <td>50</td>
                          <td>45</td>
                        </tr>
                        <tr>
                          <td>
                            <img
                              src="images/admin_users.jpg"
                              className="report_img"
                            />
                          </td>
                          <td className="text-left">Emilin Jhon</td>
                          <td>8</td>
                          <td>12</td>
                          <td>14</td>
                          <td>16</td>
                          <td>18</td>
                          <td>5</td>
                          <td>50</td>
                          <td>45</td>
                        </tr>
                        <tr>
                          <td>
                            <img
                              src="images/admin_users.jpg"
                              className="report_img"
                            />
                          </td>
                          <td className="text-left">Emilin Jhon</td>
                          <td>8</td>
                          <td>12</td>
                          <td>14</td>
                          <td>16</td>
                          <td>18</td>
                          <td>5</td>
                          <td>50</td>
                          <td>45</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <ul className="pagination m-b-5">
                <li>
                  <a href="#" aria-label="Previous">
                    {" "}
                    <i className="fa fa-angle-left" />{" "}
                  </a>
                </li>
                <li className="active">
                  <a href="#">1</a>
                </li>
                <li>
                  <a href="#">2</a>
                </li>
                <li>
                  <a href="#">3</a>
                </li>
                <li>
                  <a href="#">4</a>
                </li>
                <li>
                  <a href="#">5</a>
                </li>
                <li>
                  {" "}
                  <a href="#" aria-label="Next">
                    {" "}
                    <i className="fa fa-angle-right" />{" "}
                  </a>{" "}
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="clearfix" />
      </div>
    </>
  );
};
export default SuperAdminReportComponent;
