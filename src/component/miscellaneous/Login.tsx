<<<<<<< HEAD
import React, { useState, useContext } from "react";
import { useForm } from "react-hook-form";
=======
import React, { useState, useEffect, useContext } from "react";
import { Controller, useForm } from "react-hook-form";
>>>>>>> 39419b1 (merged-on-10032021)
import { Link, withRouter } from "react-router-dom";
import { ErrorMessage } from "@hookform/error-message";
import AuthService from "../../services/AuthService";
import {
  GoogleLoginButton,
  FacebookLoginButton,
  TwitterLoginButton,
  LinkedinLoginButton,
} from "./components/SocialLoginButton";
<<<<<<< HEAD
import { GlobalSpinnerContext } from "../../context/GlobalSpinner";
import { toast, ToastContainer } from "react-toastify";
import { socialLoginHandler } from "../../apis/misc";
=======
import { toast, ToastContainer } from "react-toastify";
import { socialLoginHandler } from "../../apis/misc";
import { Modal } from "react-bootstrap";
import OtpVerificationEmail from "./OtpVerificationEmail";
import OtpVerificationMobile from "./OtpVerificationMobile";
import { useHistory} from "react-router-dom";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { reactHookFormServerErrors } from "./../../utils/utility";
import OtpExpiryCouter from "./../GlobalOtpExpiryCouter";
import {
  sendEmailOpt,
  sendPhoneOtp,
  verifyCode,
  emailVerifyCode,
  verifyCodeNewMobile,
  createSuperAdmin,
} from "./../../apis/misc";
import {
  GlobalSpinnerContext,
  useGlobalSpinnerContext,
} from "./../../context/GlobalSpinner";
import Timer from "react-compound-timer";
import PhoneNumberWithCode from "../components/PhoneNumberWithCode";

function Verification(props: any) {
  
  const [email, setEmail] = useState("");

  const [contactNumber, setContactNumber] = useState("");

  const [countryCode, setCountryCode] = useState("");

  const [showTimer, setShowTimer] = useState(false);

  const [timerMsg, setTimerMsg] = useState("");

  const [screen, setScreen] = useState(1);

  const globalSpinner = useGlobalSpinnerContext();

  const [disabled, setDisabled] = useState(false);

  const [isEmail, setIsEmail] = useState<Boolean>(true);

  let val = {};

  if (isEmail) {
    val = {
      required: "Email is required"
    }
  } else {
    val = {
      required: "Mobile Number is required"
    }
  }
const { register, handleSubmit, watch, errors, trigger, control } = useForm<{
    contactNumber: string;
    countryCode: string;
  }>({
    defaultValues: { contactNumber: "", countryCode: "" },
  });

  const {
    register: register2,
    handleSubmit: handleSubmit2,
    watch: watch2,
    errors: errors2,
    trigger: trigger2,
    setError: setError2,
    clearErrors: clearErrors2
  } = useForm<{ code: string }>({
    defaultValues: { code: "" }
  });

  //for sending OTP
  const onSubmit = (data: any) => {
    console.log('------------------------send code-------------------------------',data);

    globalSpinner.showSpinner();

    if (isEmail) {
    
    setEmail(data["emailOrMobile"]);
  
    sendEmailOpt({}, `?email=${data["emailOrMobile"]}`)
      .then((res: any) => {
        console.log(res);
        globalSpinner.hideSpinner();
        if (res.data.includes("error")) {
          toast.error(res.data);
        } else {
           toast.success("OTP has been successfully sent on your e-mail.");
          setScreen(2);
          setShowTimer(true);
        }
      })
      .catch((err: any) => {
        console.log(err);
        globalSpinner.hideSpinner();
        toast.error("Something went wrong");
      });
    }
    else{
     
   setCountryCode(data["countryCode"] == "" ? "+91" : data["countryCode"]);
   setContactNumber(data["contactNumber"]);

      sendPhoneOtp(
      {},
      `?countryCode=${encodeURIComponent(
        data["countryCode"] == "" ? "+91" : data["countryCode"]
      )}&phoneNumber=${encodeURIComponent(data["contactNumber"])}`
    )
      .then((res: any) => {
        globalSpinner.hideSpinner();
        if (res.data.includes("error") || res.data.includes("Invalid")) {
          toast.error(res.data);
        } else {
          // toast.success("OTP has been send.");
          toast.success(
            "OTP has been successfully sent on your mobile number."
          );
          setScreen(2);
          setShowTimer(true);
        }
      })
      .catch((err: any) => {
        globalSpinner.hideSpinner();
        toast.error("Something went wrong");
      });
    }
    
  };

  const sendVerification = async () => {
    console.log(email);
    globalSpinner.showSpinner();
   if(isEmail){
    sendEmailOpt({}, `?email=${email}`)
      .then((res: any) => {
        console.log(res);
        globalSpinner.hideSpinner();
        if (res.data.includes("error")) {
          toast.error(res.data);
        } else {
          //toast.success("OTP has been resend.");
          toast.success("OTP has been successfully sent on your e-mail.");
          setShowTimer(true);
          setTimerMsg("");
          setDisabled(false);
        }
      })
      .catch((err: any) => {
        console.log(err);
        globalSpinner.hideSpinner();
        toast.error("Something went wrong");
      });
    }
    else{
    
    sendPhoneOtp(
      {},
      `?countryCode=${encodeURIComponent(countryCode)}&phoneNumber=${encodeURIComponent(contactNumber)}`
    )
      .then((res: any) => {
        globalSpinner.hideSpinner();
        if (res.data.includes("error") || res.data.includes("Invalid")) {
          toast.error(res.data);
        } else {
          // toast.success("OTP has been send.");
          toast.success(
            "OTP has been successfully sent on your mobile number."
          );
          setScreen(2);
          setShowTimer(true);
          setTimerMsg("");
          setDisabled(false);
        }
      })
      .catch((err: any) => {
        globalSpinner.hideSpinner();
        toast.error("Something went wrong");
      });
    }
  };

  //For verifying code
  const onSubmit2 = (data: any) => {
  console.log("-------------verify--------------",data);

  globalSpinner.showSpinner();
  if (isEmail) { 
    emailVerifyCode({}, `?email=${email}&code=${data["code"]}`)
      .then((res: any) => {
        globalSpinner.hideSpinner();
        if (res.data.includes("error")) {
          toast.error(res.data);
        } else {
          toast.success("OTP verified.");
          setTimerMsg("");
          props.setSuperAdmin(true);
        }
      })
      .catch((err: any) => {
        globalSpinner.hideSpinner();
        reactHookFormServerErrors(
          {
            code: ["Error: Invalid OTP.Please enter correct OTP"]
          },
          setError2
        );
      });
    }
    else{
      verifyCode({}, `?code=${data["code"]}&countryCode=${encodeURIComponent(countryCode)}&phoneNumber=${encodeURIComponent(contactNumber)}`
      ).then((res: any) => {
        console.log("Inside verify phone code ",res);
        globalSpinner.hideSpinner();
        if (res.data.includes("error")) {
          toast.error(res.data);
        } else {
          toast.success("OTP verified.");
          setTimerMsg("");
          props.setSuperAdmin(true);
        }
      }).catch((err: any) => {
        globalSpinner.hideSpinner();
        reactHookFormServerErrors(
          {
            code: ["Error: Invalid OTP.Please enter correct OTP"]
          },
          setError2
        );
      });
    }
  };

  return (
    <React.Fragment>
      <Modal.Header closeButton>
        <Modal.Title>Account Verification</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="col-sm-12">
          {screen == 1 ? (
          <div className="row">
          <div className="col-sm-12">
            <form className="needs-validation" onSubmit={handleSubmit(onSubmit)} noValidate>
              <div className="registration_forms registration_sec">
                <div className="sub_heading">
                  <h2>Verify Account</h2>
                </div>
                <div className="already_login2">Send OTP on</div>
                <div className="forgot_otp_btn veri_mar">
                  <div className="m_t_20"><button onClick={() => { setIsEmail(!isEmail); }} type="button" className={isEmail ? "btn btn-success sig_but" : "btn btn-success create_but"}>Email</button></div>
                  <div className="m_t_20"><button onClick={() => { setIsEmail(!isEmail); }} type="button" className={isEmail ? "btn btn-success create_but" : "btn btn-success sig_but"}>Mobile</button></div>
                </div>
               
                  {isEmail?
                   (
                   <div className="form-group">
                   <input type="text" className="form-control form-control-n" placeholder="Type here" name="emailOrMobile" ref={register(val)} />
                   <ErrorMessage errors={errors} name="emailOrMobile" render={({ message }) => <div className="register_validation">{message}</div>} />
                   </div>
                   ):
                   (
                   <div className="form-group">                 
                       <Controller
                  control={control}
                  name="countryCode"
                  render={({ onChange, onBlur, value, name }) => (
                    <PhoneNumberWithCode
                      // codeHeight={"50px"}
                      downwards={true}
                      noPenIcon={true}
                      onChange={onChange}
                      onBlur={onBlur}
                      value={value}
                      name={name}
                      disabled={false}
                      phoneNo={
                        <input
                          type="text"
                          name="contactNumber"
                          disabled={false}
                          id="phoneNumber"
                          className="form-control phoneNumber"
                          placeholder="Type here"
                          ref={register({
                            required: "Mobile number is required",
                            maxLength: {
                              value: watch('countryCode').length > 3 ? 9 : 10,
                              message: `It must be ${watch('countryCode').length > 3 ? 9 : 10} digits only.`,
                            },
                            pattern: {
                              value: /^[0-9]*$/,
                              message: "Mobile number should be numbers only",
                            },
                            minLength: {
                              value: watch('countryCode').length > 3 ? 9 : 10,
                              message: `It must be ${watch('countryCode').length > 3 ? 9 : 10} digits only.`,
                            },
                          })}
                        />
                      }
                    />
                  )}
                />
                <ErrorMessage
                  errors={errors}
                  name="contactNumber"
                  render={({ message }) => (
                    <div className="login_validation">{message}</div>
                  )}
                />
                   </div>
                   )
                  }  
                
                <div className="already_login1">Didn't get the code? <a className="_cursor-pointer" onClick={() => { sendVerification() }}>Resend again.</a></div>
                <div className="form-group veri_mar">
                <button type="submit" className="btn btn-success sig_but veri_but">Send</button>
                <button 
                        type="button"
                        onClick={() => {
                          props.setIsOpen(!props.isOpen);
                          AuthService.logout();
                        }}
                        data-dismiss="modal"
                        className="btn create_but  veri_but"
                      >
                        Cancel
                </button>
                </div>
                <div className="clearfix" />
              </div>
             
            </form>
          </div>
          </div>
           ) : (
            ""
          )}

          {screen == 2 ? (
            <form noValidate>
              <div className="form-group">
                <label htmlFor="email">Enter Code to confirm </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Type here"
                  name="code"
                  ref={register2({
                    required: "Code is required"
                  })}
                />
                <ErrorMessage
                  errors={errors2}
                  name="code"
                  render={({ message }) => (
                    <div className="register_validation">{message}</div>
                  )}
                />
                {showTimer ? (
                  <Timer
                    initialTime={60000 * 5}
                    direction="backward"
                    onStop={() => {
                      console.log("stop");
                    }}
                    onResume={() => console.log("onResume hook")}
                    checkpoints={[
                      {
                        time: 0,
                        callback: () => {
                          console.log("Checkpoint A");
                          setShowTimer(false);
                          setTimerMsg("OTP Expired.");
                          setDisabled(true);
                        }
                      }
                    ]}
                  >
                    {({ reset }: { reset: any }) => (
                      <React.Fragment>
                        <div className="login_validation">
                          {" "}
                          OTP Expires in <Timer.Minutes /> Minutes,{" "}
                          <Timer.Seconds /> Seconds
                        </div>
                      </React.Fragment>
                    )}
                  </Timer>
                ) : (
                  ""
                )}
                {timerMsg ? (
                  <div className="login_validation">{timerMsg}</div>
                ) : (
                  ""
                )}
              </div>
            </form>
          ) : (
            ""
          )}        
        </div>

        {screen == 2 ? (
          <div className="already_login1">
            Didn't get the code?
            <a
              className="_cursor-pointer"
              onClick={() => {
                if (!showTimer) {
                  sendVerification();
                }
              }}
            >
              &nbsp;Resend again.
            </a>
          </div>
        ) : (
          ""
        )}
      </Modal.Body>
      <div className="modal-footer  m-t-30">
        {screen == 2 ? (
          <button
            onClick={handleSubmit2(onSubmit2)}
            className="btn btn-success save-event waves-effect waves-light"
            type="button"
            disabled={disabled}
          >
            Verify
          </button>
        ) : (
          ""
        )}
        {/* <button
          onClick={() => {
            props.setIsOpen2(!props.isOpen2);
          }}
          data-dismiss="modal"
          className="btn btn-default waves-effect"
          type="button"
        >
          Cancel
        </button> */}
      </div>
      <div className="clearfix" />
    </React.Fragment>
  );
}

>>>>>>> 39419b1 (merged-on-10032021)

interface ILoginState {
  email: string;
  password: string;
  rememberMe: boolean;
}

function Login(props: any) {
  const defaultValues = {
    email: "",
    password: "",
    rememberMe: true,
  };

  const {
    register,
    handleSubmit,
    watch,
    errors,
    setValue,
    getValues,
  } = useForm<ILoginState>({
    defaultValues,
  });

  const [showPassword, setShowPassword] = useState<Boolean>(false);

  const globalSpinner = useContext(GlobalSpinnerContext);

<<<<<<< HEAD
=======
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const [superAdmin, setSuperAdmin] = useState<boolean>(false);

  useEffect(() => {
      console.log("----superadminState 01----");
      if(superAdmin){
        props.history.push("/super_admin");
      }
  }, [superAdmin]);

>>>>>>> 39419b1 (merged-on-10032021)
  const onSubmit = async (data: any) => {
    globalSpinner.showSpinner();
    try {
      // const user = await AuthService.login(data.email, data.password, data.rememberMe);
      const user = await AuthService.login(data.email, data.password, true);
      if (user) {
        console.log(1, user);
        console.log("-----------------logged user----------: "+user.roles[0]);
        if(user.roles[0] == "Candidate")
        {
          props.history.push("/candidate");
        }
        else if(user.roles[0] == "Vendor")
        {
          props.history.push("/vendor");
        }
<<<<<<< HEAD
       
=======
        else if (user.roles[0] == "SuperAdmin") {
          setIsOpen(true);
        }
>>>>>>> 39419b1 (merged-on-10032021)
        globalSpinner.hideSpinner();
      } else {
        globalSpinner.hideSpinner();
      }
    } catch (error) {
      console.log(error);
      if (error.response.status == 400) {
        toast.error("Username/Password incorrect.");
      } else {
        toast.error("Error occurred while login.");
      }

      globalSpinner.hideSpinner();
    }
  };

  const handleFbLogin = (data: any) => {
    console.log(data);

    const code = JSON.parse(atob(data.signedRequest.split(".")[1])).code;

    console.log(code);

    // socialLoginHandler({
    //   code: JSON.parse(atob(data.signedRequest.split(".")[1])).code + "",
    //   accessToken: data.accessToken,
    //   social: "fb"
    // }).then((res) => {
    //   console.log(res.data);
    // }).catch((err) => {
    //   console.log(err);
    // });
  };

  return (
    <div className=" container">
      <ToastContainer />
      <div className="row">
        <div className="col-sm-6 desk">
          <div className="login_img">
            <img
              src={require("./../../images/login_img.jpg")}
              className="img-responsive"
            />
          </div>
        </div>
        <div className="col-sm-6 col-lg-5 col-lg-offset-1">
          <div className="login_forms registration_sec">
            <div className="sub_heading">
              <h1>Welcome</h1>
              <p>
                To keep connected with us please login with your email address
                and password.
              </p>
            </div>
            <form
              className="needs-validation"
              onSubmit={handleSubmit(onSubmit)}
              noValidate
            >
              <div className="login_form_sec">
                <div className="form-group">
                  <div className="login_icon">
                    <img src={require("./../../images/mailicon.jpg")} />
                  </div>
                  <div className="login_f">
                    <label>Email Address</label>
                    <input
                      type="text"
                      className="form-control input-name"
                      placeholder="Username"
                      name="email"
                      ref={register({
                        required: "Email is required",
                        pattern: {
                          value: /\S+@\S+\.\S+/,
                          message: "Shold be a valid email"
                        }
                      })}
                    />
                    {/* <div className="login_validation">Error</div> */}
                    <ErrorMessage
                      errors={errors}
                      name="email"
                      render={({ message }) => (
                        <div className="login_validation">{message}</div>
                      )}
                    />
                  </div>
                </div>
                <div className="login_br" />
                <div className="form-group">
                  <div className="login_icon">
                    <img src={require("./../../images/lock_icon.jpg")} />
                  </div>
                  <div className="login_f">
                    <div className="fons_lg">
                      <label>Password</label>
                      <input
                        type={showPassword ? "text" : "password"}
                        className="form-control validate input-pwd"
                        placeholder="Password"
                        name="password"
                        ref={register({
                          required: "Password is required"
                        })}
                      />
                      <span
                        onClick={(event: any) => {
                          setShowPassword(!showPassword);
                        }}
                        className={
                          showPassword
                            ? "fa fa-fw fa-eye field-icon toggle-password"
                            : "fa fa-fw fa-eye-slash field-icon toggle-password"
                        }
                      />
                      {/* <div className="login_validation">Error</div> */}
                      <ErrorMessage
                        errors={errors}
                        name="password"
                        render={({ message }) => (
                          <div className="login_validation">{message}</div>
                        )}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="row m_t_25">
                  <div className="col-xs-6">
                    <input
                      id="checkbox1"
                      type="checkbox"
                      name="rememberMe"
                      ref={register}
                    />
                    <label htmlFor="checkbox1">
                      <span /> Remember Me{" "}
                    </label>
                  </div>
                  <div className="col-xs-6">
                    <div className="forgot">
                      <Link to="/forgot-password-otp">Forgot Password</Link>
                    </div>
                  </div>
                </div>
              </div>
              {/* <Link  to="/candidate"> */}
              <button type="submit" className="btn sig_but">
                Login Now
              </button>
              {/* </Link> */}
              <a href="index.html">
                <button
                  type="button"
                  className="btn create_but"
                  onClick={event => {
                    event.preventDefault();
                    props.history.push("/registration");
                  }}
                >
                  Create An Acount
                </button>
              </a>
            </form>
            <div className="clearfix" />
            <div className="already_login">Or connect with </div>
            <div className="sign_with">
              <GoogleLoginButton />
              <FacebookLoginButton handleLogin={handleFbLogin} />
              <TwitterLoginButton />
              <LinkedinLoginButton />
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </div>
<<<<<<< HEAD
=======
      <Modal
        show={isOpen}
        onHide={() => {
          setIsOpen(isOpen);
        }}
      >
        <Verification
          setIsOpen={setIsOpen}
          isOpen={isOpen}
          setSuperAdmin={setSuperAdmin}
          superAdmin={superAdmin}         
        />
      </Modal>
>>>>>>> 39419b1 (merged-on-10032021)
    </div>
  );
}

export default withRouter(Login);