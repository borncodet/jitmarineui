import React from 'react';
import GoogleLogin from 'react-google-login';
import TwitterLogin from "react-twitter-login";
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { LinkedIn } from 'react-linkedin-login-oauth2';
import { socialLoginIds } from "./../../../environments/environment";
<<<<<<< HEAD
=======
 import { socialLogin } from "./../../../apis/misc"
>>>>>>> 39419b1 (merged-on-10032021)

const FacebookLoginButton = (props: any) => {
  const handleLogin = (res: any) => {
    console.log(res);
<<<<<<< HEAD
    props.handleLogin(res);
=======
    // props.handleLogin(res);
    // if (props.clickPath.includes("register")) {
    let query = `${res.id}?fields=birthday,email,hometown,name&access_token=${res.accessToken}`
    socialLogin("facebook", {}, query)
      .then((resfinal: any) => {
        if (resfinal.status == 200) {
          props.setSocialMediaResponse({ name: resfinal.data.name, email: resfinal.data.email, id: resfinal.data.id, accessToken: res.accessToken, Provider: "facebook" })
          props.setShowSocialMediaRegister(true)
        }

      })
    // }
    // console.log(res);
    //props.handleLogin(res);
>>>>>>> 39419b1 (merged-on-10032021)
  }
  return (
    // <FacebookLogin autoLoad={false}
    //   appId={socialLoginIds.facebook.appId}
    //   callback={handleLogin}
    //   render={(renderProps: any) => (
    //     <a className="_cursor-pointer" onClick={() => { renderProps.onClick() }}><img src={require("./../../../images/facebook.png")} /></a>
    //   )}
    // />

    <FacebookLogin autoLoad={false}
      appId={socialLoginIds.facebook.appId}
      callback={handleLogin}
      fields="name,email,picture"
      scope="public_profile,user_friends"
      render={(renderProps: any) => (
        <a className="_cursor-pointer" onClick={() => { renderProps.onClick() }}><img src={require("./../../../images/facebook.png")} /></a>
      )}
    />
  );

  // return (
  //   <a href={`https://www.facebook.com/v6.0/dialog/oauth?client_id=${socialLoginIds.facebook.appId}&redirect_uri=${encodeURIComponent('http://localhost:8000/oauth-redirect')}`}>
  //     <img src={require("./../../../images/facebook.png")} />
  //   </a>
  // );
};

const GoogleLoginButton = (props: any) => {

  const responseGoogle = (response: any) => {
    console.log(response);
  }

  return (
    <GoogleLogin
      clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
      render={renderProps => (
        <a className="_cursor-pointer" onClick={renderProps.onClick}><img src={require("./../../../images/google.png")} /> </a>
      )}
      buttonText="Login"
      onSuccess={responseGoogle}
      onFailure={responseGoogle}
      cookiePolicy={'single_host_origin'}
    />
  );
};

const LinkedinLoginButton = (props: any) => {
  return (
    <LinkedIn
      clientId="81lx5we2omq9xh"
      onFailure={(err: any) => { console.log(err) }}
      onSuccess={(data: any) => { console.log(data) }}
      redirectUri="http://localhost:3000/login"
      renderElement={({ onClick, disabled }: { onClick: any, disabled: any }) => (
        <a onClick={onClick} className="_cursor-pointer"><img src={require("./../../../images/linkedin.png")} /></a>
      )}
    />
  );
};

const TwitterLoginButton = (props: any) => {

  const authHandler = (err: any, data: any) => {
    console.log(err, data);
  };

  return (
    <TwitterLogin
      className="_twitter-login-button"
      authCallback={authHandler}
      consumerKey={"CONSUMER_KEY"}
      consumerSecret={"CONSUMER_SECRET"}
    >
      <a className="_cursor-pointer"><img src={require("./../../../images/twitter.png")} /></a>
    </TwitterLogin>
  );
};

export {
  GoogleLoginButton,
  FacebookLoginButton,
  TwitterLoginButton,
  LinkedinLoginButton
}