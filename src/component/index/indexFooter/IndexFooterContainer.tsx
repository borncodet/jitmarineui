import React from "react";
import IndexFooterComponent from "./IndexFooterComponent";

interface IIndexFooterContainerProps {}

interface IIndexFooterContainerState {}

const initialState = {};

const IndexFooterContainer: React.FC<IIndexFooterContainerProps> = (props) => {
  const [
    IndexFooterContainerState,
    setIndexFooterContainerState,
  ] = React.useState<IIndexFooterContainerState>(initialState);

  return (
    <>
      <IndexFooterComponent />
    </>
  );
};
export default IndexFooterContainer;
