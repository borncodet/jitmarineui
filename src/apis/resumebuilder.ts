import { apiClient } from "./../utils/httpClient";


export function getResumeTemplateSelectBoxData(data?: any) {
  return apiClient("/api/ResumeTemplate/get-selectbox-data", data);
}


export function getResumeTemplates(data?: any) {
  return apiClient("/api/ResumeTemplate/gaaa", data);
}

// export function getResumeTemplate(data?: any) {
//   return apiClient("/api/ResumeTemplate/ga", data);
// }

export function saveResumeCandidateMap(data?: any) {
  return apiClient("/api/ResumeCandidateMap/coea", data, "post", {
    headers: {
      "Content-Type": 'multipart/form-data'
    }
  });
}
export function getResumeCandidateMap(data?: any) {
  return apiClient("/api/ResumeCandidateMap/gcra", data);
}

export function getAllResumeCandidateMap(data?: any) {
  //return apiClient("/api/ResumeCandidateMap/gaaa", data);
  return apiClient("/api/ResumeCandidateMap/gcaaa", data);
}

export function deleteResumeCandidateMap(data?: any) {
  return apiClient("/api/ResumeCandidateMap/da", data);
}


export function getCoverLetterTemplates(data?: any) {
  return apiClient("/api/CoverLetterTemplate/gaaa", data);
}

export function getAllCoverLetterCandidateMap(data?: any) {
  return apiClient("/api/CoverLetterCandidateMap/gcaaa", data);
}

export function deleteCoverLetterCandidateMap(data?: any) {
  return apiClient("/api/CoverLetterCandidateMap/da", data);
}

export function saveCoverLetterCandidateMap(data?: any) {
  return apiClient("/api/CoverLetterCandidateMap/coea", data, "post", {
    headers: {
      "Content-Type": 'multipart/form-data'
    }
  });
}
export function getCoverLetterCandidateMap(data?: any) {
  return apiClient("/api/CoverLetterCandidateMap/gcca", data);
}