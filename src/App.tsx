import React from "react";
import logo from "./logo.svg";
import "./App.css";
import StartUp from "./StartUp";

function App() {
  return (
    <>
      <StartUp />
    </>
  );
}

export default App;
