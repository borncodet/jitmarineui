import { Reducer } from "react";
import {
  IMyProfileManagementState,
  MyProfileDispatcherContext,
  MyProfileStateContext,
} from "../context/MyProfileSuperAdminContext";
import React from "react";

import axios from "axios";
import AuthService from "../services/AuthService";
import { AppUrls } from "../environments/environment";
import {
  
  profileImageViewModel, SuperAdminProfileBarProgress, SuperAdminProfileImageRequestModel,
} from "../models/superadmin/MyProfileSelectBoxData";
import { apiClient } from "../utils/httpClient";
import { BasicInfoViewModel, MyProfileSelectBoxDataViewModel } from "../models/candidate/MyProfileSelectBoxData";

let token = AuthService.accessToken;
let authorizationToken = token != null ? token.replace(/['"]+/g, "") : "";

export type IMyProfileAction =
  | {
      type: "MY_PROFILE_SELECTBOX_LIST";
      myProfile: MyProfileSelectBoxDataViewModel;
    }
  | {
      type: "MY_PROFILE_Basic_Details";
      basicInfo: BasicInfoViewModel;
    }
  | {
      type: "GET_LOGGED_USERID";
      loggedSuperAdminId: number;
    }
  | {
      type: "GET_PROFILE_IMAGE";
      profileImage: profileImageViewModel;
    }
  | {
      type: "GETTING PROGRESS";
      myProfileProgressBar: SuperAdminProfileBarProgress;
    };

export const myProfileReducer: Reducer<
  IMyProfileManagementState,
  IMyProfileAction
> = (draft, action): IMyProfileManagementState => {
  switch (action.type) {
    case "MY_PROFILE_SELECTBOX_LIST":
      draft.myProfile = action.myProfile;
      return draft;
    case "MY_PROFILE_Basic_Details":
      draft.basicInfo = action.basicInfo;
      return draft;
    case "GET_LOGGED_USERID":
      draft.loggedSuperAdminId = action.loggedSuperAdminId;
      return draft;
    case "GET_PROFILE_IMAGE":
      draft.profileImage = action.profileImage;
      return draft;
    case "GETTING PROGRESS":
      draft.myProfileProgressBar = action.myProfileProgressBar;
      return draft;
  }
};

export const useSuperAdminMyProfileDispatcher = (): React.Dispatch<IMyProfileAction> => {
  const myProfileDispatcher = React.useContext(MyProfileDispatcherContext);
  if (!myProfileDispatcher) {
    throw new Error(
      "You have to provide the MyProfile dispatcher using theMyProfileDispatcherContext.Provider in a parent component."
    );
  }
  return myProfileDispatcher;
};

export const useSuperAdminMyProfileContext = (): IMyProfileManagementState => {
  const myProfileContext = React.useContext(MyProfileStateContext);
  if (!myProfileContext)
    throw new Error(
      "You have to provide the myProfile context using the MyProfileStateContext.Provider in a parent component."
    );
  return myProfileContext;
};

export const getSuperAdminMyProfileSelectBoxList = async (
  dispatcher: React.Dispatch<IMyProfileAction>
) => {
  try {
    let header = {
      "Content-Type": "application/json",
      Accept: "application/json",
    };

    const url = AppUrls.GetMyProfileSelectBoxData;

    axios.post(url, JSON.stringify({}), { headers: header }).then((res) => {
      dispatcher({ type: "MY_PROFILE_SELECTBOX_LIST", myProfile: res.data });
    });
  } catch (e) {}
};

export const getSuperAdminMyProfileBasicInfo = async (
  dispatcher: React.Dispatch<IMyProfileAction>,
  token: string
) => {
  try {
    if (token?.startsWith(`"`)) {
      token = token.substring(1);
      token = token.slice(0, -1);
    }
    let header = {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    };

    const url = AppUrls.GetMyProfileBasicDetails;

    axios.post(url, JSON.stringify({}), { headers: header }).then((res) => {
      dispatcher({ type: "MY_PROFILE_Basic_Details", basicInfo: res.data });
    });
  } catch (e) {}
};

export const getSuperAdminLoggedUserId = async (
  dispatcher: React.Dispatch<IMyProfileAction>,
  query: number,
  token: string
) => {
  try {
    if (token?.startsWith(`"`)) {
      token = token.substring(1);
      token = token.slice(0, -1);
    }

    let header = {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    };
   
    const url = `https://jitapi.clubactive.in/api/SuperAdmin/gv/${query}`;

    axios.get(url, { headers: header }).then((res) => {
      dispatcher({ type: "GET_LOGGED_USERID", loggedSuperAdminId: res.data });
    });
  } catch (e) {}
};

export const getSuperAdminProfileImage = async (
  dispatcher: React.Dispatch<IMyProfileAction>,
  query: SuperAdminProfileImageRequestModel,
  token: string
) => {
  try {
    if (token?.startsWith(`"`)) {
      token = token.substring(1);
      token = token.slice(0, -1);
    }

    let header = {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    };

    const url = AppUrls.GetSuperAdminProfileImage;

    axios.post(url, JSON.stringify(query), { headers: header }).then((res) => {
      dispatcher({ type: "GET_PROFILE_IMAGE", profileImage: res.data });
    });
  } catch (e) {}
};

export const getSuperAdminProfileProgressBarResult = async (
  dispatcher: React.Dispatch<IMyProfileAction>,
  query: SuperAdminProfileImageRequestModel,
  token: string
) => {
  try {
    if (token?.startsWith(`"`)) {
      token = token.substring(1);
      token = token.slice(0, -1);
    }

    let header = {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    };

    const url = AppUrls.GetProfileProgressBar;

    axios.post(url, JSON.stringify(query), { headers: header }).then((res) => {
      dispatcher({ type: "GETTING PROGRESS", myProfileProgressBar: res.data });
    });
  } catch (e) {}
};
