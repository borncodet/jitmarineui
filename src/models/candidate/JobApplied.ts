import { jobViewModel } from "./BookMarkedJob";

export interface jobApplied {
  rowId: number;
  jobAppliedId: number;
  candidateId: number;
  candidatetName: string;
  jobId: number;
  jobTitle: string;
  isActive: boolean;
}

export interface jobAppliedViewModel {
  data: jobApplied[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface jobAppliedRequestModel {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
}

export interface jobAppliedSaveRequestModel {
  rowId: number;
  jobAppliedId: number;
  candidateId: number;
  jobId: number;
  isActive: boolean;
}

export interface jobAppliedSaveResultModel {
  entityId: number;
  message: string;
  type: number;
  isSuccess: boolean;
  code: number;
}

export interface jobAppliedCandidateRequestModel {
  candidateId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface jobAppliedCandidateResultModel {
  rowId: number;
  jobId: number;
  categoryId: number;
  categoryName: string;
  title: string;
  description: string;
  experienceId: number;
  experience: string;
  numberOfVacancies: number;
  jobTypeId: number;
  jobType: string;
  isBookmarked: boolean,
  isApplied: boolean,
  isPreferred: boolean;
  isRequired: boolean;
  locationId: string;
  regionId: string;
  territoryId: string;
  minAnnualSalary: number;
  maxAnnualSalary: number;
  currencyId: number;
  currency: string;
  industryId: number;
  industry: string;
  functionalAreaId: number;
  functionalArea: string;
  profileDescription: string;
  willingnessToTravelFlag: boolean;
  preferedLangId: number;
  preferedLanguage: string;
  autoScreeningFilterFlag: boolean;
  autoSkillAssessmentFlag: boolean;
  postedDate: Date;
  jobAppliedId: number;
  candidateId: number;
  candidatetName: string;
  daysAgo: string;
  isActive: boolean;
}

export interface jobAppliedCandidateResulViewtModel {
  data: jobViewModel[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}


// export interface jobVieMmodel {
//   rowId: number;
//   jobId: number;
//   categoryId: number;
//   categoryName: string;
//   title: string;
//   description: string;
//   experienceId: number;
//   experience: string;
//   numberOfVacancies: number;
//   jobTypeId: number;
//   jobType: string;
//   isBookmarked:boolean,
//   isPreferred: boolean;
//   isRequired: boolean;
//   locationId: string;
//   regionId: string;
//   territoryId: string;
//   minAnnualSalary: number;
//   maxAnnualSalary: number;
//   currencyId: number;
//   currency: string;
//   industryId: number;
//   industry: string;
//   functionalAreaId: number;
//   functionalArea: string;
//   profileDescription: string;
//   willingnessToTravelFlag: boolean;
//   preferedLangId: number;
//   preferedLanguage: string;
//   autoScreeningFilterFlag: boolean;
//   autoSkillAssessmentFlag: boolean;
//   postedDate: Date;
//   jobAppliedId: number;
//   candidateId: number;
//   candidatetName: string;
//   daysAgo: string;
//   isActive: boolean;
//   JobBookmarkedId: number;
// }
