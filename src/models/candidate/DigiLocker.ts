export interface digiLockerGetDataWithId {
  rowId: number
}


export interface digiLockerTypeRequestModel {
  Page: number;
  PageSize: number;
  SearchTerm: string;
  SortOrder: string;
  autharize: string;
}

export interface digiLockerType {
  rowId: number;
  digiDocumentTypeId: number;
  title: string;
  description: string;
  isActive: boolean;
}

export interface digiLockerTypeViewModel {
  data: digiLockerType[];
  Total: number;
  HasNext: boolean;
  HasPreviousPage: boolean;
  CurrentPage: number;
  CurrentPageSize: number;
}

export interface digiLockerDocumentRequestModel {
  candidateId: number;
  digiDocumentTypeId: number;
  pageIndex: number;
  pageSize: number;
  showInactive: boolean;
}

export interface digiLockerDetails {
  rowId: number;
  digiDocumentDetailId: number;
  name: string;
  description: string;
  candidateId: number;
  candidateName: number;
  digiDocumentTypeId: number;
  digiDocumentTypeTitle: number;
  documentNumber: number;
  expiryDate: Date;
  isActive: boolean;
  selectedFlag: boolean;
  expiryFlag: boolean;
  updatedDate: Date;
  digDcoumentFullPath: string;
  digiDocument: string;
  documentType: string;
}

export interface digiLockerDetailsResultModel {
  data: digiLockerDetails;
  message: string;
  type: number;
  isSuccess: boolean;
  code: number;
}

export interface digiLockerDetailsViewModel {
  data: digiLockerDetails[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface digiLockerExpiringDocumentRequestModel {
  candidateId: number;
  digiDocumentTypeId: number;
  pageIndex: number;
  pageSize: number;
  showInactive: boolean;
}



export interface digiLockerExpiringDocument {
  rowId: number;
  digiDocumentDetailId: number;
  name: string;
  description: string;
  candidateId: number;
  candidateName: number;
  digiDocumentTypeId: number;
  digiDocumentTypeTitle: number;
  expiryDate: Date;
  isExpired: boolean,
  updatedDate: Date;
  isActive: boolean;
  digiDocument: string
  digDcoumentFullPath: string
}

export interface digiLockerExpiringDocumentViewModel {
  data: digiLockerExpiringDocument[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface digiLockerDocumentSaveRequestModel {
  rowId: number;
  digiDocumentDetailId: number;
  name: string;
  description: string;
  documentNumber: string
  candidateId: number;
  digiDocumentTypeId: number;
  expiryDate: string;
  expiryFlag: boolean;
  isActive: boolean;
}

export interface digiLockerDocumentSaveRespondModel {
  entityId: number;
  message: string;
  type: number;
  isSuccess: boolean;
  code: number;
}

export interface digiLockerCountOfDocumentRequestModel {
  CandidateId: number;
  DigiDocumentTypeId: number;
  PageIndex: number;
  PageSize: number;
  ShowInactive: boolean;
}

export interface digiLockerDeleteDocumentRequestmodel {
  RowId: number;
}

export interface digiLockerDeleteDocumentResponseModel {
  EntityId: number;
  Message: string;
  Type: number;
  IsSuccess: boolean;
  Code: number;
}

export interface digiLockerDocumentMoveRequestModel {
  RowId: number[];
  DigiDocumentTypeId: number;
}


export interface digiLockerDocumentMoveResultmodel {
  EntityId: number;
  Message: string;
  Type: number;
  isSuccess: boolean;
  Code: number;
}

export interface digidocDownloadResult {
  downloadUrl: string;
  message: string;
  type: number;
  isSuccess: boolean;
  code: number;
}

export interface digidocDownloadRequest {
  digiDocumentDetailId: number;
}