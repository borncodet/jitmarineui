export interface ResumeResult {
  rowId: number;
  resumeCandidateMapId: number;
  resumeTemplateId: number;
  candidateId: number;
  resume: string;
  isActive: boolean;
  title: string;
  description: string;
  resumeContent: string;
  resumeImage: string;
  resumeName: string;
  resumeFile: string;
  resumeFileFullPath: string;
  candidateResumeImage: string;
  candidateResumeImageFullPath: string;
  createdDate: Date;
  updatedDate: Date;
}

export interface ResumeAllList {
  data: ResumeResult[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}
