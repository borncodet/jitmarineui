export interface suggestedJobRequestModel {
  CandidateId: number;
  Title: string;
  Location: string;
  Type: string;
  Expereince: string;
  LastDays: number;
  PageIndex: number;
  PageSize: number;
  ShowInactive: boolean;
}

export interface SuggestedJob {
  RowId: number;
  JobId: number;
  CategoryId: number;
  CategoryName: string;
  Title: string;
  Description: string;
  ExperienceId: number;
  Experience: string;
  NumberOfVacancies: number;
  JobTypeId: number;
  JobType: string;
  IsPreferred: boolean;
  IsRequired: boolean;
  LocationId: string;
  RegionId: string;
  TerritoryId: string;
  MinAnnualSalary: number;
  MaxAnnualSalary: number;
  CurrencyId: number;
  Currency: string;
  IndustryId: number;
  Industry: string;
  FunctionalAreaId: number;
  FunctionalArea: string;
  ProfileDescription: string;
  WillingnessToTravelFlag: boolean;
  PreferedLangId: number;
  PreferedLanguage: string;
  AutoScreeningFilterFlag: boolean;
  AutoSkillAssessmentFlag: boolean;
  PostedDate: Date;
  IsActive: boolean;
}

export interface SuggestedJobViewModel {
  Data: SuggestedJob[];
  Total: number;
  HasNext: boolean;
  HasPreviousPage: boolean;
  CurrentPage: number;
  CurrentPageSize: number;
}
