export interface experience {
  rowId: number;
  expereinceTypeId: number;
  title: string;
  fromDay: number;
  fromMonth: number;
  fromYear: number;
  toDay: number;
  toMonth: number;
  toYear: number;
  description: string;
  isActive: boolean;
}

export interface experienceViewModel {
  data: experience[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface experienceRequestModel {
page: number;
pageSize: number;
searchTerm: string;
sortOrder: string;
}