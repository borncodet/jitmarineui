
export interface jobAlertVendor {
  rowId: number;
  jobId: number;
  categoryId: number;
  categoryName: string;
  title: string;
  description: string;
  experienceId: number;
  experience: string;
  numberOfVacancies: number;
  jobTypeId: number;
  jobType: string;
  isPreferred: boolean;
  isRequired: boolean;
  locationId: string;
  regionId: number;
  territoryId: number;
  minAnnualSalary: number;
  maxAnnualSalary: number;
  currencyId: number;
  currency: string;
  industryId: number;
  industry: string;
  functionalAreaId: number;
  functionalArea: string;
  profileDescription: string;
  willingnessToTravelFlag: boolean;
  preferedLangId: number;
  preferedLanguage: string;
  autoScreeningFilterFlag: boolean;
  autoSkillAssessmentFlag: boolean;
  postedDate: Date;
  isActive: boolean;
}

export interface jobAlertVendorViewModel {
  data: jobAlertVendor[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}



export interface jobAlertVendorRequestmodel {
    candidateId: number;
    title: string;
    location: string;
    type: string;
    expereince: string;
    lastDays: number;
    pageIndex: number;
    pageSize: number;
    showInactive: boolean;
}

// export interface jobAlertVendorSaveRequestModel {
//   rowId: number;
//   jobAlertVendorId: number;
//   keywords: string;
//   totalExperience: string;
//   locationId: string;
//   alertTitle: string;
//   salaryFrom: number;
//   userId: number;
//   isActive: boolean;
// }

export interface jobAlertVendorSaveRequestModel {
  rowId: number;
  jobAlertId: number;
  VendorId: number;
  JobCategoryId: number;
  location: string;
  jobTitle: string;
  JobTypeId: number;
  Active: boolean;
  isActive: boolean;
}

export interface jobAlertVendorSaveRespondModel {
  entityId: number;
  message: string;
  type: number;
  isSuccess: boolean;
  code: number;
}

export interface jobAlertVendorCategoryMapRequestModel {
  rowId: number;
  jobAlertVendorCategoryMapId: number;
  jobCategoryId: number;
  jobAlertVendorId: number;
  isActive: boolean;
}

export interface jobAlertVendorExperienceMapRequestModel {
  rowId: number;
  jobAlertVendorExperienceMapId: number;
  durationId: number;
  jobAlertVendorId: number;
  isActive: boolean;
}

export interface jobAlertVendorIndustryMapSaveRequestModel {
  rowId: number;
  robAlertIndustryMapId: number;
  industryId: number;
  jobAlertVendorId: number;
  isActive: boolean;
}

export interface JobAlertVendorRoleMapSaveRequestModel

{
  rowId: number;
  jobAlertVendorRoleMapId: number;
  jobRoleId: number;
  jobAlertVendorId: number;
  isActive: boolean;
}

export interface JobAlertVendorTypeMapSaveRequestModel
{
  rowId: number;
  jobAlertVendorTypeMapId: number;
  jobTypeId: number;
  jobAlertVendorId: number;
  isActive: boolean;
}

export interface jobAlertVendorGellAllRequestModel {
  vendorId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface  jobAlertVendorGetAll {
  rowId: number;
  jobAlertId: number;
  vendorId: number;
  jobCategoryId: number;
  jobCategory: string;
  location: string;
  jobTitle: string;
  jobTypeId: number;
  jobType: string;
  active: boolean;
  isActive: boolean;
}

export interface jobAlertVendorGellAllRespondModel {
  data: jobAlertVendorGetAll[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface jobAlertVendorTitleDropdownResult {
  value: string;
  caption: string;
}

// export interface jobAlertVendorTitleDropdownResultArray {
//  data:jobAlertVendorTitleDropdownResult[]
// }