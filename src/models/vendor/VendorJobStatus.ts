export interface JobStatusGetAllRequestModel {
  VendorId: number;
  JobId: number;
  Page: number;
  jobStatusId:number;
  PageSize: number;
  SearchTerm: string;
  SortOrder: string;
  ShowInactive: boolean;
}

export interface JobStatusGetAll {
  rowId: number;
  candidateId: number;
  firstName: string;
  middleName: string;
  lastName: string;
  // dOB: Date;
  isActivevisa: boolean;
  expirary_Date: Date;
  passPort: string;
  visaIssueCountry: string;
  vendorId: number;
  dob: string;
}

export interface JobStatusGetAllRespondModel {
  data: JobStatusGetAll[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}


export interface JobStatusDeleteResult {
  entityId: number;
  message: string;
  Type: number;
  IsSuccess: boolean;
  Code: number;
}
