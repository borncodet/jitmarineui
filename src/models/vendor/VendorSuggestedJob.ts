export interface VendorSuggestedRequestModel {
  vendorId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface VendorSuggestedJob {
  rowId: number;
<<<<<<< HEAD
  vendorId: number;
  jobCtegory: string;
  jobType: string;
  daysAgo: number;
  jobLocation: string;
  experience: string;
=======
  jobId: number;
  categoryId: number;
  categoryName: string;
  title: string;
  description: string;
  experienceId: number;
  experience: string;
  numberOfVacancies: number;
  jobTypeId: number;
  jobType: string;
  isBookmarked: boolean,
  isApplied: boolean,
  isPreferred: boolean;
  isRequired: boolean;
  location: string;
  regionId: number;
  territoryId: number;
  minAnnualSalary: number;
  maxAnnualSalary: number;
  currencyId: number;
  currency: string;
  industryId: number;
  industry: string;
  functionalAreaId: number;
  functionalArea: string;
  profileDescription: string;
  willingnessToTravelFlag: boolean;
  preferedLangId: number;
  preferedLanguage: string;
  autoScreeningFilterFlag: boolean;
  autoSkillAssessmentFlag: boolean;
  postedDate: Date;
  isActive: boolean;
  noofvaccancies:number;
  days_Ago:number;
>>>>>>> 39419b1 (merged-on-10032021)
}

export interface VendorSuggestedJobViewModel {
  data: VendorSuggestedJob[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}


export interface VendorSuggestedJobTitleRequestModel {
  vendorId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface VendorSuggestedJobTitle {
  rowId: number;
  title: string;
  jobCategory: string;
}

export interface VendorSuggestedJobTitleViewModel {
  data: VendorSuggestedJobTitle[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

