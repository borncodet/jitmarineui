

  export interface VendorCandidateList {
      rowId: number;
      firstName: string;
      middleName: string;
      lastName: string;
      passPortNumber: string;
      dob: Date;
      isActiveVisa: boolean;
      visaIssueCountry: string;
      expDate: Date;
      isActive: boolean;
  }

  export interface VendorJobAppliedRequestModel {
      rowId: number;
      vendorId: number;
      vendorCandidateList: VendorCandidateList[];
      jobId: number;
  }

  export interface VendorJobAppliedRespondModal {
    entityId: number;
    message: string;
    type: number;
    isSuccess: boolean;
    code: number;
}

export interface VendorGetAllJobRequestModel {
  vendorId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface VendorGetAllJobList {
  // rowId: number;
  // jobId: number;
  // jobTitle: string;
  // jobCategory: string;
  // days_Ago: number;
  // noofvaccancies: number;
  // location: string;
  // isBookmarked: boolean;
  // isActive: boolean;
  rowId: number;
  jobId: number;
  categoryId: number;
  categoryName: string;
  title: string;
  description: string;
  experienceId: number;
  experience: string;
  numberOfVacancies: number;
  jobTypeId: number;
  jobType: string;
  isBookmarked: boolean,
  isApplied: boolean,
  isPreferred: boolean;
  isRequired: boolean;
<<<<<<< HEAD
  locationId: string;
=======
  location: string;
>>>>>>> 39419b1 (merged-on-10032021)
  regionId: number;
  territoryId: number;
  minAnnualSalary: number;
  maxAnnualSalary: number;
  currencyId: number;
  currency: string;
  industryId: number;
  industry: string;
  functionalAreaId: number;
  functionalArea: string;
  profileDescription: string;
  willingnessToTravelFlag: boolean;
  preferedLangId: number;
  preferedLanguage: string;
  autoScreeningFilterFlag: boolean;
  autoSkillAssessmentFlag: boolean;
  postedDate: Date;
  isActive: boolean;
<<<<<<< HEAD
=======
  noofvaccancies:number;
  days_Ago:number;
>>>>>>> 39419b1 (merged-on-10032021)
}

export interface VendorGetAllJobRespondModel {
  data: VendorGetAllJobList[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}



  export interface GetAllSuggestedJob {
      rowId: number;
      vendorId: number;
      jobCtegory: string;
      jobType: string;
      daysAgo: number;
      jobLocation: string;
      experience: string;
  }

  export interface GetAllSuggestedJobRespondModel 
    {
      data: GetAllSuggestedJob[];
      total: number;
      hasNext: boolean;
      hasPreviousPage: boolean;
      currentPage: number;
      currentPageSize: number;
  }

//   export interface Datum {
//     RowId: number;
//     Title: string;
//     JobCategory: string;
// }

// export interface RootObject {
//     Data: Datum[];
//     Total: number;
//     HasNext: boolean;
//     HasPreviousPage: boolean;
//     CurrentPage: number;
//     CurrentPageSize: number;
// }



