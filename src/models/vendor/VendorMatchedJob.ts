export interface matchedJob {
  rowId: number;
  jobId: number;
  categoryId: number;
  categoryName: string;
  title: string;
  description: string;
  experienceId: number;
  experience: string;
  numberOfVacancies: number;
  jobTypeId: number;
  jobType: string;
  isBookmarked: boolean,
  isApplied: boolean,
  isPreferred: boolean;
  isRequired: boolean;
<<<<<<< HEAD
  locationId: string;
=======
  location: string;
>>>>>>> 39419b1 (merged-on-10032021)
  regionId: number;
  territoryId: number;
  minAnnualSalary: number;
  maxAnnualSalary: number;
  currencyId: number;
  currency: string;
  industryId: number;
  industry: string;
  functionalAreaId: number;
  functionalArea: string;
  profileDescription: string;
  willingnessToTravelFlag: boolean;
  preferedLangId: number;
  preferedLanguage: string;
  autoScreeningFilterFlag: boolean;
  autoSkillAssessmentFlag: boolean;
  postedDate: Date;
  isActive: boolean;
<<<<<<< HEAD
=======
  noofvaccancies:number;
  days_Ago:number;
>>>>>>> 39419b1 (merged-on-10032021)
}

export interface matchedJobVendorViewModel {
  data: matchedJob[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface matchedJobVendorRequestModel {
  
    VendorId:number,
     PageIndex:number,
    PageSize:number,
    SearchTerm:String,
    SortOrder:string,
    ShowInactive:boolean

}