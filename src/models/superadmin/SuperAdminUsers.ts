export interface SuperAdminUserRequestModel {
  superAdminId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface SuperAdminUserRequest {
  checkbox: string[];
  countryCode: string;
  email: string;
  firstName: string;
  location: string;
  mobileNumber: string;
  password: string;
}

export interface SuperAdminUser {
  adminId: number;
  adminName: string;
  email: string;
  countryCode: string;
  mobileNumber: string;
  superAdminPassword: string;
  generatePassword: string;
  location: string;
  adminStatus: boolean;
}

export interface SuperAdminUserRespondModel {
  data: SuperAdminUser[];
  total: number;
  HasNext: boolean;
  HasPreviousPage: boolean;
  CurrentPage: number;
  CurrentPageSize: number;
}

export interface CreateAdminRequestModel {
  SuperAdminId: number;
  RowId: number;
  AdminName: string;
  Email: string;
  CountryCode: string;
  MobileNumber: string;
  GeneratePassword: string;
  IsActive: boolean;
  Location: string;
  AdminTitleIdList: number[];
}

export interface CreateAdminRespondModel {
  EntityId: number;
  Message: string;
  Type: number;
  IsSuccess: boolean;
  Code: number;
}

export interface SuperAdminRequestModelById {
  RowId: number;
}

export interface PermissionType {
  TypeId: number;
  TypeName: string;
  Titleid: number;
  Title: string;
}

export interface SuperAdminRespondModelById {
  adminName: string;
  email: string;
  countryCode: string;
  mobileNumber: string;
  superAdminPassword: string;
  generatePassword: string;
  location: string;
  adminStatus: boolean;
  permissionTypes: number[];
}

export interface SuperAdminRespondModel {
  data: SuperAdminRespondModelById;
}

export interface GetAllPermissionRequestModel {
  Page: number;
  PageSize: number;
  SearchTerm: string;
  SortOrder: string;
}

// export interface GetAllPermission {
//   typeId: number;
//   typeName: string;
//   titlesId: number;
//   title: string;
// }

// export interface GetAllPermissionResultModel {
//   data: GetAllPermission[];
//   total: number;
//   HasNext: boolean;
//   HasPreviousPage: boolean;
//   CurrentPage: number;
//   CurrentPageSize: number;
// }

export interface SuperAdminUserDeleteRequestModel {
  SuperAdminId: number;
  RowId: number;
}

export interface SuperAdminUserDeleteResultModel {
  EntityId: number;
  Message: string;
  Type: number;
  IsSuccess: boolean;
  Code: number;
}

export interface TypesInfo {
  typeId: number;
  typename: string;
}

export interface TitlesInfo {
  titleId: number;
  title: string;
}

export interface GetAllPermissionResult {
  typesInfo: TypesInfo;
  titlesInfo: TitlesInfo[];
}

export interface GetAllPermissionResultModel {
  data: GetAllPermissionResult[];
}

export interface InvitationRequestModel {
  PhoneNumber: string;
  Email: string;
  Password: string;
}
