export interface SuperAdminReportRequestModel {
  SuperAdminId: number;
  FilterId: number;
  Page: number;
  PageSize: number;
  SearchTerm: string;
  SortOrder: string;
  ShowInactive: boolean;
}

export interface SuperAdminReportResultModel {
  adminId: number;
  adminName: string;
  imageurl: string;
  noofAprovedJobs: number;
  noodPublishedJobs: number;
  noofViewedJobs: number;
  noofProcessedJobs: number;
  NoofCreatedJobs: number;
  NoofDeletedJobs: number;
  NoofImpoertJobs: number;
  NoofExportJobs: number;
}
