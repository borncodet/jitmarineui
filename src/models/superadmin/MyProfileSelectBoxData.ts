export interface NamePrefix {
  value: string;
  caption: string;
}

export interface Gender {
  value: string;
  caption: string;
}

export interface MartialStatus {
  value: string;
  caption: string;
}

export interface JobCategory {
  value: string;
  caption: string;
}

export interface Designation {
  value: string;
  caption: string;
}

export interface JobType {
  value: string;
  caption: string;
}

export interface Industry {
  value: string;
  caption: string;
}

export interface FunctionalArea {
  value: string;
  caption: string;
}

export interface JobRole {
  value: string;
  caption: string;
}

export interface State {
  value: string;
  caption: string;
}

export interface Country {
  value: string;
  caption: string;
}

export interface SuperAdminMyProfileSelectBoxDataViewModel {
  namePrefix: NamePrefix[];
  genders: Gender[];
  martialStatus: MartialStatus[];
  jobCategories: JobCategory[];
  designations: Designation[];
  jobTypes: JobType[];
  industries: Industry[];
  functionalAreas: FunctionalArea[];
  jobRoles: JobRole[];
  states: State[];
  countries: Country[];
}

export interface SuperAdminBasicInfoViewModel {
  userId: number;
  userName: string;
  fullName: string;
  email: string;
  phoneNumber: string;
  message: string;
  type: number;
  isSuccess: boolean;
  code: number;
}

export interface SuperAdminProfileImageRequestModel {
  superadminId: number;
  page: number;
  pageSize: number;
  searchTerm: string;
  sortOrder: string;
  showInactive: boolean;
}

export interface profileImage {
  rowId: number;
  superadminProfileImageId: number;
  superadminId: number;
  imageUrl: string;
  isActive: boolean;
}

export interface profileImageViewModel {
  data: profileImage[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface profileImageViewModel {
  data: profileImage[];
  total: number;
  hasNext: boolean;
  hasPreviousPage: boolean;
  currentPage: number;
  currentPageSize: number;
}

export interface SuperAdminProfileBarProgress {
  profileProgress: number;
  resumeProgress: number;
}
